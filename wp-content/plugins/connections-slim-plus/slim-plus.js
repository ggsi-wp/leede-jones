jQuery(document).ready(function ($) {

	var select = $('#cn-slim-plus select.cn-enhanced-select');

	if ( select.length ) {

		select.chosen();
	}

	$('.cn-hide').css('display', 'none');

	$('h3.cn-accordion-item').click( function() {
		var $this = $(this);
		var div = $this.attr('data-div-id');

		if ( $( '#' + div ).css('display') == 'block' ) {
			$( '#' + div ).slideUp();
			$($this).children('.cn-sprite').toggleClass('cn-open');
		} else {

			$( '#' + div ).slideDown( 1000, function() {
				// Trigger a click so the map renders because the visibility check is bound to a click event.
				$( this ).trigger('click');
			});

			$($this).children('.cn-sprite').toggleClass('cn-open');
		}

		return false
	});

});
