=== Connections Business Directory Template - Slim Plus ===
Contributors: Steven A. Zahm
Donate link: https://connections-pro.com/
Tags: template
Requires at least: 5.0
Tested up to: 5.6
Requires PHP: 5.6.20
Stable tag: 2.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Template for the Connections Business Directory plugin.

== Description ==

Template for the Connections Business Directory plugin.

== Frequently Asked Questions ==

= Where can I find the documentation? =

The documentation can be found here:

[https://connections-pro.com/documentation/slim-plus/](https://connections-pro.com/documentation/slim-plus/)

= Where can I find the FAQs? =

The FAQs can be found here:

[https://connections-pro.com/documentation/slim-plus/#FAQs](https://connections-pro.com/documentation/slim-plus/#FAQs)

== Screenshots ==

== Changelog ==

= 2.4 11/20/2020 =
* NEW: Introduce `isMapEnabled()`.
* NEW: Introduce helper function determine whether or not to include the map block based on the user defined settings.
* TWEAK: Remove use of deprecated methods.
* TWEAK: Utilize `isMapEnabled()` to set default map block status.
* TWEAK: Check the data is of correct type before access/modify.
* TWEAK: Trigger map to display when Entry is expanded instead waiting on user scroll.
* TWEAK: Remove usage of deprecated `getMapBlock()` method in favor of using Content Block API.
* BUG: Actions should filters.
* OTHER: Rename file.
* DEV: phpDoc updates.

= 2.3 07/29/2020 =
* TWEAK: Minor CSS tweak to edit permalink.
* TWEAK: Remove unnecessary `is_admin()` check when adding shortcode filters.
* TWEAK: The edit Entry permalink container should only render with the link is available for use to the admin.
* TWEAK: Remove usage of deprecated `getMapBlock()` method in favor of using Content Block API in the template card view.

= 2.2.2 04/28/2020 =
* TWEAK: Minor CSS tweak to make accordion heading CSS selector more specific.
* TWEAK: Use Connections core function to render the edit link instead of custom logic in the template.
* DEV: phpDoc updates.

= 2.2.1 11/15/2018 =
* BUG: Correct parsing of the `enable_map` shortcode option so it is properly converted to a boolean.

= 2.2 11/12/2018 =
* FEATURE: Add support for displaying a map.

= 2.1 09/24/2018 =
* NEW: Add support for Form/Link edit edit entry link.
* TWEAK: Use more specific selector when initializing Chosen.
* TWEAK: Do not default email label, only affect the supported shortcode options for renaming email labels.
* TWEAK: Use class constant to set the version.
* BUG: If search is enabled, it should not limit list.
* OTHER: Correct misspellings.
* OTHER: Add whitespace.
* OTHER: Prevent undefined index PHP notices.
* DEV: phpDoc corrections.
* DEV: Update .gitignore.
* DEV: Add phpDoc for IDE code completion.
* DEV: Update plugin header.

= 2.0.1 02/10/2015 =
* TWEAK: CSS tweaks.
* OTHER: Delete minified CSS which belonged to another  template.

= 2.0 05/07/2014 =
* FEATURE: Date and Link Types Shortcode Option.
* FEATURE: Add support for content boxes.
* BUG: setCategory() will not honor the 'lock' setting.
* TWEAK: CSS tweaks.
* OTHER: Bring core template code into compliance with 0.8.x.

= 1.0.2 12/27/2013 =
* BUG: Fix calling of methods as static.
* BUG: The permalink for the form action should not be run thru cnSEO permalink filter.
* TWEAK: Enable support for the exclude_category shortcode option.
* TWEAK: Enable support for the home_id shortcode option.
* TWEAK: Enable support for the force_home shortcode option.
* TWEAK: Add classes to the bio, notes and map div.
* OTHER: Remove use of get_query_var('cn-cat'). This will now be handled by core as it was overriding the `lock` shortcode option.

= 1.0.1 07/25/2013 =
* BUG: Convert show_contact_name to bool.
* BUG: Fix the drop down multi-select logic.

= 1.0 03/15/2013 =
* Initial Release

== Upgrade Notice ==

= 2.1 =
It is recommended to backup before updating. Requires WordPress >= 4.5.3 and PHP >= 5.4 PHP version >= 7.1 recommended.

= 2.2 =
It is recommended to backup before updating. Requires WordPress >= 4.5.3 and PHP >= 5.4 PHP version >= 7.1 recommended.

= 2.2.1 =
It is recommended to backup before updating. Requires WordPress >= 4.5.3 and PHP >= 5.4 PHP version >= 7.1 recommended.

= 2.2.2 =
It is recommended to backup before updating. Requires WordPress >= 4.7.12 and PHP >= 5.6.20 PHP version >= 7.1 recommended.

= 2.3 =
It is recommended to backup before updating. Requires WordPress >= 4.7.12 and PHP >= 5.6.20 PHP version >= 7.1 recommended.

= 2.4 =
It is recommended to backup before updating. Requires WordPress >= 5.0 and PHP >= 5.6.20 PHP version >= 7.2 recommended.
