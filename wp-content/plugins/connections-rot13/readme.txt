=== Connections Business Directory Extension - ROT13 Email Encryption ===
Contributors: Steven A. Zahm
Donate link: http://connections-pro.com/
Tags: csv import
Requires at least: 4.5.3
Tested up to: 5.1
Requires PHP: 5.6
Stable tag: 2.0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Encrypt the email address to help block against email harvesting bots used to send spam.

== Description ==

An extension for the Connections Business Directory plugin which encrypts the email addresses with ROT13 encryption in the frontend.

== Frequently Asked Questions ==

= Where can I find the documentation? =

The documentation can be found here:

[https://connections-pro.com/documentation/contents/](https://connections-pro.com/documentation/contents/)

= Where can I find the FAQs? =

The FAQs can be found here:

[https://connections-pro.com/documentation/contents/](https://connections-pro.com/documentation/contents/)

== Screenshots ==

== Changelog ==

= 2.0.1 04/09/2019 =
* TWEAK: Only add the filter for the email addresses to the site's frontend.
* OTHER: Update copyright year.
* OTHER: Update link to homepage.
* OTHER: Bump tested up to version to 5.1.
* OTHER: Bump minimum PHP version to 5.6.

= 2.0 11/19/2018 =
* NEW: Complete rewrite to modernize the codebase.
* TWEAK: Ensure compatibility with the Autoptimize plugin.
* TWEAK: Enqueue minified JavaScript file.

= 1.2 09/10/2015 =
* NEW: Introduce `make_protocol_relative_url()`.
* TWEAK: Change the base URL constant so it is protocol-relative.
* TWEAK: Register/enqueue the script all at once.
* TWEAK: Move the add_action() hook for enqueueing the scripts.
* TWEAK: Remove the by reference in the hook callbacks.
* TWEAK: Enqueue the javascript on the `wp_enqueue_scripts` hook.
* TWEAK: Remove the PHP version check as it is unneeded.
* OTHER: Quick code formatting cleanup.
* DEV: Update .git* files.

= 1.1 05/08/2014 =
* FEATURE: Add licensing and updater support.

= 0.1 =
Initial Release.

== Upgrade Notice ==

= 1.2 =
It is recommended to backup before updating. Requires WordPress >= 4.5.3 and PHP >= 5.4 PHP version >= 7.1 recommended.

= 2.0 =
It is recommended to backup before updating. Requires WordPress >= 4.5.3 and PHP >= 5.4 PHP version >= 7.1 recommended.

= 2.0.1 =
It is recommended to backup before updating. Requires WordPress >= 4.5.3 and PHP >= 5.6 PHP version >= 7.1 recommended.
