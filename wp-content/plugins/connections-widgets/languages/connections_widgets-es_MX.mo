��          �      |      �     �  +     '   3  .   [  *   �  !   �  $   �     �       
         +     @     W     q     �     �     �     �     �     �     �  ?  �     8  +   Q  -   }  1   �  4   �  2     0   E     v     �     �     �     �     �     �          /     H     ]     p     y     �                              	                
                                                   A list of categories. A list of entries with anniversaries today. A list of entries with birthdays today. A list of entries with upcoming anniversaries. A list of entries with upcoming birthdays. A list of recently added entries. A list of recently modified entries. Anniversaries Today Birthdays Today Categories Directory Categories No Anniversaries Today No Upcoming Anniversaries No Upcoming Birthdays Recently Added Recently Modified Search Directory Simple Search Title: Upcoming Anniversaries Upcoming Birthdays Project-Id-Version: Connections Widgets
POT-Creation-Date: 2018-04-24 11:28-0400
PO-Revision-Date: 2018-04-24 11:28-0400
Last-Translator: Steven A. Zahm <helpdesk@connections-pro.com>
Language-Team: 
Language: es_MX
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.7
X-Poedit-KeywordsList: __;_e;_n;_x;esc_html_e;esc_html__;esc_attr_e;esc_attr__;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_x:1,2c
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
 Una lista de categorías Una lista de entradas con aniversarios hoy. Una lista de las entradas de cumpleaños hoy. Una lista de entradas con próximos aniversarios. Una lista de entradas con los próximos cumpleaños. Una lista de las entradas agregadas recientemente. Una lista de entradas modificadas recientemente. Aniversarios de hoy Cumpleaños hoy Categorías Categorías del directorio No hay aniversarios de hoy No hay próximos aniversarios No hay cumpleaños próximos. Añadido recientemente Recientemente modificado Buscar en Directorio Búsqueda sencilla Título: Próximos aniversarios No hay cumpleaños próximos. 