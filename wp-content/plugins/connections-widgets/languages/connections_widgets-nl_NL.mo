��          �      |      �     �  +     '   3  .   [  *   �  !   �  $   �     �       
         +     @     W     q     �     �     �     �     �     �     �  E  �     >     Y  ,   y  !   �  %   �  ,   �  +        G     Y     m     y     �     �     �     �     �                    %     9                              	                
                                                   A list of categories. A list of entries with anniversaries today. A list of entries with birthdays today. A list of entries with upcoming anniversaries. A list of entries with upcoming birthdays. A list of recently added entries. A list of recently modified entries. Anniversaries Today Birthdays Today Categories Directory Categories No Anniversaries Today No Upcoming Anniversaries No Upcoming Birthdays Recently Added Recently Modified Search Directory Simple Search Title: Upcoming Anniversaries Upcoming Birthdays Project-Id-Version: connections_widget_nl-NL.po
POT-Creation-Date: 2018-04-24 11:28-0400
PO-Revision-Date: 2018-04-24 11:28-0400
Last-Translator: Steven A. Zahm <helpdesk@connections-pro.com>
Language-Team: 
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.7
X-Poedit-KeywordsList: __;_e;_n;_x;esc_html_e;esc_html__;esc_attr_e;esc_attr__;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_x:1,2c
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SearchPath-0: .
 Eeen lijst van categorieen Een lijst van vieringen vandaag Een lijst van mensen die vandaag jarig zijn. Een lijst met verwachte vieringen Een lijst van verwachte verjaardagen. Een lijst van recentelijk toegevoegde items. Een lijst van recentelijk gewijzigde items. Vieringen vandaag Vandaag zijn jarig: Categorieen Directory Categorieen Geen vieringen vandaag Geen verwachte vieringen Geen verwachte verjaardagen Recentelijk toegevoegd Recentelijk gewijzigd Zoek directory Zoeken Titel: Verwachte vieringen Verwachte verjaardagen 