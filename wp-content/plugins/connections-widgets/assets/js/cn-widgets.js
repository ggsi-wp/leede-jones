;jQuery( document ).ready( function($) {

	// Hide children term lists.
	$( 'ul.cn-cat-tree.cnw-style-expand li.cn-cat-parent.cn-cat-has-children ul.cn-cat-children, ul.cn-cat-tree.cnw-style-accordion li.cn-cat-parent.cn-cat-has-children ul.cn-cat-children' ).slideUp('normal');

	// Expand children term lists on click.
	$( 'ul.cn-cat-tree.cnw-style-expand li.cn-cat-parent.cn-cat-has-children a' ).click( function(e) {

		var me = $(this);

		if ( ! me.next().is( ':visible' ) ) {

			me.next().stop( true, true ).slideDown('normal');
			me.parent().addClass('cn-cat-selected');

		} else {

			me.next().stop( true, true ).slideUp('normal');
			me.parent().removeClass('cn-cat-selected');
		}

		if ( me.parent().hasClass( 'cn-cat-has-children' ) ) {

			e.preventDefault();
		}

	});

	$( 'ul.cn-cat-tree.cnw-style-accordion li.cn-cat-parent.cn-cat-has-children a' ).click( function(e){

		var me = $(this);
		var depth = me.parents().length;
		var allAtDepth = $('.cn-cat-children').filter( function() {

			if ( $(this).parents().length >= depth && $(this).prev().get(0) !== me.get(0) ) {

				return true;
			}
		});

		$(allAtDepth).slideUp('fast').parent().removeClass('cn-cat-selected');

		me.next().slideToggle( 'normal', function() {

			$('.ul.cn-cat-tree.cnw-style-accordion .cn-cat-children :visible:last');

		}).parent().toggleClass('cn-cat-selected');

		if ( me.parent().hasClass( 'cn-cat-has-children' ) ) {

			e.preventDefault();
		}
	});

});
