= 2.6.1 10/03/2018 =
* TWEAK: Beaver Builder compatibility tweak.
* TWEAK: Elementor compatibility tweak.

= 2.6 09/25/2018 =
* BUG: Fix the checkboxes saving in the Upcoming Event Dates widget.

= 2.6-rc1 08/06/2018 =
* FEATURE: Add a new upcoming widget which can be configured to display a list of any of the available events dates.
* TWEAK: Only show image option in List widget when one of the image options are chosen.
* BUG: Need to set the `page_id` if the directory home page is the same as the home page so searches are correctly processed by WordPress.
* BUG: Correct undefined index PHP notice.
* BUG: Correct undefined variable PHP notice.
* DEV: Readme.txt updates.

= 2.5 04/24/2018 =
* TWEAK: Prevent index undefined notice by checking for `wp_inactive_widgets` and displaying a user message stating the widget was found but is inactive.
* TWEAK: HTML tweak to the category list widget.
* TWEAK: Minor CSS tweaks to the category list widget.
* BUG: Add missing text domain to string.
* BUG: Correct widget option description.
* I18N: Update POT file.
* I18N: Add Portuguese - PORTUGAL (PT) (pt_PT) translation.
* I18N: Update all included translations.
* OTHER: Plugin header name and description updates.
* OTHER: Update copyright year.
* DEV: Add phpDoc.

= 2.4 08/26/2016 =
* FEATURE: Add an "Expand" and "Accordion" layout style option to the Category List widget.
* BUG: Add missing closing `span` tag.
* I18N: Update .POT file.
* I18N: Include Spanish (Mexico) translation.
* I18N: Update nl_NL Translation.

= 2.3 04/28/2016 =
* NEW: Add filter to to list widget to allow other image types to be added; such as SiteShot.
* TWEAK: Refactor the recently added widget template to support other image image options register with the `cn_widget_list_image_options` filter.
* TWEAK: Make the date the default image in the birthday and anniversaries widgets.
* OTHER: Correct misspelling.
* DEV: Add some phpDoc.

= 2.2 06/17/2015 =
* FEATURE: Add a randomization option to the List Widget.

= 2.1 06/09/2015 =
* FEATURE: New List Widget.

= 2.0.1 05/25/2015 =
* TWEAK: Minor phpDoc fixes.
* TWEAK: Remove priority and argument count from a couple actions as they were unnecessary.
* TWEAK: Add helper function to render widgets.
* BUG: Correct display label in the today's birthdays widget.
* BUG: Fix bug that prevented the widget from outputting correctly when the do not display widget when no results option was disabled.

= 2.0 04/16/2015 =
* FEATURE: Complete rewrite.
* FEATURE: All widgets can be used as shortcodes.
* FEATURE: Category Widget - Option to define which categories to include or exclude.
* FEATURE: Category Widget - Option to display a specific category children categories.
* FEATURE: Category Widget - Option to limit the category level depth to display.
* FEATURE: Category Widget - Option to show the number of entries assigned to the category.
* FEATURE: Category Widget - Option to show or hide empty categories.
* FEATURE: Anniversary/Birthday - Limit entries to specific categories and whether the entry much be within all selected categories or not.
* FEATURE: Anniversary/Birthday - Exclude entries assigned to specific categories.
* FEATURE: Anniversary/Birthday - Whether or not to display the entry image or logo or none.
* FEATURE: Anniversary/Birthday - Option to control the format of the name.
* FEATURE: Anniversary/Birthday - Option to link to the entry profile page or not.
* FEATURE: Anniversary/Birthday - Option to display the widget or not if there are no birthdays/anniversaries today.
* FEATURE: Anniversary/Birthday - Option to define the "No results" message.
* FEATURE: Anniversary/Birthday - Option to limit the max number of entries to display.
* FEATURE: Upcoming Anniversary/Upcoming Birthday - Limit entries to specific categories and whether the entry much be within all selected categories or not.
* FEATURE: Upcoming Anniversary/Upcoming Birthday - Exclude entries assigned to specific categories.
* FEATURE: Upcoming Anniversary/Upcoming Birthday - Whether or not to display the entry date or image or logo or none.
* FEATURE: Upcoming Anniversary/Upcoming Birthday - Option to control the format of the name.
* FEATURE: Upcoming Anniversary/Upcoming Birthday - Option to link to the entry profile page or not.
* FEATURE: Upcoming Anniversary/Upcoming Birthday - Option to display the widget or not if there are no birthdays/anniversaries today.
* FEATURE: Upcoming Anniversary/Upcoming Birthday - Option to define the "No results" message.
* FEATURE: Upcoming Anniversary/Upcoming Birthday - Option to limit the max number of days to look ahead for upcoming birthdays/anniversaries.
* FEATURE: Recently Added/ Recently Modified - Limit entries to specific categories and whether the entry much be within all selected categories or not.
* FEATURE: Recently Added/ Recently Modified - Exclude entries assigned to specific categories.
* FEATURE: Recently Added/ Recently Modified - Whether or not to display the entry date or image or logo or none.
* FEATURE: Recently Added/ Recently Modified - Option to control the format of the name.
* FEATURE: Recently Added/ Recently Modified - Option to link to the entry profile page or not.
* FEATURE: Recently Added/ Recently Modified - Option to limit the max number of entries to display.
* TWEAK: Convert code to singleton pattern.
* TWEAK: Break out each widget into its own file.
* TWEAK: Prevent direct access of widgets.
* TWEAK: Add blank index.php files.
* TWEAK: Remove use of PHP short tag.
* TWEAK: Widgets now use the core template engine.
* OTHER: Update plugin header.
* OTHER: Add license.txt.
* OTHER: Add gpl.txt.
* OTHER: Change changelog.txt file name case.
* DEV: Add .editorconfig file.
* DEV: Add phpStorm to .gitignore.
* DEV: Add .jshintrc

= 1.2 09/18/2013 =
* TWEAK: Force links in the widgets to resolve to the directory home page.

= 1.1.2 08/25/2013 =
* BUG: Fix grammar.

= 1.1.1 07/25/2013 =
* BUG: Remove stray debug code.

= 1.1 07/24/2013 =
* Add Dutch translation.

= 1.0.1 07/17/2013 =
* Remove the page_link filter from the search widget so it'll search the entire directory when on a directory page.
* Move the changelog.txt file.

= 1.0 10/1/2012 =
* Initial Release
