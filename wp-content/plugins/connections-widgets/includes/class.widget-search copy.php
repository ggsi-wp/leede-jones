<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class cnWidgetSearch extends WP_Widget {

	/**
	 * Register widget.
	 */
	public function __construct() {

		$options = array(
			'description' => __('Simple Search', 'connections_widgets' )
		);

		parent::__construct(
			'cnw_search',
			'Connections : ' . __('Simple Search', 'connections_widgets' ),
			$options
		);
	}

	/**
	 * Registers the widget with the WordPress Widget API.
	 *
	 * @access public
	 * @since  2.0
	 *
	 * @return void
	 */
	public static function register() {

		register_widget( __CLASS__ );
	}

	/**
	 * Logic for handling updates from the widget form.
	 *
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		// Insert update logic here, and check that all the values in $new_instance are valid for your particular widget

		return $new_instance;
	}

	/**
	 * Function for handling the widget control in admin panel.
	 *
	 * @param array $instance
	 * @return void
	 */
	public function form( $instance ) {

		$title = isset( $instance['title'] ) && strlen( $instance['title'] ) > 0 ? esc_attr( $instance['title'] ) : __( 'Search Directory', 'connections_widgets' );

		?>

		<label for="<?php echo $this->get_field_id('title');?>"><?php _e('Title:', 'connections_widgets') ?></label><br/>
		<input class="widefat" id="<?php echo $this->get_field_id('title');?>" name="<?php echo $this->get_field_name('title');?>" type="text" value="<?php echo $title?>" /><br/>

		<?php
	}

	/**
	 * Function for displaying the widget on the page.
	 *
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	public function widget( $args, $instance ) {
		global $wp_rewrite, $connections;

		// The class.seo.file is only loaded in the frontend; do not attempt to remove the filter
		// otherwise it'll cause an error.
		if ( ! is_admin() ) cnSEO::doFilterPermalink( FALSE );

		$form = '';

		// Extract $before_widget, $after_widget, $before_title and $after_title
		extract( $args );

		// Extract your options
		extract( $instance );

		// Get the directory home page ID.
		$homeID = $connections->settings->get( 'connections', 'connections_home_page', 'page_id' );

		// Get the permalink of the of the directory home.
		$permalink = get_permalink( $homeID );

		$title = strlen( $title ) > 0 ? $title : __( 'Search Directory', 'connections_widgets' );

		echo $before_widget;

		echo $before_title . $title . $after_title . PHP_EOL;

		if ( $wp_rewrite->using_permalinks() ) {

			$form .= '<form role="search" method="get" action="' . $permalink . '">';
			if ( is_front_page() ) $form .= '<input type="hidden" name="page_id" value="' . $homeID .'">';

		} else {

			$form .= '<form role="search" method="get">';
			$form .= '<input type="hidden" name="p" value="' . $homeID .'">';
		}

		do_action( 'cn_search_form' );

		$form .= $connections->template->search( array( 'show_label' => FALSE , 'return' => TRUE ) );

		$form .= '</form>';

		echo apply_filters( 'cn_search_form', $form );

		echo $after_widget;

		// The class.seo.file is only loaded in the frontend; do not attempt to remove the filter
		// otherwise it'll cause an error.
		if ( ! is_admin() ) cnSEO::doFilterPermalink( TRUE );
	}
}
