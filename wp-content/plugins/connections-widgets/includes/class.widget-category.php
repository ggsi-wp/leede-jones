<?php

	/**
	 * The upcoming bithdays widget.
	 *
	 * @package     Connections Widget Pack
	 * @subpackage  Upcoming Birthdays Widget
	 * @copyright   Copyright (c) 2014, Steven A. Zahm
	 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
	 * @since       2.0
	 */

	// Exit if accessed directly
	if ( ! defined( 'ABSPATH' ) ) {
		exit;
	}

	class cnWidgetCategory extends WP_Widget {

		/**
		 * @var string
		 */
		var $style = 'cnw-style-default';

		/**
		 * Register widget.
		 */
		public function __construct() {

			$options = array(
				'description' => __( 'A list of categories.', 'connections_widgets' )
			);

			parent::__construct(
				'cnw_categories',
				'Connections : ' . __( 'Categories', 'connections_widgets' ),
				$options
			);

			add_filter( 'cn_term_list_class', array( $this, 'termListTreeClass' ) );
		}

		/**
		 * Registers the widget with the WordPress Widget API.
		 *
		 * @access public
		 * @since  2.0
		 *
		 * @return void
		 */
		public static function register() {

			register_widget( __CLASS__ );
		}

		/***
		 * Process updates from the widget form.
		 *
		 * @access private
		 * @since  1.0
		 * @uses   sanitize_text_field()
		 * @uses   absint()
		 * @uses   cnFragment::clear()
		 *
		 * @param array $new
		 * @param array $old
		 *
		 * @return array
		 */
		public function update( $new, $old ) {

			$new['title'] = sanitize_text_field( $new['title'] );

			$new['limit'] = isset( $new['limit'] ) && in_array( $new['limit'], array( 'none', '__clude', 'parent' ) ) ? $new['limit'] : 'none';

			if ( in_array( $new['limit'], array( '__clude', 'parent' ) ) ) {

				switch ( $new['limit'] ) {

					case '__clude':

						if ( isset( $new['category'] ) && ! empty( $new['category'] ) ) {

							array_walk( $new['category'], 'absint' );

							$new['category'] = json_encode( $new['category'] );
						}

						$new['__clude'] = isset( $new['__clude'] ) && in_array( $new['__clude'], array( 'include', 'exclude_tree' ) ) ? $new['__clude'] : 'include';

						break;

					case 'parent':

						if ( isset( $new['child_of'] ) && ! empty( $new['child_of'] ) ) {

							$new['child_of'] = absint( $new['child_of'] );
						}

						$new['descendant_depth'] = isset( $new['descendant_depth'] ) && in_array( $new['descendant_depth'], array( 'child_of', 'parent' ) ) ? $new['descendant_depth'] : 'parent';

						break;
				}

			}

			$new['show_count'] = isset( $new['show_count'] ) ? '1' : '0';

			$new['hide_empty'] = isset( $new['hide_empty'] ) ? '1' : '0';

			if ( $new['limit'] == 'none' ) {

				$new['depth']  = isset( $new['depth'] ) && filter_var( $new['depth'], FILTER_VALIDATE_INT, array( 'options' => array( 'min_range' => 0 ) ) ) !== FALSE ? absint( $new['depth'] ) : 0;
			}

			if ( ! in_array( $new['style'], array( 'cnw-style-default', 'cnw-style-accordion', 'cnw-style-expand' ) ) ) {

				$new['style'] = 'cnw-style-default';
			}

			//$new['style'] = 'cnw-style-expand';

			// Clear the widget group fragment cache.
			cnFragment::clear( TRUE, 'cnw_categories-' . $this->number );

			return $new;
		}

		/**
		 * Callback to display the widget's settings in the admin.
		 *
		 * @access private
		 * @since  1.0
		 * @uses   esc_attr()
		 * @uses   cnHTML::text()
		 * @uses   get_field_id()
		 * @uses   get_field_name()
		 * @uses   set_query_var()
		 * @uses   cnTemplatePart::category()
		 * @uses   cnHTML::field()
		 * @uses   cnHTML::input()
		 * @uses   wp_enqueue_style()
		 * @uses   wp_enqueue_script()
		 * @uses   add_action()
		 *
		 * @param array $instance
		 *
		 * @return void
		 */
		public function form( $instance ) {

			$title = isset( $instance['title'] ) && strlen( $instance['title'] ) > 0 ? esc_attr(
				$instance['title']
			) : __( 'Directory Categories', 'connections_widgets' );

			cnHTML::text(
				array(
					'prefix' => '',
					'class'  => 'widefat',
					'id'     => $this->get_field_id( 'title' ),
					'name'   => $this->get_field_name( 'title' ),
					'label'  => __( 'Title:', 'connections_widgets' ),
					'before' => '<p>',
					'after'  => '</p>',
				),
				$title
			);

			echo '<p>' . __( 'Display:', 'connections_widgets' ) . '</p>';

			$limit = isset( $instance['limit'] ) && in_array( $instance['limit'], array( 'none', '__clude', 'parent' ) ) ? $instance['limit'] : 'none';

			cnHTML::field(
				array(
					'type'    => 'radio',
					'prefix'  => '',
					'id'      => $this->get_field_id( 'limit' ),
					'name'    => $this->get_field_name( 'limit' ),
					'options' => array(
						'none' => __(
							'All.',
							'connections_widgets'
						),
						'__clude'   => __(
							'Include or exclude specific categories.',
							'connections_widgets'
						),
						'parent' => __(
							'Display specific category parent.',
							'connections_widgets'
						),
					),
					'before'  => '<div style="margin-left: 16px;">',
					'after'   => '</div>',
					'display' => 'block',
					'layout'  => '%field% %label%',
				),
				$limit
			);

			if ( in_array( $limit, array( '__clude', 'parent' ) ) ) {

				switch ( $limit ) {

					case '__clude':

						if ( isset( $instance['category'] ) && ! empty( $instance['category'] ) ) {

							$categories = json_decode( $instance['category'] );

							array_walk( $categories, 'absint' );

						} else {

							$categories = array();
						}

						cnTemplatePart::category(
							array(
								'type'            => 'multiselect',
								'id'              => $this->get_field_id( 'category' ),
								'name'            => $this->get_field_name( 'category' ),
								'label'           => __( 'Select categories:', 'connections_widgets' ),
								'default'         => __( 'Click to Select Categories', 'connections_widgets' ),
								'on_change'       => '',
								'enhanced'        => TRUE,
								'style'           => array( 'max-width' => '100%' ),
								'show_select_all' => FALSE,
								'before'          => '<p>',
								'after'           => '</p>',
							),
							$categories
						);

						cnHTML::field(
							array(
								'type'    => 'radio',
								'prefix'  => '',
								'id'      => $this->get_field_id( '__clude' ),
								'name'    => $this->get_field_name( '__clude' ),
								'options' => array(
									'include' => __(
										'Include only the categories selected above.',
										'connections_widgets'
									),
									'exclude_tree' => __(
										'Exclude the categories selected above.',
										'connections_widgets'
									),
								),
								//'label'   => __( 'Must be in all categories selected above?', 'connections_widgets' ),
								'before'  => '<p style="margin-left: 16px;">',
								'after'   => '</p>',
								'display' => 'block',
								'layout'  => '%field% %label%',
							),
							isset( $instance['__clude'] ) && in_array( $instance['__clude'], array( 'include', 'exclude_tree' ) ) ? $instance['__clude'] : 'include'
						);

						break;

					case 'parent':

						cnTemplatePart::category(
							array(
								'type'            => 'select',
								'id'              => $this->get_field_id( 'child_of' ),
								'name'            => $this->get_field_name( 'child_of' ),
								'label'           => __( 'Limit to the selected category:', 'connections_widgets' ),
								'default'         => __( 'Click to Select Category', 'connections_widgets' ),
								'on_change'       => '',
								'enhanced'        => TRUE,
								'style'           => array( 'max-width' => '100%' ),
								'show_select_all' => FALSE,
								'before'          => '<p>',
								'after'           => '</p>',
							),
							isset( $instance['child_of'] ) && ! empty( $instance['child_of'] ) ? absint( $instance['child_of'] ) : array()
						);

						$depth = isset( $instance['descendant_depth'] ) && in_array( $instance['descendant_depth'], array( 'child_of', 'parent' ) ) ? $instance['descendant_depth'] : 'parent';

						cnHTML::field(
							array(
								'type'    => 'radio',
								'prefix'  => '',
								'id'      => $this->get_field_id( 'descendant_depth' ),
								'name'    => $this->get_field_name( 'descendant_depth' ),
								'options' => array(
									'parent'   => __(
										'Display only the immediate children of the selected category.',
										'connections_widgets'
									),
									'child_of' => __(
										'Display all descendants of the selected category.',
										'connections_widgets'
									),
								),
								'before'  => '<p style="margin-left: 16px;">',
								'after'   => '</p>',
								'display' => 'block',
								'layout'  => '%field% %label%',
							),
							$depth
						);

						break;
				}

			}

			cnHTML::field(
				array(
					'type'   => 'checkbox',
					'prefix' => '',
					'id'     => $this->get_field_id( 'show_count' ),
					'name'   => $this->get_field_name( 'show_count' ),
					'label'  => __( 'Show Count?', 'connections_widgets' ),
					'before' => '<p>',
					'after'  => '</p>',
					'layout' => '%field% %label%',
				),
				isset( $instance['show_count'] ) && $instance['show_count'] !== '0' ? '1' : '0'
			);

			cnHTML::field(
				array(
					'type'   => 'checkbox',
					'prefix' => '',
					'id'     => $this->get_field_id( 'hide_empty' ),
					'name'   => $this->get_field_name( 'hide_empty' ),
					'label'  => __( 'Hide empty?', 'connections_widgets' ),
					'before' => '<p>',
					'after'  => '</p>',
					'layout' => '%field% %label%',
				),
				isset( $instance['hide_empty'] ) && $instance['hide_empty'] !== '0' ? '1' : '0'
			);


			if ( $limit == 'none' ) {

				$depth = isset( $instance['depth'] ) && filter_var( $instance['depth'], FILTER_VALIDATE_INT, array( 'options' => array( 'min_range' => 0 ) ) ) !== FALSE ? absint( $instance['depth'] ) : 0;

				cnHTML::input(
					array(
						'type'   => 'number',
						'prefix' => '',
						'class'  => 'small-text',
						'id'     => $this->get_field_id('depth'),
						'name'   => $this->get_field_name('depth'),
						'label'  => __( 'The number of levels deep of the category tree that should be shown.', 'connections_widgets' ),
						'before' => '<p>',
						'after'  => '</p>',
						'layout'   => '%label% %field%',
					),
					$depth
				);

				echo '<p class="description">' . __( '<strong>NOTE:</strong> The default is <code>0</code>. When set to <code>0</code>, the entire category tree will be shown.', 'connections_widgets' ) . '</p>';

			}

			$style = isset( $instance['style'] ) && in_array( $instance['style'], array( 'cnw-style-default', 'cnw-style-accordion', 'cnw-style-expand' ) ) ? $instance['style'] : 'cnw-style-default';

			cnHTML::field(
				array(
					'type'    => 'select',
					'prefix'  => '',
					'id'      => $this->get_field_id( 'style' ),
					'name'    => $this->get_field_name( 'style' ),
					'options' => array(
						'cnw-style-default'    => esc_html__( 'Default', 'connections_widgets' ),
						'cnw-style-accordion'  => esc_html__( 'Accordion', 'connections_widgets' ),
						'cnw-style-expand'     => esc_html__( 'Expand', 'connections_widgets' ),
					),
				    'label'   => esc_html__( 'Display Style:', 'connections_widgets' ),
					'before'  => '<p>',
					'after'   => '</p>',
					//'display' => 'block',
					//'layout'  => '%field% %label%',
				),
				$style
			);

			// Add Chosen support for the enhanced select drop downs.
			wp_enqueue_style( 'cn-chosen' );
			wp_enqueue_script( 'jquery-chosen' );
			add_action( 'admin_print_footer_scripts', array( 'Connections_Widgets', 'chosen' ) );

		}

		/**
		 * Callback to display the widget on the frontend.
		 *
		 * @access private
		 * @since  1.0
		 * @uses   Connections_Directory()
		 * @uses   cnTemplateFactory::loadTemplate()
		 * @uses   wp_get_current_user()
		 * @uses   cnFragment()
		 * @uses   cnRetrieve::entries()
		 * @uses   add_filter()
		 * @uses   remove_filter()
		 *
		 * @param array $atts
		 * @param array $instance
		 *
		 * @return void
		 */
		public function widget( $atts, $instance ) {

			/**
			 * @var $before_widget
			 * @var $show_title
			 *  Will only exist if the widget is being displayed via the `[cn_widget]` shortcode.
			 * @var $before_title
			 * @var $after_title
			 * @var $after_widget
			 */
			extract( $atts );

			if ( ! isset( $show_title ) ) {

				$show_title = TRUE;
			}

			/*
			 * --> START <--
			 * Setup the default widget options if they were not set when they were added to the sidebar;
			 * the user did not click the "Save" button on the widget.
			 */

			// Add the `widget_title` filter to match the WP core widgets.
			$instance['title'] = apply_filters(
				'widget_title',
				strlen( $instance['title'] ) > 0
					? $instance['title']
					: __(
					'Directory Categories',
					'connections_widgets'
				),
				$instance,
				$this->id_base,
				$this
			);

			$instance['limit'] = isset( $instance['limit'] ) && in_array( $instance['limit'], array( 'none', '__clude', 'parent' ) ) ? $instance['limit'] : 'none';

			if ( in_array( $instance['limit'], array( '__clude', 'parent' ) ) ) {

				switch ( $instance['limit'] ) {

					case '__clude':

						if ( isset( $instance['category'] ) && ! empty( $instance['category'] ) ) {

							$categories = json_decode( $instance['category'] );

							array_walk( $categories, 'absint' );

							$__clude = isset( $instance['__clude'] ) && in_array( $instance['__clude'], array( 'include', 'exclude_tree' ) ) ? $instance['__clude'] : 'include';

							$attr[ $__clude ] = $categories;
						}

						break;

					case 'parent':

						if ( isset( $instance['child_of'] ) && ! empty( $instance['child_of'] ) ) {

							$instance['child_of'] = absint( $instance['child_of'] );

						} else {

							$instance['child_of'] = 0;
						}

						$descendant_depth = isset( $instance['descendant_depth'] ) && in_array( $instance['descendant_depth'], array( 'child_of', 'parent' ) ) ? $instance['descendant_depth'] : 'parent';

						$attr[ $descendant_depth ] = $instance['child_of'];

						break;
				}

			}

			$attr['show_count'] = ! isset( $instance['show_count'] ) || $instance['show_count'] !== '0' ? TRUE : FALSE;

			$attr['hide_empty'] = ! isset( $instance['hide_empty'] ) || $instance['hide_empty'] !== '0' ? TRUE : FALSE;

			$depth = isset( $instance['depth'] ) && filter_var( $instance['depth'], FILTER_VALIDATE_INT, array( 'options' => array( 'min_range' => 0 ) ) ) !== FALSE ? absint( $instance['depth'] ) : 0;

			if ( isset( $instance['style'] ) && 0 < strlen( $instance['style'] ) ) {

				$this->style = $instance['style'];
			}

			/*
			 * --> END <--
			 * Setup the default widget options if they were not set when they were added to the sidebar;
			 * the user did not click the "Save" button on the widget.
			 */

			// Add the user ID to the $attr array so it will be included when creating the hash for the cache fragment key.
			// The reason is because the category counts are dependant on the current user permissions.
			$user = wp_get_current_user();
			$attr[] =  $user->ID;

			// Add the depth to the $attr array so it will be included when creating the hash for the cache fragment key.
			$attr['depth'] = $depth;

			$key   = hash( 'crc32b', json_encode( $attr ) );
			$group = 'cnw_categories-' . $this->number;

			echo $before_widget;

			if ( $show_title && strlen( $instance['title'] ) > 0 ) {

				echo $before_title . $instance['title'] . $after_title . PHP_EOL;
			}

			$widget = new cnFragment( $key, $group );

			if ( ! $widget->get() ) {

				cnTemplatePart::walker( 'term-list', $attr );

				$widget->save();
			}

			echo $after_widget;

			wp_enqueue_script( 'cn-widgets-js' );
		}

		/**
		 * Callback for the `cn_term_list_class` filter.
		 *
		 * @param array $class
		 *
		 * @return array
		 */
		public function termListTreeClass( $class ) {

			$class[] = $this->style;

			return $class;
		}

	}
