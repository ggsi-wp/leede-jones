<?php
/**
 * Change password 
 */
namespace LJClient\Security;

function change_password()
{   
    require_once( SECURITYFORM . 'security.php' ); 
    require_once( SECURITYFORM . 'verify.php' ); 
    require_once( SECURITYFORM . 'form-parts.php' ); 
    $questions  = getQuestions(); 

    ob_start();
        if( is_user_logged_in() )
        {   
            $res    = NULL;
            $cQuestions = json_decode( get_post_meta( $_SESSION['clientId'], 'client_secqs', TRUE ) ); 
            $cAnswers   = json_decode( get_post_meta( $_SESSION['clientId'], 'client_seqas', TRUE ) ); 
            
            if( isset( $_POST['submit'] ) )
            {
                $res    = \Verify\verifyUpdate();
                if( TRUE === $res )
                {   
                    $message    = '<p>Your changes have been successfully made.';
                    // Write responses
                    if( !empty( $_POST['new-password'] ) )  // If there's a new password
                    {
                        \Security\updatePassword( get_current_user_id(), $_SESSION['clientId'] );   
                        $message    .= ' As you have changed your password, <a href="' . site_url() . '/login">click here to log in again.</a></p>';
                    }
                    else
                    {
                        $message    .= ' <a href="' . site_url() . '/client-dashboard">Click here to go the Dashboard</a></p>';
                    }
                    if( !empty( $_POST['client_secq1'] ) ) \Security\updateAnswers( $_SESSION['clientId'] );    // If there are new security questions
                }
            }

            // What do we display?
            if( !empty( $message ) )
            {
                echo $message;
            }
            else
            {   
                $formType   = ( isset( $_SESSION['pwchange'] ) && !isset( $_SESSION['secChange'] ) ) ? 'password-only' : 'full';
                echo \LJClient\Security\form( $formType, $res, $questions, $cQuestions, $cAnswers );
            }
        }
        elseif( isset( $_REQUEST['step'] ) )    // Link from Password Reset email
        {   
            $userId     = filter_var( $_REQUEST['user_id'], FILTER_VALIDATE_INT ); 
            $clientId   = getClientId( $userId );
            $cQuestions = json_decode( get_post_meta( $clientId, 'client_secqs', TRUE ) ); 
            $cAnswers   = json_decode( get_post_meta( $clientId, 'client_seqas', TRUE ) ); 

            if( isset( $_POST['submit'] ) )
            {
                $res    = \Verify\verifyReset( $userId, $cQuestions, $cAnswers ); 
                if( TRUE === $res )
                {   
                    \Security\updatePassword( $userId, $clientId );
                    $message    = 'Your changes have been successful. <a href="' . site_url() . '/login">Would you like to login</a>?';
                }
                if( !empty( $message ) )
                {
                    echo $message;
                }
                else
                {   
                    $res->clientId  = $clientId;
                    echo \LJClient\Security\form( 'partial', $res, $questions, $cQuestions, $cAnswers, TRUE );
                }

            }
            else
            {
                $res    = \Verify\verifyKey( $userId ); 
                
                if( 'WP_Error' == get_class( $res ) )   // Well, that didn't work
                {   
                    // Echo out the errors
                    ?>
                    <div class="error">
                        <?php foreach( $res->errors as $e ): ?>
                            <?php echo reset( $e ); ?><br />
                        <?php endforeach; ?>
                    </div>
                    <?php
                }
                else
                {
                    $res->clientId      = getClientId( $userId );
                    echo \LJClient\Security\form( 'partial', $res, $questions, $cQuestions, $cAnswers, TRUE );
                }
            }
        }
        else
        {
            echo 'You shouldn\'t be here. ';
            // Redirect to login or home page
        }

    return ob_get_clean();
}

function form( $formType = 'full', $res = NULL, $questions, $cQuestions, $cAnswers, $wrapper = FALSE )
{   
    $qCount     = esc_attr( get_option( 'security_questions_required' ) ); 
    
    ob_start(); 
        //Present password change form
        echo \FormParts\changePassword( $res, $wrapper );

        switch ( $formType )
        {
            case 'full':
                // All questions
                echo \FormParts\changeAnswers( $questions, $cQuestions, $cAnswers, $qCount );
                break;
            case 'partial':
                // One confirmation question
                echo \FormParts\checkAnswer( $questions, $cQuestions );
                break;
            case 'password-only':
                // No questions
                break;         
        }

        echo \FormParts\formClose( $wrapper );
        $o  = ob_get_clean();
    return $o;
}