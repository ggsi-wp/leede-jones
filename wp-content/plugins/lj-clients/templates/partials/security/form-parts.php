<?php 
/**
 * Form parts
 */

 namespace FormParts;

function changePassword( $res, $wrapper = FALSE )
{   
    ob_start(); 
        if( $wrapper ) echo '<div class="lj-form_wrapper">'; ?>
<!--             <style>
                form.lj-login-form {
                    font-family: 'Roboto', sans-serif;
                }
                form.lj-login-form fieldset {
                    margin: 0;
                    padding: 0 0 1em 0;
                    line-height: 2em;
                    border: 0;
                }
                form.lj-login-form label {
                    display: block;
                    margin: 0;
                    vertical-align: middle;
                }
                form.lj-login-form fieldset input {
                    border: 1px solid #4774b3;
                    background-color: #eee;
                    color: #4774b3;
                    box-shadow: none;
                    border-radius: 5px;
                    font-size: 18px;
                    padding: 7px 5px;
                    height: auto;
                    max-width: 300px;
                }
                form.lj-login-form input[type="submit"] {
                    padding: 12px 20px;
                    border: 1px solid #3d6399;
                    margin-top: 0;
                    height: auto;
                    font-family: 'Roboto', sans-serif;
                    font-size: 18px;
                    transition: all .25s ease-in-out;
                    background: #3d6399;
                    color: #fff;
                    border-radius: 5px;
                    cursor: pointer;                
                }
                form.lj-login-form .note {
                    display: block;
                    font-size: 14px;
                }
            </style>    -->     

        <form method="post" action="<?php echo site_url(); ?><?php echo $_SERVER['REQUEST_URI']; ?>" class="lj-login-form">
        <?php if( property_exists( $res->data, 'clientId' ) ): ?>
            <input type="hidden" name="clientId" value="<?php echo $res->clientId; ?>" />
        <?php endif; ?> 
        <?php if( !empty( $res ) ): ?>
            <fieldset class="error">
                <?php echo join( '<br />', $res ); ?>
            </fieldset>
        <?php endif; ?>

        <fieldset>
            <label for="new-password">New Password<span class="wpum-required">*</span></label>
            <div class="field required-field">
                <input type="password" name="new-password" autocomplete="off" class="input-text" />
            </div>
        </fieldset>
        <fieldset>
            <label for="new-password-conf">Confirm New Password<span class="wpum-required">*</span></label>
            <div class="field required-field">
                <input type="password" name="new-password-conf" autocomplete="off" class="input-text" />
                <span class="note">Password should be at least 8 characters long, containing a mix of uppercase and lowercase letters, numbers and special characters</span>
            </div>
        </fieldset> 
    <?php
    $o  = ob_get_clean();  
    return $o;    
}

function changeAnswers( $questions, $cQuestions, $cAnswers, $qCount )
{
    ob_start();  ?>
        <fieldset><?php echo __( 'For the security of your account, we require the answers to a few questions.' );
            for( $i=1; $i<=$qCount; $i++ ): 
                $qId    = array_shift( $cQuestions ); 
                $answer = array_shift( $cAnswers ); 
            ?>
                <p>
                    <!--<label for="client_secq<?php echo $i; ?>">Question <?php echo $i; ?><span class="wpum-required">*</span></label>-->
                    <select name="client_secq<?php echo $i; ?>">
                        <?php getOptions( $questions, $qId ); ?>
                    </select>
                    <input type="text" name="client_secq<?php echo $i; ?>_ans" size=45 value="<?php echo $answer; ?>" />
                </p>
            <?php
            endfor; ?>

        </fieldset>
    <?php
        $o  = ob_get_clean();
        return $o;    
}

function checkAnswer( $questions, $cQuestions )
{    
    $qId		= rand( 0, 2 );         // [FIX] - should be based on the required number of questions
    $question	= $questions[$cQuestions[$qId]-1]; 
    ob_start();
    ?>	
        <fieldset>
            <label><?php echo $question; ?> <span class="wpum-required">*</span></label>
            <div class="field required-field">
                <input type="text" class="input-text" name="answer" id="answer" placeholder="" value="" maxlength="" required="">
                <input type="hidden" name="qId" value="<?php echo $qId; ?>" />
            </div>
        </fieldset>
    <?php return ob_get_clean();    
}

function formClose( $wrapper = FALSE )
{
    ob_start(); ?>
            <!-- <input name="change-password" type="submit" class="button button-primary button-large" value="Save">-->
            <input type="submit" name="submit" class="button" value="Save">
        </form>
        <?php if( $wrapper ) echo '</div>'; ?>
    <?php $o    = ob_get_clean();
    return $o;    
}