<?php

function form_trade( $clientId )
{
    ob_start();
    ?>

    <form method="post" action="<?php echo $base_url . '?tab=trade'; ?>">
        <div>
            <label for="account-no">Accounts</label>
            <select name="account-no" id="account-no">
                <?php renderAccountOptions( $clientId ); ?>
            </select>
        </div>
        <div><input type="radio" name="range" value="0" checked="checked">Show 20 most recent</div>
        <div><input type="radio" name="range" value="1" >
            <select name="days">
                <option value="10">Last 10 days</option>
                <option value="30">Last 30 days</option>
                <option value="90">Last 3 months</option>
            </select>
        </div>
        <div><input type="radio" name="range" value="2">
            <select name="year">
                <?php renderYearOptions( $yRange ); ?>
            </select>
            <select name="month">
                <?php renderMonthOptions(); ?>    
            </select>            
        </div>
        <div>
            <input type="submit" name="submit" value="Go" />
        </div>
    </form>
    <?php
    $o  = ob_get_clean();

    echo $o;
}    