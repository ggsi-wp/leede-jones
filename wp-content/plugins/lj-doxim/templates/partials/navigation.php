<?php 
/**
 * Navigation for Doxim documents page
 */

 
?>
<style>
    .doxim-nav {
        display: flex;
        margin: 0;
        padding: 0;
        list-style: none;
        margin-bottom: 15px;
    }
    .doxim-nav li {
        width: 30%;
    }
    .doxim-nav li a {
        background-color: #eee;
        display: inline-block;
        width: 100%;
        padding: 12px;
        border-right: 1px solid #999;
    }
    .doxim-nav li a:last-child {
        border-right: none;
    }
    .doxim-nav li a.active {
        background-color: #ccc;
    }    
</style>
<?php
    if( 'reports' == $tab ): ?>
        <h2>Annual & Quarterly Reports</h2>
<?php
    else: ?>

        <ul class="doxim-nav" >
            <li>
                <a href="<?php echo $base_url  . '?tab=account'; ?>" class="<?php echo $tab == 'account' ? 'active' : ''; ?>">Account Statements</a>
            </li>
            <li>
                <a href="<?php echo $base_url  . '?tab=trade'; ?>" class="<?php echo $tab == 'trade' ? 'active' : ''; ?>">Trade Confirmations</a>
            </li>
            <li>
                <a href="<?php echo $base_url  . '?tab=tax'; ?>" class="<?php echo $tab == 'tax' ? 'active' : ''; ?>">Tax Documents</a>
            </li>        
        </ul>
<?php endif; ?>