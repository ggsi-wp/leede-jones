<?php

function form_reports( $clientId )
{   
    ob_start();
    ?>

    <form method="post" action="<?php echo $base_url . '?tab=reports'; ?>">
        <div>
            <label for="account-no">Accounts</label>
            <select name="account-no" id="account-no">
                <?php renderAccountOptions( $clientId ); ?>
            </select>
        </div>
        <div><input type="radio" name="range" value="0" checked="checked">Show 10 most recent</div>
        <div><input type="radio" name="range" value="1">
            <select name="year">
                <?php renderYearOptions( $yRange ); ?>
            </select>
            <select name="month">
                <?php renderMonthOptions(); ?>    
            </select>            
        </div>
        <div>
            <input type="submit" name="submit" value="Go" />
        </div>
    </form>
<?php
    $o  = ob_get_clean();

    echo $o;
}