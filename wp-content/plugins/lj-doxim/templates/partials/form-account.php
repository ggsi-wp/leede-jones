<?php

function form_account( $clientId )
{   
    ob_start();
    ?>

    <form method="post" action="<?php echo $base_url . '?tab=account'; ?>">
        <div>
            <label for="account-no">Accounts</label>
            <select name="account-no" id="account-no">
                <?php renderAccountOptions( $clientId ); ?>
            </select>
        </div>
        <div><input type="radio" name="range" value="minCount" checked="checked">Show 18 most recent</div>
        <div><input type="radio" name="range" value="byMonth">
            <select name="year">
                <?php renderYearOptions( $yRange ); ?>
            </select>
            <select name="month">
                <?php renderMonthOptions(); ?>    
            </select>            
        </div>
        <div>
            <input type="submit" name="submit" value="Go" />
        </div>
    </form>
<?php
    $o  = ob_get_clean();

    echo $o;
}