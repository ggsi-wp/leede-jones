<?php
/*
Plugin Name: Leede Jones Doxim plugin
Plugin URI: 
Description: A plugin to present Doxim documents
Version: 1.0
Author: John Anderson/Graphically Speaking
Author URI: http://www.graphicallyspeaking.ca
License: GPL2
*/
/*
Copyright 2019  Graphically Speaking  (email : john.anderson@graphicallyspeaking.ca)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as 
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
if( !class_exists( 'LJ_Doxim' ) )
{
    include_once( 'shortcode.php' );
    class LJ_Doxim
    {
        /**
         * Construct the plugin object
         */
        public function __construct()
        {
            add_action( 'admin_init', [&$this, 'admin_init'] );
            add_action( 'admin_menu', [&$this, 'admin_menu'] );
        }

        /**
         * Activate the plugin
         */
        public static function activate()
        {   
        }

        /**
         * Deactivate the plugin
         */
        public static function deactivate()
        {
        }

        /**
         * Hook into WP's admin_init action hook
         */
        public function admin_init()
        {
            $this->init_settings();
        }

        /**
         * Initialize some custom settings
         */
        public function init_settings()
        {
            register_setting( 'lj_doxim-group', 'doxim_bin' );
            register_setting( 'lj_doxim-group', 'doxim_password' );
            register_setting( 'lj_doxim-group', 'doxim_apiurl' );
            register_setting( 'lj_doxim-group', 'accountCount' );
            register_setting( 'lj_doxim-group', 'tradeCount' );
            register_setting( 'lj_doxim-group', 'taxCount' );
            register_setting( 'lj_doxim-group', 'reportsCount' );
            register_setting( 'lj_doxim-group', 'doxim_account_view_id' );
            register_setting( 'lj_doxim-group', 'doxim_trade_view_id' );
            register_setting( 'lj_doxim-group', 'doxim_tax_view_id' );    
            register_setting( 'lj_doxim-group', 'doxim_reports_view_id' );                                 
        }

        /**
         * Admin Menu
         */
        function admin_menu()
        {
            // Add Menu item
            add_submenu_page( 'options-general.php', 'Leede Jones Doxim parameters', 'Doxim parameters', 'manage_options', 'doxim_settings', 'doxim_plugin_options_doxim' );
        }    
        


        
    }
    // Installation and uninstallation hooks
    register_activation_hook( __FILE__, ['LJ_Doxim', 'activate'] );
    register_deactivation_hook( __FILE__, ['LJI_Doxim', 'deactivate'] );

    //instantiate the plugin class
    $lj_doxim = new LJ_Doxim;

    // Add a link to the settings page onto the plugin page
    /*if( isset( $lj_doxim ) )
    {
        // Add the settings link to the plugin page
        function plugin_settings_link( $links )
        {
            $settings_link  = '<a href="options-general.php?page=lj_doxim-group">Settings</a>';
            array_unshift( $links, $settings_link );
            return $links;
        }

        $plugin     = plugin_basename( __FILE__ );
        add_filter( "plugin_action_links_$plugin", 'plugin_settings_link' );
    }*/

    /**
     * Register download template
     */
    add_action( 'template_include', 'download_template' );
    function download_template( $template )
    {
        $plugindir  = dirname( __FILE__ );
        
        if( is_page_template( '/templates/download.php' ) )
        {
            $template   = $plugindir . '/templates/download.php';
        }

        return $template;
    }


    /**
     * Register custom page
     */
    function add_custom_page()
    {   
        $my_post    = [
            'post_title'    => wp_strip_all_tags( 'Doxim documents page' ),
            'post_content'  => '[lj_doxim_page]',
            'post_status'   => 'publish',
            'post_author'   => 1,
            'post_type'     => 'page'
        ];

        wp_insert_post( $my_post );

        if( NULL == get_page_by_title( 'Doxim document download page' ) )
        {
            $my_post    = [
                'post_title'    => wp_strip_all_tags( 'Doxim document download page' ),
                'post_content'  => 'This page is set up for downloading Doxim files.',
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'     => 'page'
            ];
            $post_id    = wp_insert_post( $my_post );
            if( !post_id )
            {
                wp_die( 'Error creating template page');
            }
            else
            {
                update_post_meta( $post_id, '_wp_page_template', '/templates/download.php' );
            }
        }
    }    
    
    function remove_custom_page()
    {
        $page    = get_page_by_title( 'Doxim documents page' );
        wp_delete_post( $page->ID );
        $page    = get_page_by_title( 'Doxim document download page' );
        wp_delete_post( $page->ID );        
    }

    register_activation_hook( __FILE__, 'add_custom_page' );
    register_deactivation_hook( __FILE__, 'remove_custom_page' );

        /**
         * My plugin options
         */
        function doxim_plugin_options_doxim()
        {
            if( !current_user_can( 'manage_options' ) )
            {
                wp_die( __( 'You do not have sufficient permissions to access this page' ) );
            }
            ?>
        
            <div class="wrap">
                <form method="post" action="options.php">
                <?php settings_fields( 'lj_doxim-group' ); ?>
                <?php do_settings_sections( 'lj_doxim-group' ); ?>
                <table class="form-table">
        
                    <tr valign="top">
                        <th scope="row">Bin number</th>
                        <td><input type="text" name="doxim_bin" value="<?php echo esc_attr( get_option('doxim_bin') ); ?>" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Doxim Password</th>
                        <td><input type="password" name="doxim_password" value="<?php echo esc_attr( get_option('doxim_password') ); ?>" /></td>
                    </tr>  
                    <tr valign="top">
                        <th scope="row">Doxim apiUrl</th>
                        <td><input type="text" name="doxim_apiurl" value="<?php echo esc_attr( get_option('doxim_apiurl') ); ?>" /></td>
                    </tr>    
                    <tr valign="top">
                        <th scope="row">Account View Id</th>
                        <td><input type="text" name="doxim_account_view_id" value="<?php echo esc_attr( get_option('doxim_account_view_id') ); ?>" /></td>
                    </tr>   
                    <tr valign="top">
                        <th scope="row">Account Recent Range<br />("Show most recent")</th>
                        <td><input type="text" name="accountCount" value="<?php echo esc_attr( get_option('accountCount') ); ?>" /></td>
                    </tr>                     
                    <tr valign="top">
                        <th scope="row">Trade View Id</th>
                        <td><input type="text" name="doxim_trade_view_id" value="<?php echo esc_attr( get_option('doxim_trade_view_id') ); ?>" /></td>
                    </tr> 
                    <tr valign="top">
                        <th scope="row">Trade Recent Range<br />("Show most recent")</th>
                        <td><input type="text" name="tradeCount" value="<?php echo esc_attr( get_option('tradeCount') ); ?>" /></td>
                    </tr>                      
                    <tr valign="top">
                        <th scope="row">Tax View Id</th>
                        <td><input type="text" name="doxim_tax_view_id" value="<?php echo esc_attr( get_option('doxim_tax_view_id') ); ?>" /></td>
                    </tr>      
                    <tr valign="top">
                        <th scope="row">Tax Recent Range<br />("Show most recent")</th>
                        <td><input type="text" name="taxCount" value="<?php echo esc_attr( get_option('taxCount') ); ?>" /></td>
                    </tr>    
                    <tr valign="top">
                        <th scope="row">Reports View Id</th>
                        <td><input type="text" name="doxim_reports_view_id" value="<?php echo esc_attr( get_option('doxim_reports_view_id') ); ?>" /></td>
                    </tr>      
                    <tr valign="top">
                        <th scope="row">Reports Recent Range<br />("Show most recent")</th>
                        <td><input type="text" name="reportsCount" value="<?php echo esc_attr( get_option('reportsCount') ); ?>" /></td>
                    </tr>                                                                                                                          
                </table>
            
            <?php submit_button(); ?>
        
                </form>
            </div>
        <?php
        }    


}