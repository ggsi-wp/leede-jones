<?php
/**
 * Allow for agencies to override the Nexcess branding.
 */

namespace Nexcess\MAPPS\Support;

use Nexcess\MAPPS\Integrations\Support;

use const Nexcess\MAPPS\PLUGIN_URL;

class Branding {

	/**
	 * Retrieve the company name used throughout the platform's branding.
	 *
	 * @return string The branded company name.
	 */
	public static function getCompanyName() {
		$name = _x( 'Nexcess', 'company name', 'nexcess-mapps' );

		/**
		 * Override the company name used throughout the Nexcess MAPPS platform.
		 *
		 * @param string $name The branded company name.
		 */
		$filtered = apply_filters( 'nexcess_mapps_branding_company_name', $name );

		return self::isNonEmptyString( $filtered ) ? trim( $filtered ) : $name;
	}

	/**
	 * Retrieve the company name as it should appear in the WP Admin Bar.
	 *
	 * @return string The title for the WP Admin Bar.
	 */
	public static function getAdminBarTitle() {
		$name = self::getCompanyName();

		/**
		 * Override the company name used in the WP Admin Bar.
		 *
		 * @param string $name The branded company name.
		 */
		$filtered = apply_filters( 'nexcess_mapps_branding_admin_bar_name', $name );

		return self::isNonEmptyString( $filtered ) ? trim( $filtered ) : $name;
	}

	/**
	 * Retrieve the label for the top-level admin menu item.
	 *
	 * @return string The label of the admin menu.
	 */
	public static function getDashboardMenuItemLabel() {
		$name = self::getCompanyName();

		/**
		 * Override the label for the top-level admin menu item.
		 *
		 * @param string $name The branded company name.
		 */
		$filtered = apply_filters( 'nexcess_mapps_branding_dashboard_menu_item_title', $name );

		return self::isNonEmptyString( $filtered ) ? trim( $filtered ) : $name;
	}

	/**
	 * Retrieve the title of the dashboard page.
	 *
	 * @return string The title for the dashboard page.
	 */
	public static function getDashboardPageTitle() {
		$name = self::getCompanyName();

		/**
		 * Override the title of the dashboard page.
		 *
		 * @param string $name The branded company name.
		 */
		$filtered = apply_filters( 'nexcess_mapps_branding_dashboard_page_title', $name );

		return self::isNonEmptyString( $filtered ) ? trim( $filtered ) : $name;
	}

	/**
	 * Retrieve the <svg> markup for the a single-color logo.
	 *
	 * @param string $color Optional. The default fill color for the SVG icon.
	 *                      Default is "currentColor".
	 *
	 * @return string An inline SVG icon.
	 */
	public static function getCompanyIcon( $color = 'currentColor' ) {
		$icon = self::getNexcessIcon( $color );

		/**
		 * Override the icon branding for the Nexcess MAPPS dashboard.
		 *
		 * @param string $icon  An inline SVG of the branded icon.
		 * @param string $color The SVG color. By default, this will be "currentColor".
		 */
		$filtered = apply_filters( 'nexcess_mapps_branding_company_icon_svg', $icon, $color );

		return self::isNonEmptyString( $filtered ) ? $filtered : $icon;
	}

	/**
	 * Retrieve the company logo used throughout the platform.
	 *
	 * @return string The URL of the logo.
	 */
	public static function getCompanyImage() {
		$logo = PLUGIN_URL . '/nexcess-mapps/assets/img/nexcess-logo.svg';

		/**
		 * Override the company logo image file for the Nexcess MAPPS dashboard.
		 *
		 * @param string $logo A URL for the branded company logo.
		 */
		$filtered = apply_filters( 'nexcess_mapps_branding_company_image', $logo );

		return self::isNonEmptyString( $filtered ) ? $filtered : $logo;
	}

	/**
	 * Retrieve the URL for support requests.
	 *
	 * @return string The support URL.
	 */
	public static function getSupportUrl() {
		$url = Support::SUPPORT_URL;

		/**
		 * Filter the URL for support.
		 *
		 * @param string $url The support URL.
		 */
		$filtered = apply_filters( 'nexcess_mapps_branding_support_url', $url );

		return self::isNonEmptyString( $filtered ) ? trim( $filtered ) : $url;
	}

	/**
	 * Retrieve the <svg> markup for the single-color Nexcess logo.
	 *
	 * @param string $color Optional. The default fill color for the SVG icon.
	 *                      Default is "currentColor".
	 *
	 * @return string An inline SVG version of the single-color Nexcess "N" icon.
	 */
	public static function getNexcessIcon( $color = 'currentColor' ) {
		return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.72 40.17" style="fill: ' . $color . ';"><title>Nexcess</title><path d="M0,8.18A8.18,8.18,0,0,1,13.1,1.64c2.56,1.8,12.73,9.92,12.73,9.92v16L11.67,16.1V39.36H0Z"/><path d="M53.72,32a8.19,8.19,0,0,1-13.1,6.55c-2.56-1.81-12.73-9.92-12.73-9.92v-16L42.05,24.09V.83H53.72Z" /><path d="M38,20.82l1.27-1v-16L27.89,12.63Z"/><path d="M14.4,36.2l11.43-8.68L15.63,19.3l-1.23.93Z"/></svg>';
	}

	/**
	 * Quickly validate that the given value is a non-empty string.
	 *
	 * @param mixed $value The value to check.
	 *
	 * @return bool True if $value is a non-empty string, false otherwise.
	 */
	protected static function isNonEmptyString( $value ) {
		return is_string( $value ) && ! empty( $value );
	}
}
