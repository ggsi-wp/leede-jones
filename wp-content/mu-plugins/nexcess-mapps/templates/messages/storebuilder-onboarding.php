<?php

/**
 * Onboarding email sent to StoreBuilder customers.
 *
 * @global string   $reset_url The password reset URL for this user.
 * @global \WP_User $user      The email recipient.
 */

?>

Hello there!

To access your new store, please visit <?php echo esc_url_raw( $reset_url ); ?> to choose your new, super-secure password.

From there, you can log in with the username "<?php echo esc_html( $user->user_login ); ?>" and your password.

You can also reach the login page at any time by visiting <?php echo esc_url_raw( wp_login_url() ); ?>.
