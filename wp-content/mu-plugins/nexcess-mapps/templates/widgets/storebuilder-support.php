<?php

/**
 * The "StoreBuilder Support" widget.
 */

use Nexcess\MAPPS\Integrations\StoreBuilder;
use Nexcess\MAPPS\Integrations\Support;
use Nexcess\MAPPS\Support\Branding;

?>

<h3><?php esc_html_e( 'Need Help With StoreBuilder?', 'nexcess-mapps' ); ?></h3>

<p><?php echo wp_kses_post( sprintf(
	/* Translators: %1$s is the StoreBuilder marketing URL. */
	__( 'Check out the <a href="%1$s">StoreBuilder website</a> for answers to common questions, such as:', 'nexcess-mapps' ),
	StoreBuilder::MARKETING_URL
) ); ?>
<ul>
	<li><a href="https://nnus-stg.nxswd.net//storebuilder/resources/publishing-going-live/"><?php esc_html_e( 'Publishing Your StoreBuilder Shop To The World', 'nexcess-mapps' ); ?></a></li>
	<li><a href="https://nnus-stg.nxswd.net/storebuilder/resources/customizing-storebuilder-content/"><?php esc_html_e( 'How To Edit Your Individual Content, Text, and Media (Using Kadence Blocks)', 'nexcess-mapps' ); ?></a></li>
	<li><a href="https://nnus-stg.nxswd.net/storebuilder/resources/accepting-major-credit/"><?php esc_html_e( 'Learn about and configure your Payment Gateway to accept major credit cards', 'nexcess-mapps' ); ?></a></li>
</ul>

<p><?php echo wp_kses_post( sprintf(
	/* Translators: %1$s is the Nexcess support URL. */
	__( 'Can\'t find your answer? Get help from the eCommerce experts: the support team at Nexcess. Go to <a href="%1$s">%1$s</a> to get help via email, chat, or phone. We\'re here 24/7, all over the world.', 'nexcess-mapps' ),
	Branding::getSupportUrl()
) );
?></p>
