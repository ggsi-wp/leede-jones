<?php

/**
 * The "Going Live with StoreBuilder" widget.
 */

use Nexcess\MAPPS\Integrations\StoreBuilder;
use Nexcess\MAPPS\Integrations\Support;
use Nexcess\MAPPS\Support\Branding;

?>

<h3><?php esc_html_e( 'Ready To Go Live?', 'nexcess-mapps' ); ?></h3>

<p><?php echo wp_kses_post( sprintf(
	/* Translators: %1$s is the domain name registrar article URL, %2$s is the name servers article URL. */
	__( 'To get your store live on your real store name, you will need to tell your <a href="%1$s">domain name registrar</a> that your store is waiting on a temporary URL on StoreBuilder. To do this, find your domain name registrar\'s information on configuring your <a href="%2$s">name servers</a>. Look through their instructions to find out, then, you’ll want to set them as follows:', 'nexcess-mapps' ),
	'https://www.cloudflare.com/learning/dns/glossary/what-is-a-domain-name-registrar/',
	'https://techterms.com/definition/nameserver'
) ); ?></p>

<pre>ns1.nexcess.net
ns2.nexcess.net
ns3.nexcess.net
ns4.nexcess.net</pre>

<p><?php echo wp_kses_post( sprintf(
	/* Translators: %1$s is the link to the going live article, %2$s is the support URL. */
	__( 'That\'s all there is to it! In 24-48 hours, your store will be live for the world to see. If you run into any problems, check out <a href="%1$s">our full article on going live on StoreBuilder</a> on the StoreBuilder website, or <a href="%2$s">reach out to us</a> and we\'ll be happy to help.', 'nexcess-mapps' ),
	'https://nnus-stg.nxswd.net/storebuilder/resources/publishing-going-live/',
	Branding::getSupportUrl()
) ); ?></p>
