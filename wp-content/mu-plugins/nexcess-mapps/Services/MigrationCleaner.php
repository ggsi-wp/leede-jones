<?php

/**
 * This service is responsible for cleaning up leftover MU plugins, configurations, etc. that were
 * carried over during migrations.
 *
 * For each [previous] host, we establish a set of "check" files, meant to distinguish one host
 * from another (for example, another host is unlikely to have "mu-plugins/liquid-web.php"). If
 * one of the check files is found, we'll clean up known files/directories from those hosts.
 */

namespace Nexcess\MAPPS\Services;

use Nexcess\MAPPS\Exceptions\FilesystemException;
use WP_Filesystem_Base;

class MigrationCleaner {

	/**
	 * The WordPress filesystem object.
	 *
	 * @var \WP_Filesystem_Base
	 */
	protected $fs;

	/**
	 * Create a new instance of the MigrationCleaner service.
	 *
	 * @param \WP_Filesystem_Base $filesystem A WP_Filesystem instance.
	 */
	public function __construct( WP_Filesystem_Base $filesystem ) {
		$this->fs = $filesystem;
	}

	/**
	 * Remove the given $paths from the filesystem.
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\FilesystemException if any files are unable to be removed.
	 *
	 * @param string[] $paths The files and/or directories to remove, relative to WP_CONTENT_DIR.
	 */
	public function remove( array $paths ) {
		$missed = [];
		rsort( $paths );

		foreach ( $paths as $path ) {
			$path = $this->fs->wp_content_dir() . $path;

			try {
				$this->deletePath( $path );
			} catch ( FilesystemException $e ) {
				$missed[] = $path;
			}
		}

		if ( ! empty( $missed ) ) {
			throw new FilesystemException( sprintf(
				"One or more files could not be deleted:\n- %s",
				implode( "\n- ", $missed )
			) );
		}
	}

	/**
	 * Scan the local filesystem for leftovers. No files will be removed.
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\FilesystemException if an error occurs.
	 *
	 * @return array[] An array of detected files, grouped by host.
	 */
	public function scan() {
		$definitions = $this->getDefinitions();
		$found       = [];

		foreach ( $definitions as $platform => $definition ) {
			// If no checkfile exists, continue onto the next platform.
			if ( ! $this->checkFilesFound( $definition['check'] ) ) {
				continue;
			}

			// Filter the list to those files that actually exist.
			$found[ $platform ] = array_filter( $definition['paths'], function ( $path ) {
				return $this->fs->exists( $this->fs->wp_content_dir() . $path );
			} );

			rsort( $found[ $platform ] );
		}

		return $found;
	}

	/**
	 * Given an array of file paths relative to WP_CONTENT_DIR, determine if at least one of the
	 * files exists.
	 *
	 * @param string[] $paths Filepaths to check.
	 *
	 * @return bool True if at least one of the files was found, false otherwise.
	 */
	protected function checkFilesFound( array $paths ) {
		foreach ( $paths as $path ) {
			if ( $this->fs->exists( $this->fs->wp_content_dir() . $path ) ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Delete a single path.
	 *
	 * This is a wrapper around WP_Filesystem_Base::delete(), but will attempt to automatically
	 * remove empty directories.
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\FilesystemException if a file exists but cannot be deleted.
	 *
	 * @param string $path The path to remove.
	 * @param string $type Optional. One of "d" ("Directory") or "f" ("File"). Default is empty.
	 *
	 * @return bool True if the file was deleted.
	 */
	protected function deletePath( $path, $type = '' ) {
		if ( ! in_array( $type, [ 'd', 'f' ], true ) ) {
			$type = false;
		}

		// The file doesn't exist, so there's nothing to do.
		if ( $this->fs->exists( $path ) ) {
			if ( ! $this->fs->delete( $path, true, $type ) ) {
				throw new FilesystemException( sprintf( 'Unable to delete %s.', $path ) );
			}
		}

		// If the parent directory is empty, recursively try to delete it.
		$dirname = untrailingslashit( dirname( $path ) );

		if ( empty( $this->fs->dirlist( $dirname, false ) ) && $this->fs->wp_content_dir() !== $dirname ) {
			return $this->deletePath( $dirname, 'd' );
		}

		return true;
	}

	/**
	 * Retrieve an array of known leftovers, grouped by host.
	 *
	 * @return array[] {
	 *   Leftover files, grouped by host.
	 *
	 *   @type string[] $check An array of one or more files to test; at least one of these files
	 *                        must exist before the service will attempt to clean anything up.
	 *   @type string[] $paths File and directory paths, relative to WP_CONTENT_DIR, to be
	 *                        found and removed.
	 * }
	 */
	protected function getDefinitions() {
		return [
			'Kinsta'         => [
				'check' => [
					'mu-plugins/kinsta-mu-plugins.php',
				],
				'paths' => [
					'mu-plugins/kinsta-mu-plugins/',
					'mu-plugins/kinsta-mu-plugins.php',
				],
			],
			'Liquid Web MWX' => [
				'check' => [
					'mu-plugins/000-liquidweb-config.php',
					'mu-plugins/liquid-web.php',
					'mu-plugins/liquidweb_mwp.php',
				],
				'paths' => [
					'mu-plugins/000-liquidweb-config.php',
					'mu-plugins/liquid-web.php',
					'mu-plugins/liquid-web/',
					'mu-plugins/liquidweb_mwp.php',
					'mu-plugins/lw_disable_nags.php',
					'mu-plugins/lw-varnish-cache-purger.php',
					'mu-plugins/plugin-reports/package.php',
					'mu-plugins/plugin-reports/src/gather.php',
					'mu-plugins/plugin-reports/src/init.php',
					'mu-plugins/plugin-reports/src/models.php',
					'mu-plugins/plugin-reports/src/plugin-stats/woocommerce.php',
					'mu-plugins/plugin-reports/src/send-report.php',
					'mu-plugins/plugin-reports/src/utils.php',
					'mu-plugins/wp-cli-packages/regression_urls_command.php',
					'mu-plugins/wp-cli-packages/varnish_purge_command.php',
				],
			],

			'Pagely'         => [
				'check' => [
					'mu-plugins/pagely-assets/',
					'mu-plugins/pagely-cache-control/',
					'mu-plugins/pagely-management-v2.php',
				],
				'paths' => [
					'mu-plugins/pagely-app-stats/',
					'mu-plugins/pagely-assets/',
					'mu-plugins/pagely-cache-control/',
					'mu-plugins/pagely-cache-purge/',
					'mu-plugins/pagely-cdn/',
					'mu-plugins/pagely-cli/',
					'mu-plugins/pagely-plugin-upgrade-hooks/',
					'mu-plugins/pagely-security-patches/',
					'mu-plugins/pagely-site-health/',
					'mu-plugins/pagely-status/',
					'mu-plugins/pagely-util/',
					'mu-plugins/pagely-wp-cli-mail-patch/',
					'mu-plugins/pagely-management-v2.php',
				],
			],

			/*
			 * @link https://wpengine.com/support/platform-settings/#WP_Engine_MU_Plugins
			 */
			'WP Engine'      => [
				'check' => [
					'mu-plugins/wpe-wp-sign-on-plugin.php',
					'mu-plugins/wpengine-common/',
					'mu-plugins/wpengine-security-auditor.php',
				],
				'paths' => [
					'mu-plugins/force-strong-passwords/',
					'mu-plugins/mu-plugin.php',
					'mu-plugins/slt-force-strong-passwords.php',
					'mu-plugins/stop-long-comments.php',
					'mu-plugins/wpe-wp-sign-on-plugin/',
					'mu-plugins/wpe-wp-sign-on-plugin.php',
					'mu-plugins/wpengine-common/',
					'mu-plugins/wpengine-security-auditor.php',
				],
			],
		];
	}
}
