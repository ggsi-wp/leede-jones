# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## Version 1.16.4 — 2021-02-10

### Fixed

* Further fixes around the licensing of premium Kadence plugins on StoreBuilder sites


## Version 1.16.3 — 2021-02-09

### Updated

* Use the Robots API (WordPress 5.7+) for adding noindex to support users
* Warn StoreBuilder customers if the ingestion process did not complete successfully
* Update the email address pattern used for support users

### Fixed

* Fixed a bug where Kadence plugins weren't being licensed during the first setup run
* Delay the sending of the StoreBuilder welcome email until after ingestion has completed
* Automatically re-create Cache Enabler directories after the cache is cleared


## Version 1.16.2 — 2021-02-03

### Updated

* Pre-install Astra as part of the setup script on MWCH sites

### Fixed

* Ensure the Kadence Pro theme add-on gets licensed
* Fixed a bug where MAPPS flags weren't being cached
* Ensure Kadence, not Astra, is the default theme for StoreBuilder sites
* Fixed an issue where the WordPress database wasn't being updated during `nxmapps setup`


## Version 1.16.1 — 2021-02-02

### Updated

* Update the StoreBuilder welcome screen and widgets with the latest content
* Install Kadence Pro as part of StoreBuilder site setup
* Hide the Redis Cache dashboard widget by default on StoreBuilder
* Prevent the Instagram Feed plugin from filling the dashboard with nags
* Hide the Recapture Promotion for StoreBuilder customers
* Send the password reset as part of StoreBuilder setup

### Fixed

* Automatically update the WordPress database in the setup script after updating core
* Fixed issue where Kadence Blocks Pro wasn't pre-activating
* Force Kadence as the default theme for StoreBuilder sites
* Fixed an issue where the site name + navigation weren't appearing on StoreBuilder sites


## Version 1.16.0 — 2021-02-02

### Added

* Introduce a PSR-11 Dependency Injection (DI) container
* Define a base WP-CLI command and resolve commands through the DI container
* Initial public release of the StoreBuilder integration
* Update WordPress core as part of the `nxmapps setup` command
* Automatically clean up migration artifacts from Kinsta, Pagely, and WP Engine
* Check visual regression URLs from the UI
* Introduce a `ConsoleCommand` support class, ensuring all WP-CLI commands invoked from within the MU plugin are wrapped with "nice" and "timeout"
* Attempt to insert missing anchors in the site's wp-config.php file
* Prevent Kadence from hijacking WP-Admin
* Add a temporary cleanup task to the PageCache integration

### Updated

* Split page cache functionality into a separate integration
* Rewrite the Visual Comparisons UI in React
* Hide the Redis Cache Pro dashboard widget by default
* Block search engines from indexing support users
* Update Cache Enabler rewrite rules and defaults, including the exclusion of Event Calendar Pro's `/events/*` path from Cache Enabler by default
* Update the namespace used for WPConfigTransformer
* Change the "Get Support" link in WooCommerce Admin
* Don't load the Feedback integration if disabled via filter
* Raise the default MAPPS API timeout to 30 seconds
* Move WooCommerce defaults into the WooCommerce integration
* Update Nexcess MAPPS Dashboard to 1.3.6
* Update wp-fail2ban to 4.3.0.9

### Fixed

* Ensure the `StagingSites` integration adds its hooks on staging sites
* Verify that wp-admin/includes/file.php is loaded before calling `WP_Filesystem()`
* Support both "wp_package" and "wp-package" in the Installer service


## Version 1.15.3 — 2020-12-22

### Updated

* Split full-page caching out of the `Cache` integration into `PageCache`
* Use the most recent setting keys and Htaccess rules for Cache Enabler

### Fixed

* Ensure default permalink structures are set on new sites
* Ensure that StagingSites' constants are actually being set


## Version 1.15.2 — 2020-12-09

### Fixed

* Bypass the cache to verify the current environment in the `StagingSites` and `RegressionSites` integrations


## Version 1.15.1 — 2020-11-24

### Added

* Display a notice encouraging users running WooCommerce, EDD, or RCP to install Recapture

### Fixed

* Fixed an issue where the `WPConfigTransformer` class was not loading properly.


## Version 1.15.0 — 2020-11-16

### Added

* Enable support to generate self-destructing users

### Updated

* Disable the "plugin_theme_auto_updates" Site Health check when updates are handled by MAPPS

### Fixed

* Check for `wp_is_maintenance_mode()` before calling it in `FatalErrorHandler::handle()`


## Version 1.14.0 — 2020-11-12

### Added

* Add a custom `fatal-error-handler.php` drop-in
* [Introduce new filters to replace Nexcess branding](README.md#change-the-branding-and-display-names-in-the-plugin)
* Add a notice under the auto-update UI (introduced in WordPress 5.6) if core updates are being handled by MAPPS
* [Enable the `DisplayEnvironment` integration to be disabled via filter](README.md#disable-the-environment-indicator)

### Updated

* Perform platform checks as early as possible
* Rotate the `WP_CACHE_KEY_SALT` constant on staging sites
* Explicitly set the `WP_ENVIRONMENT_TYPE` constant on staging sites
* Move `AdminNotice` under the `Nexcess\MAPPS\Support` namespace
* Soften the colors used to distinguish environments in the WP Admin Bar
* Updated Nexcess MAPPS Dashboard to version 1.3.5


## Version 1.13.1 — 2020-10-21

### Fixed

* Resolved ambiguous `Cache` reference within `Nexcess\MAPPS\Commands\Setup`


## Version 1.13.0 — 2020-10-13

### Added

* Introduce a UI for controlling visual regression URLs
* Add support for [Redis Cache](https://wordpress.org/plugins/redis-cache/)
* Enable the plugin to programmatically update `wp-config.php`
* Add a "Flush PHP OPcache" button the the admin bar
* Display the current environment in the admin bar
* Add common support articles to the support tab
* Add public-facing documentation to `nexcess-mapps/README.md`

### Updated

* Fire the `Telemetry::REPORT_CRON_ACTION` during site setup
* Install Redis Cache, not WP Redis, by default during site setup
* Separate the various cache integrations
* Rename the Security integration to Fail2Ban


## Version 1.12.3 — 2020-10-01

### Updated

* Disable the MU plugin for unsupported versions of WordPress
* Upgraded Nexcess MAPPS Dashboard to version 1.3.4
* Upgraded wp-fail2ban to version 4.3.0.8


## Version 1.12.2 — 2020-09-23

### Updated

* Respect customers' `wp_get_environment_type()` configuration
* Let customers manually update WordPress core
* Update the links on the support tab to point to the resolved URLs
* Upgraded Nexcess MAPPS Dashboard to version 1.3.3


## Version 1.12.1 — 2020-09-17

### Updated

* Disable the "WordPress X.X is available! Please notify the site administrator" messages
* Disable Sucuri scans on regression sites

### Fixed

* Prevent uninitialized offset notices in wp-fail2ban


## Version 1.12.0 — 2020-09-15

### Added

* Introduce a top-level "Nexcess" menu in WP-Admin
* Add a "Delete expired transients" button to the Nexcess Admin Bar menu
* Filter Site Health checks for MAPPS sites
* Include Nexcess information in the WP-Admin footer
* Introduce an `Integration::boot()` method, enabling specific code to *always* be run
* Reduce randomness on regression sites to produce more consistent results
* Add scaffolding around deprecating functionality

### Updated

* Replace custom autoloaders with those generated by Composer
* Reduce the Siteworx cache time from 1hr to 5min
* Replace the `wp_mail()` function on regression sites to prevent emails from getting out
* Rename the `Dashboard` integration to `PluginInstaller`
* Removed the `WP_ENVIRONMENT_TYPES` environment variable, first defined in v1.11.0
* Nexcess MAPPS Dashboard has been upgraded to to 1.3.1


## Version 1.11.2 — 2020-08-13

### Updated

* Reduce the SiteWorx cache time from 1 hour to 5 minutes


## Version 1.11.1 — 2020-08-07

### Added

* Introduce a new Recapture integration


## Version 1.11.0 — 2020-08-06

### Added

* Introduce a simple autoloader for WordPress core classes referenced within the MU plugin
* Add a new WooCommerce integration class
  - Currently, the only task it's responsible for is disabling background image regeneration
* Add a `nexcess_mapps_disable_dashboard` filter to enable site owners to disable the Nexcess MAPPS Dashboard installer
* Define `WP_ENVIRONMENT_TYPE` and `WP_ENVIRONMENT_TYPES` environment variables ahead of WordPress 5.5

### Fixed

* Handle deprecated PHPMailer locations ahead of WordPress 5.5
* Be more judicious in setting `JETPACK_STAGING_MODE` on non-production sites
* Revert the temporary change made for the 1.10.1 hotfix release
* Use `wp_using_ext_object_cache()`, not `WP_CACHE` to determine if an external object cache is being used

### Updated

* Abstract the calculation of PHP EOL dates
* Removed the now-unnecessary Maintenance integration
* Remove the code that would clean up stand-alone copies of the Nexcess Dashboard (installer) plugin
* Move the telemetry reporter key into the Settings object
* Lazy-loading of individual settings
* Include "Protect" among the default Jetpack modules
* Disable automatic core + plugin updates through WordPress 5.5+ if plugin updates are enabled via MAPPS
* Remove the WP-fail2ban dashboard widget (introduced in 4.3)
* Put Jetpack into offline/development mode for sites on temp domains


## Version 1.10.1 — 2020-08-04

### Updated

* Disable theme and plugin updates during site setup #316


## Version 1.10.0 — 2020-06-25

### Added

* Introduced a new `Nexcess\MAPPS\Services\Installer` class, which provides a standard API for installing and licensing plugins on a site

### Updated

* MU plugins are now included in the telemetry reports
* Removed the "background_updates" Site Health status check, as the platform is responsible for these updates
* Extracted the Canny board token into the Settings object
* The `nxmapps setup` command will now attempt to pre-install (and license) plugins on a site based on the plan


## Version 1.9.1 — 2020-06-03

### Fixed

* Move the check for the block editor into the `adminEnqueueScripts()` method for the StoreBuilder integration


## Version 1.9.0 — 2020-05-29

### Added

* Introduce the `nxmapps cache` WP-CLI command

### Fixed

* Disable the StoreBuilder integration if the block editor (a.k.a. Gutenberg) is disabled
* Automatically install/remove `object-cache.php` based on WP Redis' state

### Updated

* Automatically update all plugins and themes as part of the `nxmapps setup` WP-CLI command
* Explicitly instruct Cache Enabler not to bypass the page cache if only `utm_*` query string parameters are present
* Nexcess MAPPS Dashboard has been updated to version 1.3.0


## Version 1.8.1 — 2020-05-21

### Updated

* Consider the value of the "beta_client" environment flag when determining the "is_beta_tester" setting


## Version 1.8.0 — 2020-05-20

### Added

* Introduce the StoreBuilder integration for beta customers
* Added the ability to solicit feedback from beta customers

### Fixed

* Fixed a logic issue that was preventing the `wp nxmapps setup` script from aborting if not on a MAPPS site
* Prevent the Nexcess MAPPS Dashboard plugin from loading on sites running WordPress < 5.0
* Prepare for WP fail2ban version 4.3 (forward compatibility)
* Only return URLs which return a 200 response code for Visual Comparison

### Updated

* Abort `wp nxmapps setup:woocommerce` with a warning, not an error, if WooCommerce is inactive


## Version 1.7.1 — 2020-05-09

### Added

* Allow Beta Tester flag to be overridden using a Constant `NEXCESS_MAPPS_BETA_TESTER`

### Fixed

* Don't attempt to call `new PHPMailer()` before the `PHPMailer` class is loaded


## Version 1.7.0 — 2020-05-08

### Added

* Enable developers to use a development copy of Nexcess MAPPS Dashboard
* Remember dismissed admin notices
* Add a WP-CLI command for licensing WP All Import Pro
* Add logic for the "is_beta_tester" setting
* Add a new Themes integration

### Updated

* Update plugin versions being loaded via Composer
* Nexcess MAPPS Dashboard has been updated to version 1.2.1

### Fixed

* Return early from the `nxmapps setup` command on non-MAPPS sites
* Unset unused integration objects
* Fix the EOL dates for PHP 7.3 and 7.4
* Don't load the MU plugin if `WP_INSTALLING`
* Prevent emails from being sent on regression sites
* Remove single attachment pages from the list of visual comparison URLs if the attachment page is redirected elsewhere


## Version 1.6.0 - 2020-02-28

### Added

* Add a `nxmapps dokan` WP-CLI command for licensing Dokan

### Updated

* Include v1.2.0 of the Nexcess MAPPS Dashboard plugin


## Version 1.5.0 - 2020-02-20

### Added

* Add the `nxmapps qubely` WP-CLI commands for managing Qubely Pro licenses
* Introduce a new Maintenance integration, which can be used to run weekly cleanup/maintenance scripts

### Fixed

* Interpret "M" as minutes, not months when creating a `DateInterval`


## Version 1.4.0 - 2020-02-12

### Added

* Install WP-fail2ban by default
* Add a new Debug integration

### Fixed

* Fix method used to determine MySQL version to be in line with WordPress core for debugging
* Use `set_site_transient()` instead of `wp_cache_set()` when caching Siteworx environment details, increase cache time to one hour
* Adjust spacing around Nexcess icon in the admin bar
* Only register the 'flush-object-cache' button if `WP_CACHE` is true

### Updated

* Add additional debug information to the `wp nxmapps details` command


## Version 1.3.1 - 2020-01-30

### Fixed

* Ensure the telemetry integration's API key is passed along with the report


## Version 1.3.0 — 2020-01-28

### Added

* Register a cron event for the telemetry report
* Add the `ICONIC_DISABLE_DASH` constant for disabling the Iconic plugins' dashboard
* Register a WP-CLI command for handling iThemes licensing
* Add a "Nexcess" WP Admin bar, along with a "Flush cache" button
* Display a warning to administrators on sites running old versions of PHP
* Bundle the Nexcess MAPPS Dashboard plugin

### Updated

* Rewrote the `wp nxmapps vc urls` WP-CLI command
* Remove the Cache Enabler Htaccess modifications when the plugin is deactivated
* Verify WooCommerce is installed + active before attempting to install
* Adjust Nexcess MAPPS plan codes
* Only load the full Jetpack integration on legacy plans and mwc.enterprise


## Version 1.2.0 - 2019-12-04

### Added

* Set default Cache Enabler settings

### Updated

* Track WooCommerce store currencies
* Check for `.flush-cache` files in the web root
* Convert changelog to follow [the Keep a Changelog standards](https://keepachangelog.com/en/1.0.0/)
* Set additional WooCommerce default settings


## Version 1.1.0 - 2019-11-21

### Added

* Create a WP CLI command to assist in Brainstorm plugin licensing
* Register the Object Cache integration

### Updated

* Cache the SiteWorx account configuration
* Include the overall product counts in WooCommerce telemetry data


## Version 1.0.0 - 2019-10-29

Initial release of the plugin.

### Added

* WP-CLI Commands
  * AffiliateWP License Activation / Deactivation
  * Platform Setup Command
  * Visual Comparison Page Listings
  * Regenerate `WP_CACHE_KEY_SALT`
* Plugin Integrations
  * Jetpack
  * PHPCompatability Checker
  * Staging Sites
  * Telemetry
  * Varnish
* Core Integrations
  * Core Updates
* Platform Features
  * WooCommerce Upper Limits - Restricts orders and products for our WooCommerce Basic plan.
