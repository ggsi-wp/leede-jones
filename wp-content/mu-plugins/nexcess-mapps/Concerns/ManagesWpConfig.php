<?php

namespace Nexcess\MAPPS\Concerns;

use Nexcess\MAPPS\Exceptions\ConfigException;
use Nexcess\Vendor\WPConfigTransformer;

trait ManagesWpConfig {

	/**
	 * @var \Nexcess\Vendor\WPConfigTransformer
	 */
	private $wpConfigTransformer;

	/**
	 * Add or update an existing configuration.
	 *
	 * This is a wrapper around WPConfigTransformer::update() with better error handling.
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\ConfigException if the configuration cannot be written.
	 *
	 * @param string  $type    The type of configuration.
	 * @param string  $name    The configuration name.
	 * @param string  $value   The configuration value.
	 * @param mixed[] $options Optional. Adjustments to write behavior. Default is empty.
	 */
	public function setConfig( $type, $name, $value, array $options = [] ) {
		try {
			$this->getWpConfigTransformer()->update( $type, $name, (string) $value, $options );
		} catch ( \Exception $e ) {
			if ( 'Unable to locate placement anchor.' !== $e->getMessage() ) {
				throw new ConfigException( $e->getMessage(), $e->getCode(), $e );
			}

			// If the problem was a missing anchor, try to remedy the situation.
			try {
				$this->restoreMissingAnchor();
			} catch ( \Exception $e ) {
				throw new ConfigException( sprintf(
					'Unable to add missing anchor to wp-config.php file: %s',
					$e->getMessage()
				), $e->getCode(), $e );
			}

			$this->setConfig( $type, $name, $value, $options );
		}
	}

	/**
	 * Add (or update) the given constant in wp-config.php.
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\ConfigException if the configuration cannot be written.
	 *
	 * @param string $constant The constant name.
	 * @param mixed  $value    The constant value.
	 */
	public function setConfigConstant( $constant, $value ) {
		$options = [
			'add'       => true,
			'raw'       => false,
			'normalize' => true,
		];

		if ( is_bool( $value ) ) {
			$options['raw'] = true;
			$value          = $value ? 'true' : 'false';
		}

		try {
			$this->setConfig( 'constant', $constant, $value, $options );
		} catch ( ConfigException $e ) {
			throw new ConfigException( $e->getMessage(), $e->getCode(), $e );
		}
	}

	/**
	 * Remove the given constant from wp-config.php.
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\ConfigException if the configuration cannot be written.
	 *
	 * @param string $constant The constant name.
	 */
	public function removeConfigConstant( $constant ) {
		try {
			$this->getWpConfigTransformer()->remove( 'constant', $constant );
		} catch ( \Exception $e ) {
			throw new ConfigException( $e->getMessage(), $e->getCode(), $e );
		}
	}

	/**
	 * Determine whether or not the given constant exists in wp-config.php.
	 *
	 * @param string $constant The constant name.
	 *
	 * @return bool True if the constant is defined, false otherwise.
	 */
	public function hasConfigConstant( $constant ) {
		try {
			$exists = $this->getWpConfigTransformer()->exists( 'constant', $constant );
		} catch ( \Exception $e ) {
			$exists = false;
		}

		return $exists;
	}

	/**
	 * Add (or update) the given variable in wp-config.php.
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\ConfigException if the configuration cannot be written.
	 *
	 * @param string $variable The variable name.
	 * @param mixed  $value    The variable value.
	 */
	public function setConfigVariable( $variable, $value ) {
		$options = [
			'add'       => true,
			'raw'       => false,
			'normalize' => true,
		];

		if ( is_bool( $value ) ) {
			$options['raw'] = true;
			$value          = $value ? 'true' : 'false';
		} elseif ( is_array( $value ) ) {
			$options['raw'] = true;
			$value          = var_export( $value, true ); // phpcs:ignore WordPress.PHP.DevelopmentFunctions.error_log_var_export
		}

		try {
			$this->setConfig( 'variable', $variable, $value, $options );
		} catch ( ConfigException $e ) {
			throw new ConfigException( $e->getMessage(), $e->getCode(), $e );
		}
	}

	/**
	 * Remove the given variable from wp-config.php.
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\ConfigException if the configuration cannot be written.
	 *
	 * @param string $variable The variable name.
	 */
	public function removeConfigVariable( $variable ) {
		try {
			$this->getWpConfigTransformer()->remove( 'variable', $variable );
		} catch ( \Exception $e ) {
			throw new ConfigException( $e->getMessage(), $e->getCode(), $e );
		}
	}

	/**
	 * Determine whether or not the given variable exists in wp-config.php.
	 *
	 * @param string $variable The variable name.
	 *
	 * @return bool True if the variable is defined, false otherwise.
	 */
	public function hasConfigVariable( $variable ) {
		try {
			$exists = $this->getWpConfigTransformer()->exists( 'variable', $variable );
		} catch ( \Exception $e ) {
			$exists = false;
		}

		return $exists;
	}

	/**
	 * Attempt to restore a missing anchor in the wp-config.php file.
	 *
	 * The WPConfigTransformer class relies on an "anchor", a particular string, in order to adjust
	 * configuration; by default, the anchor is a newline character followed by the comment
	 * "/* That's all, stop editing!".
	 *
	 * The transformer will not add anything after the anchor, as this is reserved for defining
	 * ABSPATH and loading the "wp-settings.php" file.
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\ConfigException if the anchor cannot be added.
	 *
	 * @return bool True if the anchor was added, false if it already exists.
	 */
	protected function restoreMissingAnchor() {
		$path   = ABSPATH . 'wp-config.php';
		$anchor = PHP_EOL . '/* That\'s all, stop editing!';
		$insert = $anchor . ' Happy publishing. */' . PHP_EOL;

		try {
			// phpcs:ignore WordPress.WP.AlternativeFunctions.file_get_contents_file_get_contents
			$config = (string) file_get_contents( $path );

			// The anchor already exists, nothing to do.
			if ( false !== mb_strpos( $config, $anchor ) ) {
				return false;
			}

			// Find the ABSPATH definition.
			$pattern = '/(?:if\s*\(\s*!\s*)?(?:defined\(.+\))?\s*\{?(?:\|\|)?\s*define\(\s*["\']ABSPATH["\']/';

			if ( ! preg_match( $pattern, $config, $abspath ) ) {
				throw new ConfigException( 'Unable to find the ABSPATH definition' );
			}

			// Insert the anchor just before the ABSPATH definition.
			$config = str_replace( $abspath[0], $insert . $abspath[0], $config );

			// phpcs:ignore WordPress.WP.AlternativeFunctions.file_system_read_file_put_contents
			if ( false === file_put_contents( $path, $config ) ) {
				throw new ConfigException( 'Unable to write to wp-config.php file' );
			}
		} catch ( \Exception $e ) {
			throw new ConfigException( $e->getMessage(), $e->getCode(), $e );
		}

		return true;
	}

	/**
	 * Get the WPConfigTransformer instance.
	 *
	 * @return \Nexcess\Vendor\WPConfigTransformer
	 */
	private function getWpConfigTransformer() {
		if ( empty( $this->wpConfigTransformer ) ) {
			$this->wpConfigTransformer = new WPConfigTransformer( ABSPATH . 'wp-config.php' );
		}

		return $this->wpConfigTransformer;
	}
}
