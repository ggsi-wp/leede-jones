<?php

/**
 * A PSR-11 implementation of a Dependency Injection (DI) container.
 *
 * Note that the official interface, psr/container, requires PHP >= 7.2.0, so we can't explicitly
 * implement the interface until the minimum version of PHP on our plans has been raised.
 */

namespace Nexcess\MAPPS;

use Nexcess\MAPPS\Exceptions\ContainerException;
use Nexcess\MAPPS\Exceptions\ContainerNotFoundException;
use Nexcess\MAPPS\Integrations\Integration;

class Container {

	/**
	 * Any extensions that have been applied via $this->extend().
	 *
	 * @var mixed[]
	 */
	protected $extensions = [];

	/**
	 * Resolved instances.
	 *
	 * @var mixed[]
	 */
	protected $resolved = [];

	/**
	 * Finds an entry of the container by its identifier and returns it.
	 *
	 * @param string $id Identifier of the entry to look for.
	 *
	 * @throws ContainerNotFoundException  No entry was found for **this** identifier.
	 * @throws ContainerException          Error while retrieving the entry.
	 *
	 * @return mixed Entry.
	 */
	public function get( $id ) {
		if ( ! isset( $this->resolved[ $id ] ) ) {
			$this->resolved[ $id ] = $this->make( $id );
		}

		return $this->resolved[ $id ];
	}

	/**
	 * Returns true if the container can return an entry for the given identifier.
	 * Returns false otherwise.
	 *
	 * `has($id)` returning true does not mean that `get($id)` will not throw an exception.
	 * It does however mean that `get($id)` will not throw a `ContainerNotFoundException`.
	 *
	 * @param string $id Identifier of the entry to look for.
	 *
	 * @return bool
	 */
	public function has( $id ) {
		$config = $this->getConfiguration();

		return isset( $config[ $id ] )
			|| ( class_exists( $id ) && is_subclass_of( $id, Integration::class, true ) );
	}

	/**
	 * Determine whether or not we have a resolved instance of the given identifier.
	 *
	 * @param string $id Identifier of the entry to look for.
	 *
	 * @return bool True if we have a cached instance, false otherwise.
	 */
	public function hasResolved( $id ) {
		return isset( $this->resolved[ $id ] );
	}

	/**
	 * Make a new instance of the given identifier.
	 *
	 * @throws ContainerNotFoundException  No entry was found for **this** identifier.
	 * @throws ContainerException          Error while retrieving the entry.
	 *
	 * @param string $id The identifier to construct.
	 *
	 * @return mixed
	 */
	public function make( $id ) {
		$config = $this->getConfiguration();

		try {
			// We have an explicit definition.
			if ( array_key_exists( $id, $config ) ) {
				if ( null === $config[ $id ] ) {
					return new $id();
				}

				return call_user_func( $config[ $id ], $this, $id );
			}
		} catch ( \Exception $e ) {
			throw new ContainerException(
				sprintf( 'Unable to build %1$s: %2$s', $id, $e->getMessage() ),
				$e->getCode(),
				$e
			);
		}

		// If we haven't returned yet, we couldn't find a definition.
		throw new ContainerNotFoundException(
			sprintf( 'Unable to find a definition for %1$s.', $id )
		);
	}

	/**
	 * Create or replace an existing definition.
	 *
	 * @param string        $id       The identifier.
	 * @param callable|null $callback The callback used to build the service.
	 *
	 * @return self
	 */
	public function extend( $id, $callback ) {
		$this->extensions[ $id ] = $callback;

		return $this->forget( $id );
	}

	/**
	 * Forget any cached instance of the given $id.
	 *
	 * @param string $id The ID to forget.
	 */
	public function forget( $id ) {
		unset( $this->resolved[ $id ] );

		return $this;
	}

	/**
	 * Definitions for all entries registered within in the container.
	 *
	 * For classes that can be constructed directly, pass NULL as the value.
	 *
	 * @return mixed[] An array of keys mapped to callables that define how objects should be
	 *                 constructed (or NULL if objects have no dependencies).
	 */
	public function getConfiguration() {
		return array_merge( [
			AdminBar::class                            => null,
			Plugin::class                              => function ( $app ) {
				return new Plugin( $app, $app->get( Settings::class ) );
			},
			Settings::class                            => null,

			// Commands.
			Commands\AffiliateWP::class                => null,
			Commands\BrainstormForce::class            => null,
			Commands\Cache::class                      => null,
			Commands\Config::class                     => null,
			Commands\Dokan::class                      => null,
			Commands\iThemes::class                    => null,
			Commands\Qubely::class                     => null,
			Commands\Setup::class                      => function ( $app ) {
				return new Commands\Setup(
					$app->get( Settings::class ),
					$app->get( Services\Installer::class ),
					$app->get( Integrations\WooCommerce::class )
				);
			},
			Commands\StoreBuilder::class               => function ( $app ) {
				return new Commands\StoreBuilder(
					$app->get( Settings::class ),
					$app->get( Integrations\StoreBuilder::class )
				);
			},
			Commands\Support::class                    => null,
			Commands\VisualComparison::class           => null,
			Commands\WPAllImportPro::class             => null,

			// Integrations.
			Integrations\Admin::class                  => function ( $app ) {
				return new Integrations\Admin(
					$app->get( Settings::class ),
					$app->get( AdminBar::class )
				);
			},
			Integrations\Cache::class                  => null,
			Integrations\Cron::class                   => null,
			Integrations\Dashboard::class              => [ $this, 'buildIntegration' ],
			Integrations\Debug::class                  => null,
			Integrations\DisplayEnvironment::class     => null,
			Integrations\ErrorHandling::class          => [ $this, 'buildIntegration' ],
			Integrations\Fail2Ban::class               => null,
			Integrations\Feedback::class               => [ $this, 'buildIntegration' ],
			Integrations\Jetpack::class                => [ $this, 'buildIntegration' ],
			Integrations\Maintenance::class            => function ( $app ) {
				return new Integrations\Maintenance(
					$app->get( Services\MigrationCleaner::class )
				);
			},
			Integrations\ObjectCache::class            => function ( $app ) {
				return new Integrations\ObjectCache(
					$app->get( Settings::class ),
					$app->get( AdminBar::class )
				);
			},
			Integrations\OPcache::class                => function ( $app ) {
				return new Integrations\OPcache(
					$app->get( AdminBar::class )
				);
			},
			Integrations\PageCache::class              => null,
			Integrations\PHPCompatibility::class       => null,
			Integrations\PluginInstaller::class        => null,
			Integrations\Recapture::class              => function ( $app ) {
				return new Integrations\Recapture(
					$app->get( Settings::class ),
					$app->get( Integrations\PluginInstaller::class )
				);
			},
			Integrations\RegressionSites::class        => [ $this, 'buildIntegration' ],
			Integrations\SiteHealth::class             => [ $this, 'buildIntegration' ],
			Integrations\StagingSites::class           => [ $this, 'buildIntegration' ],
			Integrations\StoreBuilder::class           => function ( $app ) {
				return new Integrations\StoreBuilder(
					$app->get( Settings::class ),
					$app->get( Services\Importer::class ),
					$app->get( AdminBar::class )
				);
			},
			Integrations\Support::class                => [ $this, 'buildIntegration' ],
			Integrations\SupportUsers::class           => null,
			Integrations\Telemetry::class              => [ $this, 'buildIntegration' ],
			Integrations\Themes::class                 => null,
			Integrations\Updates::class                => [ $this, 'buildIntegration' ],
			Integrations\Varnish::class                => null,
			Integrations\VisualComparison::class       => [ $this, 'buildIntegration' ],
			Integrations\WooCommerce::class            => [ $this, 'buildIntegration' ],
			Integrations\WooCommerceUpperLimits::class => [ $this, 'buildIntegration' ],

			// Services.
			Services\MigrationCleaner::class           => function ( $app ) {
				return new Services\MigrationCleaner( Support\Filesystem::init() );
			},
			Services\Importer::class                   => null,
			Services\Installer::class                  => function ( $app ) {
				return new Services\Installer( $app->get( Settings::class ) );
			},
		], $this->extensions );
	}

	/**
	 * Build a generic integration that receives a Settings object.
	 *
	 * @param self   $app   The current container instance.
	 * @param string $class The integration class name.
	 *
	 * @return \Nexcess\MAPPS\Integrations\Integration The integration instance.
	 */
	private function buildIntegration( $app, $class ) {
		return new $class( $app->get( Settings::class ) );
	}
}
