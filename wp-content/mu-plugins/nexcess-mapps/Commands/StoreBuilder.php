<?php

namespace Nexcess\MAPPS\Commands;

use Kadence_Plugin_API_Manager;
use Nexcess\MAPPS\Concerns\MakesHttpRequests;
use Nexcess\MAPPS\Exceptions\IngestionException;
use Nexcess\MAPPS\Exceptions\RequestException;
use Nexcess\MAPPS\Exceptions\WPErrorException;
use Nexcess\MAPPS\Integrations\StoreBuilder as StoreBuilderIntegration;
use Nexcess\MAPPS\Settings;

/**
 * Commands specific to StoreBuilder sites.
 */
class StoreBuilder extends Command {
	use MakesHttpRequests;

	/**
	 * @var \Nexcess\MAPPS\Integrations\StoreBuilder
	 */
	private $integration;

	/**
	 * @var \Nexcess\MAPPS\Settings
	 */
	private $settings;

	/**
	 * Create a new instance of the command.
	 *
	 * @param \Nexcess\MAPPS\Settings                  $settings    The settings object.
	 * @param \Nexcess\MAPPS\Integrations\StoreBuilder $integration The StoreBuilder integration.
	 */
	public function __construct( Settings $settings, StoreBuilderIntegration $integration ) {
		$this->settings    = $settings;
		$this->integration = $integration;
	}

	/**
	 * Build out the site based on details from the StoreBuilder app.
	 *
	 * This command will also install and activate the Kadence theme (if it isn't already the current theme).
	 *
	 * ## OPTIONS
	 *
	 * [<site_id>]
	 * : The StoreBuilder site ID. Defaults to $settings->storebuilder_site_id.
	 *
	 * [--force]
	 * : Ingest content, even if the ingestion lock has already been set and/or the store has received orders.
	 *
	 * ## EXAMPLES
	 *
	 *   # Build the store using the UUID provided by SiteWorx
	 *   wp nxmapps storebuilder build
	 *
	 *   # Build the store based on specific UUID
	 *   wp nxmapps storebuilder build 3e973431-56d6-4c86-8278-84a72ad5238b
	 *
	 * @param string[] $args    Positional arguments.
	 * @param string[] $options Associative arguments.
	 */
	public function build( array $args, array $options ) {
		if ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			return $this->warning( 'Unable to configure StoreBuilder, as WooCommerce is not active on this site.' );
		}

		$id    = ! empty( $args[0] ) ? $args[0] : $this->settings->storebuilder_site_id;
		$force = ! empty( $options['force'] ) ? (bool) $options['force'] : false;

		if ( empty( $id ) ) {
			return $this->error( 'A StoreBuilder site ID is required to proceed, aborting.', 1 );
		}

		if ( ! $this->integration->mayIngestContent() && ! $force ) {
			return $this->error(
				'StoreBuilder has already been run for this site. If you wish to re-run the build, call this again with the --force option.',
				1
			);
		}

		try {
			$dependencies = $this->getDependencies();
		} catch ( RequestException $e ) {
			return $this->error( sprintf( 'Unable to get StoreBuilder dependencies: %s', $e->getMessage() ), 1 );
		}

		// Set a flag in the global settings object.
		$this->settings->setFlag( 'storebuilder_use_kadence', true );

		$this->step( 'Installing and activating Kadence' )
			->wp( 'theme install --activate kadence' );

		$this->step( 'Installing StoreBuilder plugins' )
			->wp( 'plugin install --activate instagram-feed kadence-blocks woocommerce-gateway-stripe wp101' );

		// Install and license Kadence Pro.
		if ( ! empty( $dependencies['kadence']['zip'] ) ) {
			$kadence = $dependencies['kadence'];

			$plugins = is_array( $kadence['zip'] )
				? implode( ' ', array_map( 'escapeshellarg', $kadence['zip'] ) )
				: escapeshellarg( (string) $kadence['zip'] );
			$this->wp( 'plugin install --activate ' . $plugins );

			if ( ! empty( $kadence['email'] ) && ! empty( $kadence['key'] ) ) {
				try {
					// Explicitly require the API Manager so it's available on this request.
					require_once WP_CONTENT_DIR . '/plugins/kadence-blocks-pro/kadence-classes/kadence-activation/class-kadence-plugin-api-manager.php';
					require_once WP_CONTENT_DIR . '/plugins/kadence-pro/kadence-classes/kadence-activation/class-kadence-plugin-api-manager.php';

					$instance = Kadence_Plugin_API_Manager::get_instance();
					$instance->on_init(); // Normally run on "init".
					$instance->activate( [
						'email'       => sanitize_text_field( $kadence['email'] ),
						'licence_key' => sanitize_text_field( $kadence['key'] ),
						'product_id'  => 'kadence_gutenberg_pro',
					] );
					$instance->activate( [
						'email'       => sanitize_text_field( $kadence['email'] ),
						'licence_key' => sanitize_text_field( $kadence['key'] ),
						'product_id'  => 'kadence_pro',
					] );

					update_option( 'ktp_api_manager', [
						'ktp_api_key'      => sanitize_text_field( $kadence['key'] ),
						'activation_email' => sanitize_text_field( $kadence['email'] ),
					] );
					update_option( 'kt_api_manager_kadence_gutenberg_pro_data', [
						'api_key'    => sanitize_text_field( $kadence['key'] ),
						'api_email'  => sanitize_text_field( $kadence['email'] ),
						'product_id' => 'kadence_gutenberg_pro',
					] );
					update_option( 'kt_api_manager_kadence_pro_data', [
						'api_key'    => sanitize_text_field( $kadence['key'] ),
						'api_email'  => sanitize_text_field( $kadence['email'] ),
						'product_id' => 'kadence_pro',
					] );

					update_option( 'kadence_pro_api_manager_activated', 'Activated' );
					update_option( 'kadence_gutenberg_pro_activation', 'Activated' );
					update_option( 'kadence_pro_activation', 'Activated' );
				} catch ( \Exception $e ) {
					$this->warning( 'Kadence Pro and Kadence Blocks Pro have been installed, but not activated.' );
				}
			}
		}

		// License WP101.
		if ( ! empty( $dependencies['wp101']['key'] ) ) {
			update_option( 'wp101_api_key', sanitize_text_field( $dependencies['wp101']['key'] ) );
		}

		// Hide nags in the Instagram Feed plugin.
		update_option( 'sbi_rating_notice', 'dismissed' );
		update_option( 'sbi_usage_tracking', [
			'last_send' => 0,
			'enabled'   => false,
		] );

		$this->step( 'Ingesting content from StoreBuilder' );
		try {
			$this->integration->ingestContent( $id, $force );
		} catch ( IngestionException $e ) {
			return $this->error( $e->getMessage(), 1 );
		}

		// Send the welcome email.
		$this->step( 'Sending welcome email' );
		$admin = get_user_by( 'email', get_option( 'admin_email' ) );

		if ( $admin ) {
			$this->integration->sendWelcomeEmail( $admin );
		}

		$this->success( sprintf( 'Site %s has been built successfully!', $id ) );
	}

	/**
	 * Retrieve dependencies from the StoreBuilder API.
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\RequestException if the dependencies can't be retrieved.
	 *
	 * @return array[] Details necessary to install all StoreBuilder plugins.
	 */
	protected function getDependencies() {
		$response = wp_remote_get( $this->integration->getAppUrl() . '/api/dependencies', [
			'headers' => [
				'Authorization' => 'Bearer ' . $this->settings->managed_apps_token,
				'Accept'        => 'application/json',
			],
		] );

		try {
			$json = $this->validateHttpResponse( $response );
		} catch ( WPErrorException $e ) {
			throw new RequestException( $e->getMessage(), $e->getCode(), $e );
		}

		$body = json_decode( $json, true );

		if ( ! is_array( $body ) ) {
			throw new RequestException(
				sprintf( 'Received an unexpected response body from the StoreBuilder app: %s', (string) $json )
			);
		}

		return $body;
	}
}
