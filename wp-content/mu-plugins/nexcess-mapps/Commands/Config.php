<?php

namespace Nexcess\MAPPS\Commands;

use Nexcess\MAPPS\Concerns\ManagesWpConfig;
use Nexcess\MAPPS\Exceptions\ConfigException;

/**
 * WP-CLI sub-commands for modifying a site's wp-config.php file.
 */
class Config extends Command {
	use ManagesWpConfig;

	/**
	 * Regenerate the WP_CACHE_KEY_SALT constant.
	 *
	 * ## EXAMPLES
	 *
	 * $ wp nxmapps config regenerate-cache-key
	 * Success: WP_CACHE_KEY_SALT regenerated.
	 *
	 * @subcommand regenerate-cache-key
	 */
	public function regenerate_cache_key() {
		$salt = wp_generate_password( 64, true, true );

		try {
			$this->setConfigConstant( 'WP_CACHE_KEY_SALT', $salt );
		} catch ( ConfigException $e ) {
			$this->error( 'Unable to update WP_CACHE_KEY_SALT: ' . $e->getMessage(), 1 );
		}

		$this->success( 'The WP_CACHE_KEY_SALT constant has been rotated.' );
	}
}
