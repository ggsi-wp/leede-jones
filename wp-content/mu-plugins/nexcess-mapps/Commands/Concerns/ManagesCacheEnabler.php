<?php

namespace Nexcess\MAPPS\Commands\Concerns;

use Nexcess\MAPPS\Integrations\PageCache;

trait ManagesCacheEnabler {

	/**
	 * Install, activate, and configure Cache Enabler.
	 */
	protected function configureCacheEnabler() {
		$this->setDefaultPermalinkStructure();

		$this->step( 'Configuring Cache Enabler' );
		$this->wp( 'plugin install cache-enabler --activate' );

		// Apply our default configuration.
		update_option( 'cache_enabler', PageCache::getCacheEnablerSettings() );

		// Inject rewrite rules.
		if ( ! PageCache::injectCacheEnablerRewriteRules() ) {
			return $this->error( 'Unable to write Htaccess file!' );
		}
	}

	/**
	 * Set a default permalink structure.
	 *
	 * Cache Enabler requires a non-default permalink structure, but he default value for the
	 * "permalink_structure" option is an empty string; if that's the case (or the option doesn't
	 * exist), use "/%postname%/".
	 */
	protected function setDefaultPermalinkStructure() {
		if ( empty( get_option( 'permalink_structure', '' ) ) ) {
			$this->log( 'Setting default permalink structure' );
			PageCache::setDefaultPermalinkStructure();
		}
	}
}
