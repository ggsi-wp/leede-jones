<?php
/**
 * Dynamically generate a new store for customers.
 */

namespace Nexcess\MAPPS\Integrations;

use Nexcess\MAPPS\AdminBar;
use Nexcess\MAPPS\Concerns\HasAdminPages;
use Nexcess\MAPPS\Concerns\HasHooks;
use Nexcess\MAPPS\Concerns\HasWordPressDependencies;
use Nexcess\MAPPS\Concerns\MakesHttpRequests;
use Nexcess\MAPPS\Concerns\QueriesWooCommerce;
use Nexcess\MAPPS\Exceptions\ContentOverwriteException;
use Nexcess\MAPPS\Exceptions\IngestionException;
use Nexcess\MAPPS\Exceptions\WPErrorException;
use Nexcess\MAPPS\Services\Importer;
use Nexcess\MAPPS\Settings;
use Nexcess\MAPPS\Support\AdminNotice;
use Nexcess\MAPPS\Support\Branding;
use Nexcess\MAPPS\Support\Helpers;
use WC_Product_CSV_Importer_Controller;
use WP_Term;
use WP_User;

use const Nexcess\MAPPS\PLUGIN_VERSION;

class StoreBuilder extends Integration {
	use HasAdminPages;
	use HasHooks;
	use HasWordPressDependencies;
	use MakesHttpRequests;
	use QueriesWooCommerce;

	/**
	 * @var \Nexcess\MAPPS\AdminBar
	 */
	protected $adminBar;

	/**
	 * @var string[]
	 */
	protected $contentPlaceholders = [];

	/**
	 * @var \Nexcess\MAPPS\Services\Importer
	 */
	protected $importer;

	/**
	 * @var \Nexcess\MAPPS\Settings
	 */
	protected $settings;

	/**
	 * The option that gets set once we've already ingested once.
	 */
	const INGESTION_LOCK_OPTION_NAME = '_storebuilder_created_on';

	/**
	 * The post meta key that gets set on generated content.
	 */
	const GENERATED_AT_POST_META_KEY = '_storebuilder_generated_at';

	/**
	 * The URL for the StoreBuilder marketing site.
	 */
	const MARKETING_URL = 'https://www.nexcess.net/storebuilder';

	/**
	 * @param \Nexcess\MAPPS\Settings $settings
	 * @param \Nexcess\MAPPS\Services\Importer $importer
	 */
	public function __construct( Settings $settings, Importer $importer, AdminBar $admin_bar ) {
		$this->adminBar = $admin_bar;
		$this->settings = $settings;
		$this->importer = $importer;
	}

	/**
	 * Determine whether or not this integration should be loaded.
	 *
	 * @return bool Whether or not this integration be loaded in this environment.
	 */
	public function shouldLoadIntegration() {
		return $this->settings->is_storebuilder
			|| get_option( self::INGESTION_LOCK_OPTION_NAME, false );
	}

	/**
	 * Perform any necessary setup for the integration.
	 *
	 * This method is automatically called as part of Plugin::registerIntegration(), and is the
	 * entry-point for all integrations.
	 */
	public function setup() {
		$this->addHooks();

		// Remove the default WordPress welcome panel.
		remove_action( 'welcome_panel', 'wp_welcome_panel' );
	}

	/**
	 * Retrieve all actions for the integration.
	 *
	 * @return array[]
	 */
	protected function getActions() {
		// phpcs:disable WordPress.Arrays
		return [
			[ 'admin_init',         [ $this, 'warnUsersAboutFailedIngestion' ]        ],
			[ 'admin_menu',         [ $this, 'filterWp101Menu'               ], 9999  ],
			[ 'login_init',         [ $this, 'clearKadenceFlag'              ]        ],
			[ 'setup_theme',        [ $this, 'setKadenceAsDefaultTheme'      ], 100   ],
			[ 'welcome_panel',      [ $this, 'renderWelcomePanel'            ], -9999 ],
			[ 'wp_dashboard_setup', [ $this, 'registerDashboardWidgets'      ]        ],
		];
		// phpcs:enable WordPress.Arrays
	}

	/**
	 * Retrieve all filters for the integration.
	 *
	 * @return array[]
	 */
	protected function getFilters() {
		// phpcs:disable WordPress.Arrays
		return [
			[ 'default_hidden_meta_boxes',                [ $this, 'filterDashboardMetaboxes' ] ],
			[ 'get_user_option_meta-box-order_dashboard', [ $this, 'filterDefaultMetaboxOrder' ] ],

			// Add classes to our custom widgets.
			[ 'postbox_classes_dashboard_mapps-storebuilder-launch',  [ $this, 'postBoxClasses' ] ],
			[ 'postbox_classes_dashboard_mapps-storebuilder-support', [ $this, 'postBoxClasses' ] ],

			// Kadence configuration.
			[ 'kadence_theme_options_defaults', [ $this, 'setKadenceDefaults' ] ],

			// Disable Instagram Feed notifications.
			[ 'sbi_admin_notifications_has_access', '__return_false' ],
		];
		// phpcs:enable WordPress.Arrays
	}

	/**
	 * Clear the flag used to indicate that Kadence should be forced.
	 */
	public function clearKadenceFlag() {
		if ( $this->settings->hasFlag( 'storebuilder_use_kadence' ) ) {
			$this->settings->deleteFlag( 'storebuilder_use_kadence' );
		}
	}

	/**
	 * Filter the default dashboard meta boxes shown to users.
	 *
	 * Once a user shows or hides a meta box, their selections are saved and will be used for
	 * subsequent page loads.
	 *
	 * @param string[] $meta_boxes An array of meta box keys that should be hidden by default.
	 *
	 * @return string[] The filtered $meta_boxes array.
	 */
	public function filterDashboardMetaboxes( array $meta_boxes ) {
		return array_unique( array_merge( $meta_boxes, [
			'dashboard_activity',
			'dashboard_primary',
			'dashboard_rediscache',
			'dashboard_quick_press',
		] ) );
	}

	/**
	 * Filter the default ordering of dashboard meta boxes unless the user has set their own.
	 *
	 * @param string[] $order A user-defined ordering, in the form of "column_name:id1,id2".
	 *
	 * @return string[] Either the $order array or an array of reasonable defaults if $order is empty.
	 */
	public function filterDefaultMetaboxOrder( $order ) {
		if ( ! empty( $order ) ) {
			return $order;
		}

		return [
			'normal'  => 'dashboard_right_now,mapps-storebuilder-support',
			'side'    => 'mapps-storebuilder-launch,dashboard_site_health',
			'column3' => 'woocommerce_dashboard_status,woocommerce_dashboard_recent_reviews',
		];
	}

	/**
	 * Filter the menu label for WP101, changing it to "StoreBuilder Academy".
	 *
	 * This is being done with approval from Shawn @ WP101, of course :).
	 *
	 * @global $menu
	 * @global $submenu
	 */
	public function filterWp101Menu() {
		global $menu, $submenu;

		/*
		 * Important keys:
		 *
		 * 0: The page label (shown in the menu bar)
		 * 2: The page ID
		 * 3: The <title> of the rendered page
		 */
		array_walk( $menu, function ( &$item ) {
			if ( 'wp101' !== $item[2] ) {
				return;
			}

			$item[0] = _x( 'StoreBuilder Academy', 'menu label', 'nexcess-mapps' );
			$item[3] = _x( 'StoreBuilder Academy, powered by WP101', 'menu page title', 'nexcess-mapps' );
		} );

		// Something's gone wrong and we don't have a child page, so there's nothing more to do.
		if ( ! isset( $submenu['wp101'] ) ) {
			return;
		}

		/*
		 * Important keys:
		 *
		 * 2: The page ID
		 * 3: The <title> of the rendered page
		 */
		array_walk( $submenu['wp101'], function ( &$item ) {
			if ( 'wp101' !== $item[2] ) {
				return;
			}

			$item[3] = _x( 'StoreBuilder Academy, powered by WP101', 'menu page title', 'nexcess-mapps' );
		} );
	}

	/**
	 * Get the URL for the StoreBuilder API.
	 *
	 * @return string The StoreBuilder API URL, without a trailing slash.
	 */
	public function getAppUrl() {
		return defined( 'NEXCESS_MAPPS_STOREBUILDER_URL' )
			? untrailingslashit( NEXCESS_MAPPS_STOREBUILDER_URL )
			: 'https://storebuilder.app';
	}

	/**
	 * Retrieve content from the app and ingest it into WordPress.
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\ContentOverwriteException if ingesting content would cause
	 *                                                             content to be overwritten.
	 * @throws \Nexcess\MAPPS\Exceptions\IngestionException        if content cannot be ingested.
	 *
	 * @param string $site_id The StoreBuilder site ID.
	 * @param bool   $force   Optional. Whether or not to run the ingestion regardless of
	 *                        mayIngestContent(). Default is false.
	 */
	public function ingestContent( $site_id, $force = false ) {
		if ( ! $this->mayIngestContent() && ! $force ) {
			throw new ContentOverwriteException(
				__( 'StoreBuilder layouts have already been imported for this store, abandoning in order to prevent overwriting content.', 'nexcess-mapps' )
			);
		}

		// Retrieve content from the StoreBuilder app.
		$response = wp_remote_get( sprintf( '%s/api/stores/%s', $this->getAppUrl(), $site_id ), [
			'headers' => [
				'Accept' => 'application/json',
			],
			'timeout' => 45,
		] );

		try {
			$layout = json_decode( $this->validateHttpResponse( $response ), true );
		} catch ( WPErrorException $e ) {
			throw new IngestionException(
				sprintf( 'Unable to retrieve content from StoreBuilder: %1$s', $e->getMessage() ),
				$e->getCode(),
				$e
			);
		}

		// Verify that the content matches what we're expecting.
		if ( empty( $layout ) || empty( $layout['pages'] ) ) {
			throw new IngestionException( __( 'StoreBuilder response body was empty or malformed.', 'nexcess-mapps' ) );
		}

		// Finally, ingest content.
		try {
			if ( ! empty( $layout['attachments'] ) ) {
				$this->createAttachments( (array) $layout['attachments'] );
			}

			if ( ! empty( $layout['links'] ) ) {
				$this->createLinks( (array) $layout['links'] );
			}

			if ( ! empty( $layout['pages'] ) ) {
				$this->createPages( (array) $layout['pages'] );
			}

			if ( ! empty( $layout['taxonomies'] ) ) {
				$this->createTerms( (array) $layout['taxonomies'] );
			}

			if ( ! empty( $layout['menus'] ) ) {
				$this->createMenus( (array) $layout['menus'] );
			}

			if ( ! empty( $layout['store'] ) ) {
				$this->setThemeOptions( (array) $layout['store'] );
			}

			if ( ! empty( $layout['products'] ) ) {
				$this->createProducts( (string) $layout['products'] );
			}
		} catch ( IngestionException $e ) {
			throw new IngestionException(
			/* Translators: %1$s is the previous exception's message. */
				sprintf( __( 'An error occurred while ingesting content: %1$s', 'nexcess-mapps' ), $e->getMessage() ),
				$e->getCode(),
				$e
			);
		}

		// Prevent the StoreBuilder from being run again.
		update_option( self::INGESTION_LOCK_OPTION_NAME, [
			'mapps_version' => PLUGIN_VERSION,
			'timestamp'     => time(),
		] );
	}

	/**
	 * Determine if the store is eligible to ingest content.
	 *
	 * @return bool True if the store is allowed to ingest content, false otherwise.
	 */
	public function mayIngestContent() {
		return ! get_option( self::INGESTION_LOCK_OPTION_NAME, false )
			&& ! $this->storeHasOrders();
	}

	/**
	 * Add .mapps-storebuilder-widget to our custom widgets.
	 *
	 * @param string[] $classes Classes to be applied to the post boxes.
	 *
	 * @return string[] The filtered $classes array.
	 */
	public function postBoxClasses( array $classes ) {
		$classes[] = 'mapps-storebuilder-widget';

		return $classes;
	}

	/**
	 * Register StoreBuilder-specific dashboard widgets.
	 */
	public function registerDashboardWidgets() {
		wp_add_dashboard_widget(
			'mapps-storebuilder-support',
			_x( 'StoreBuilder Support', 'widget title', 'nexcess-mapps' ),
			function () {
				$this->renderTemplate( 'widgets/storebuilder-support' );
			},
			null,
			null,
			'normal',
			'default'
		);

		wp_add_dashboard_widget(
			'mapps-storebuilder-launch',
			_x( 'Going Live with StoreBuilder', 'widget title', 'nexcess-mapps' ),
			function () {
				$this->renderTemplate( 'widgets/storebuilder-launch' );
			},
			null,
			null,
			'side',
			'high'
		);
	}

	/**
	 * Render the welcome panel on the WP Admin dashboard.
	 */
	public function renderWelcomePanel() {
		$this->renderTemplate( 'widgets/storebuilder-welcome' );
	}

	/**
	 * Send the welcome email to the given user.
	 *
	 * @param \WP_User $user The user to receive the welcome email.
	 *
	 * @return bool True if the message was sent, false otherwise.
	 */
	public function sendWelcomeEmail( WP_User $user ) {
		$title = __( '👋 Welcome to StoreBuilder by Nexcess', 'nexcess-mapps' );
		$body  = $this->getTemplateContent( 'messages/storebuilder-onboarding', [
			'reset_url' => Helpers::getResetPasswordUrl( $user ),
			'user'      => $user,
		] );

		// Override the default "from" name and address.
		add_filter( 'wp_mail_from', function ( $email ) {
			return str_ireplace( 'wordpress@', 'storebuilder@', $email );
		} );

		add_filter( 'wp_mail_from_name', function () {
			return 'StoreBuilder';
		} );

		// Finally, send the message.
		return wp_mail( $user->user_email, $title, $body, [
			'Reply-To: storebuilder@nexcess.net',
		] );
	}

	/**
	 * Set Kadence as the default theme.
	 *
	 * The provisioner currently installs and activates Astra *after* the setup script is run,
	 * meaning we install + activate Kadence only to have it overwritten.
	 *
	 * If the flag is set, force Kadence as the active theme.
	 */
	public function setKadenceAsDefaultTheme() {
		if ( ! $this->settings->getFlag( 'storebuilder_use_kadence', false ) ) {
			return;
		}

		if ( 'kadence' !== get_stylesheet() ) {
			switch_theme( 'kadence' );
		}
	}

	/**
	 * Overwrite some default settings for Kadence.
	 *
	 * @param mixed[] $options Default theme options for Kadence.
	 *
	 * @return mixed[] The filtered $options array.
	 */
	public function setKadenceDefaults( $options ) {
		// Overwrite the default Kadence footer.
		$options['footer_html_content'] = sprintf(
			'{copyright} {year} {site-title} — %s',
			_x( 'StoreBuilder by Nexcess', 'StoreBuilder theme footer', 'nexcess-mapps' )
		);

		// Use un-boxed styles by default.
		$options['page_content_style'] = 'unboxed';

		// Set default assignments.
		$options['header_desktop_items'] = [
			'main' => [
				'main_left'  => [
					'logo',
				],
				'main_right' => [
					'navigation',
				],
			],
		];

		return $options;
	}

	/**
	 * Warn users if they're on a StoreBuilder plan but have no UUID and/or ingestion has not yet
	 * been completed.
	 */
	public function warnUsersAboutFailedIngestion() {
		if ( get_option( self::INGESTION_LOCK_OPTION_NAME, false ) ) {
			return;
		}

		$message  = sprintf(
			'<strong>%s</strong>',
			__( 'Something seems to have gone wrong setting up your StoreBuilder shop.', 'nexcess-mapps' )
		);
		$message .= PHP_EOL;
		$message .= __( 'Our apologies, but it seems there was a hiccup building your new store. <a href="https://www.nexcess.net/storebuilder">Please reach out to StoreBuilder chat support</a> and we\'ll get things up-and-running for you.', 'nexcess-mapps' );

		$notice = new AdminNotice( $message, 'warning', false, 'storebuilder-setup-incomplete' );
		$notice->setCapability( 'manage_options' );

		$this->adminBar->addNotice( $notice );
	}

	/**
	 * Set the homepage content based on content from the StoreBuilder app.
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\IngestionException If content cannot be imported.
	 *
	 * @param int $page_id The ID of the page to make the homepage.
	 */
	protected function setHomepage( $page_id ) {
		update_option( 'show_on_front', 'page' );
		update_option( 'page_on_front', (int) $page_id );
	}

	/**
	 * Import attachments that have been specified by the import.
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\IngestionException If one or more attachments cannot be
	 *                                                      imported into WordPress.
	 *
	 * @param array[] $attachments An array of attachments, keyed by their placeholder ID.
	 */
	protected function createAttachments( array $attachments ) {
		$errors = [];

		foreach ( $attachments as $id => $attachment ) {
			try {
				if ( empty( $attachment['url'] ) ) {
					continue;
				}

				$attachment_id  = $this->importer->importAttachment( $attachment['url'], $attachment );
				$attachment_src = wp_get_attachment_url( $attachment_id );

				if ( ! $attachment_src ) {
					continue;
				}

				// With the attachment loaded, add it to our mapping array.
				$this->contentPlaceholders[ '%attachment:' . $id . '%' ] = $attachment_src;
			} catch ( \Exception $e ) {
				$errors[] = $e->getMessage();
			}
		}

		// Gather up any errors into a single exception.
		if ( ! empty( $errors ) ) {
			throw new IngestionException( sprintf(
				"The following errors occurred while creating attachments:\n- %s",
				implode( "\n- ", $errors )
			) );
		}
	}

	/**
	 * Add links to $this->contentPlaceholders.
	 *
	 * @param array[] $links Links defined by the StoreBuilder app.
	 */
	protected function createLinks( array $links ) {
		foreach ( $links as $hash => $link ) {
			if ( empty( $link['slug'] ) ) {
				continue;
			}

			$this->contentPlaceholders[ '%link:' . $hash . '%' ] = site_url( $link['slug'] );
		}
	}

	/**
	 * Ingest menus defined by the StoreBuilder app.
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\IngestionException If one or more menus could not be created.
	 *
	 * @param array[] $menus Menus provided by StoreBuilder.
	 */
	protected function createMenus( array $menus ) {
		$locations = [];
		$errors    = [];

		foreach ( $menus as $menu ) {
			try {
				$menu_id = $this->createMenu( $menu );

				if ( ! empty( $menu['display_location'] ) ) {
					$locations[ $menu['display_location'] ] = $menu_id;
				}
			} catch ( \Exception $e ) {
				$errors[] = $e->getMessage();
			}
		}

		// Assign the newly-created menus to their display locations.
		if ( ! empty( $locations ) ) {
			set_theme_mod( 'nav_menu_locations', $locations );
		}

		// Gather up any errors into a single exception.
		if ( ! empty( $errors ) ) {
			throw new IngestionException( sprintf(
				"The following errors occurred while creating menus:\n- %s",
				implode( "\n- ", $errors )
			) );
		}
	}

	/**
	 * Create a single menu.
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\IngestionException If any menu items cannot be created.
	 * @throws \Nexcess\MAPPS\Exceptions\WPErrorException   If the nav menu could not be created.
	 *
	 * @param mixed[] $menu {
	 *   Details about the menu being created.
	 *
	 *   @type string  $display_location The theme location for the menu.
	 *   @type array[] $items            Individual menu items.
	 *   @type string  $label            The menu name.
	 * }
	 *
	 * @return int The newly-created menu ID.
	 */
	protected function createMenu( array $menu ) {
		$menu_id = wp_create_nav_menu( addslashes( $menu['label'] ) );

		if ( is_wp_error( $menu_id ) ) {
			throw new WPErrorException( $menu_id );
		}

		$menu_object = wp_get_nav_menu_object( $menu_id );

		if ( ! $menu_object ) {
			throw new IngestionException(
				sprintf( 'Unable to create the "%s" menu.', $menu['label'] )
			);
		}

		// Add the menu items.
		foreach ( (array) $menu['items'] as $item ) {
			$this->createMenuItem( $item, $menu_object );
		}

		return $menu_id;
	}

	/**
	 * Create a single menu item.
	 *
	 * The type of menu item can be any of the following:
	 * 1. A link to a page object.
	 * 2. A link to a product category.
	 * 3. A custom URL.
	 *
	 * For post + term objects, the menu item will not be created if the target does not exist.
	 *
	 * @param string[] $item {
	 *   Details about the menu item.
	 *
	 *   @type string $label The label to use for the menu item. Only used custom URLs.
	 *   @type string $name  The link target object's name. Used for posts + terms.
	 *   @type string $type  The type of menu item (page|product_cat|custom).
	 *   @type string $url   The link URL. Only used for custom URLs.
	 * }
	 * @param \WP_Term $menu The menu object to which the item should be assigned.
	 */
	protected function createMenuItem( array $item, WP_Term $menu ) {
		switch ( $item['type'] ) {
			// A page.
			case 'page':
				$page = get_page_by_path( $item['name'], 'page' );

				if ( empty( $page ) ) {
					return;
				}

				$menu_item = [
					'menu-item-object-id' => $page->ID,
					'menu-item-object'    => 'page',
					'menu-item-type'      => 'post_type',
				];
				break;

			// A product category.
			case 'product_cat':
				$term = get_term_by( 'slug', $item['name'], 'product_cat', OBJECT );

				if ( ! $term instanceof WP_Term ) {
					return;
				}

				$menu_item = [
					'menu-item-title'     => $term->name,
					'menu-item-object-id' => $term->term_id,
					'menu-item-object'    => 'product_cat',
					'menu-item-type'      => 'taxonomy',
					'menu-item-url'       => get_term_link( $term ),
				];
				break;

			// A custom URL.
			case 'custom':
				$menu_item = [
					'menu-item-title' => $item['label'],
					'menu-item-url'   => $item['url'],
					'menu-item-type'  => 'custom',
				];
				break;
			default:
				return;
		}

		wp_update_nav_menu_item( $menu->term_id, 0, array_merge( [
			'menu-item-status' => 'publish',
		], $menu_item ) );
	}

	/**
	 * Ingest pages defined by the StoreBuilder app.
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\IngestionException If one or more pages can't be created.
	 *
	 * @param array[] $pages Pages provided by the StoreBuilder app.
	 */
	protected function createPages( array $pages ) {
		$errors = [];

		foreach ( $pages as $page ) {
			try {
				$this->createPage( $page );
			} catch ( IngestionException $e ) {
				$errors[] = $e;
			}
		}

		// Gather up any errors into a single exception.
		if ( ! empty( $errors ) ) {
			throw new IngestionException( sprintf(
				"The following errors occurred while creating pages:\n- %s",
				implode( "\n- ", $errors )
			) );
		}
	}

	/**
	 * Create a single page within WordPress.
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\IngestionException if the page cannot be created.
	 *
	 * @param mixed[] $page An array of details about the page.
	 *
	 * @return int The post ID of the newly-created page.
	 */
	protected function createPage( array $page ) {
		if ( empty( $page['label'] ) ) {
			throw new IngestionException( 'A page title must be provided.' );
		}

		$content = ! empty( $page['content'] ) ? trim( (string) $page['content'] ) : '';

		// Update any placeholders in the block content.
		if ( ! empty( $this->contentPlaceholders ) ) {
			$content = str_replace(
				array_keys( $this->contentPlaceholders ),
				array_values( $this->contentPlaceholders ),
				$content
			);
		}

		// Assemble post meta.
		$meta = array_merge( [
			self::GENERATED_AT_POST_META_KEY => time(),
			'_kad_post_content_style'        => 'unboxed',
			'_kad_post_title'                => 'hide',
			'_kad_post_layout'               => 'fullwidth',
		], isset( $page['meta'] ) ? (array) $page['meta'] : [] );

		// Insert the page.
		$page_id = wp_insert_post( [
			'post_title'   => $page['label'],
			'post_name'    => ! empty( $page['name'] ) ? $page['name'] : null,
			'post_content' => $content,
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'meta_input'   => $meta,
		] );

		if ( is_wp_error( $page_id ) ) {
			throw new IngestionException( sprintf(
				'Error creating page "%1$s": %2$s',
				$page['label'],
				$page_id->get_error_message()
			) );
		}

		if ( 'home' === $page['name'] ) {
			$this->setHomepage( $page_id );
		}

		return $page_id;
	}

	/**
	 * Ingest taxonomy terms from the StoreBuilder app.
	 *
	 * The $terms array groups terms by their taxonomy in nested arrays:
	 *
	 *     [
	 *         'product_cat' => [
	 *             [
	 *                 'name'  => 'first-cat',
	 *                 'label' => 'First Category',
	 *             ],
	 *             [
	 *                 'name'  => 'second-cat',
	 *                 'label' => 'Second Category',
	 *             ],
	 *         ],
	 *         'category'    => [
	 *             // ...
	 *         ],
	 *     ]
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\IngestionException If one or more terms can't be created.
	 *
	 * @param array[] $terms An array of terms, grouped by taxonomy.
	 */
	protected function createTerms( $terms ) {
		$errors = [];

		foreach ( $terms as $taxonomy => $taxonomy_terms ) {
			foreach ( $taxonomy_terms as $term ) {
				try {
					$this->createTerm( $taxonomy, $term );
				} catch ( IngestionException $e ) {
					$errors[] = $e->getMessage();
				}
			}
		}

		// Gather up any errors into a single exception.
		if ( ! empty( $errors ) ) {
			throw new IngestionException( sprintf(
				"The following errors occurred while creating taxonomy terms:\n- %s",
				implode( "\n- ", $errors )
			) );
		}
	}

	/**
	 * Create a single taxonomy term.
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\IngestionException If a term cannot be created.
	 *
	 * @param string  $taxonomy  The taxonomy name.
	 * @param mixed[] $args      An array of term data.
	 * @param ?int    $parent_id Optional. The taxonomy term parent. Default is null.
	 */
	protected function createTerm( $taxonomy, $args, $parent_id = null ) {
		// Do nothing if the term array is invalid.
		if ( empty( $args['name'] ) || empty( $args['label'] ) ) {
			return;
		}

		if ( ! taxonomy_exists( $taxonomy ) ) {
			throw new IngestionException( sprintf( 'Taxonomy "%s" does not exist.', $taxonomy ) );
		}

		// Create the term, re-using exiting terms when available.
		try {
			$term = term_exists( $args['name'], $taxonomy, $parent_id );

			/*
			 * If term_exists() returned anything but an array of term ID and taxonomy term ID,
			 * we should attempt to create it.
			 */
			if ( ! is_array( $term ) ) {
				$term = wp_insert_term( $args['label'], $taxonomy, [
					'parent' => (int) $parent_id,
					'slug'   => $args['name'],
				] );

				if ( is_wp_error( $term ) ) {
					throw new WPErrorException( $term );
				}
			}

			$term_id = current( $term );
		} catch ( \Exception $e ) {
			throw new IngestionException( sprintf(
				'Unable to create term "%1$s" in taxonomy "%2$s": %3$s',
				$args['label'],
				$taxonomy,
				$e->getMessage()
			), $e->getCode(), $e );
		}

		// Recursively create children, if defined.
		if ( empty( $args['children'] ) ) {
			return;
		}

		$errors = [];

		foreach ( $args['children'] as $child ) {
			try {
				$this->createTerm( $taxonomy, $child, $term_id );
			} catch ( IngestionException $e ) {
				$errors[] = $e->getMessage();
			}
		}

		if ( ! empty( $errors ) ) {
			throw new IngestionException( sprintf(
				"The following errors occurred while creating \"%1\$s\" terms for parent \"%2\$s\" (ID %3\$d):\n- %4\$s",
				$taxonomy,
				$args['label'],
				$term_id,
				implode( "\n- ", $errors )
			) );
		}
	}

	/**
	 * Ingest products defined by the StoreBuilder app.
	 *
	 * This works in the same way as the WooCommerce core CSV importer, using a CSV file provided
	 * by the StoreBuilder app.
	 *
	 * @param string $csv_url URL for a CSV containing sample products.
	 */
	protected function createProducts( $csv_url ) {
		include_once ABSPATH . 'wp-admin/includes/file.php';
		include_once WC_ABSPATH . 'includes/admin/importers/class-wc-product-csv-importer-controller.php';
		include_once WC_ABSPATH . 'includes/import/class-wc-product-csv-importer.php';

		/*
		 * By default, WooCommerce is expecting files with .csv or .txt extensions, but download_url()
		 * will produce temp files with .tmp extensions.
		 *
		 * @param array $types Valid extension => MIME-type mappings.
		 */
		add_filter( 'woocommerce_csv_product_import_valid_filetypes', function ( $types ) {
			$types['tmp'] = 'text/csv';

			return $types;
		} );

		// Download the file locally, then import the products.
		try {
			$csv = download_url( $csv_url );

			if ( is_wp_error( $csv ) ) {
				throw new WPErrorException( $csv );
			}

			// Use the WooCommerce core product importer.
			$results = WC_Product_CSV_Importer_Controller::get_importer( $csv, [
				'parse' => true,
			] )->import();

			// Unlink the temporary CSV file.
			if ( file_exists( $csv ) ) {
				unlink( $csv );
			}
		} catch ( \Exception $e ) {
			throw new IngestionException( sprintf(
				'Unable to import demo products from %1$s: %2$s',
				$csv_url,
				$e->getMessage()
			), $e->getCode(), $e );
		}

		// Report any errors.
		if ( ! empty( $results['failed'] ) ) {
			$messages = array_map( function ( $error ) {
				return is_wp_error( $error )
					? $error->get_error_message()
					: 'Unspecified (failure was not a WP_Error object)';
			}, $results['failed'] );

			throw new IngestionException( sprintf(
				"The following error(s) occurred while importing products:\n -%1\$s",
				implode( "\n- ", $messages )
			) );
		}
	}

	/**
	 * Apply theme modifications.
	 *
	 * @param mixed[] $mods {
	 *   Theme modifications to be applied. All keys are optional.
	 *
	 *   @type bool $search Whether or not to add search to the main sidebar. Default is false.
	 * }
	 */
	protected function setThemeOptions( array $mods = [] ) {
		$option = 'theme_mods_kadence';
		$mods   = wp_parse_args( $mods, [
			'search' => false,
		] );

		$kadence_options = $this->setKadenceDefaults( get_option( $option, [] ) );

		// If 'search' is true, add a search widget to the main sidebar.
		if ( $mods['search'] ) {
			// Avoid duplicate entries in the main right header section while allowing the primary nav to remain.
			if ( ! in_array( 'search', $kadence_options['header_desktop_items']['main']['main_right'], true ) ) {
				array_push( $kadence_options['header_desktop_items']['main']['main_right'], 'search' );
			}
		}

		update_option( $option, $kadence_options );
	}
}
