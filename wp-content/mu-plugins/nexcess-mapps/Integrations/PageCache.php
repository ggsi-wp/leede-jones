<?php

/**
 * Full-page cache integration.
 */

namespace Nexcess\MAPPS\Integrations;

use Nexcess\MAPPS\Concerns\HasHooks;
use Nexcess\MAPPS\Exceptions\FilesystemException;
use Nexcess\MAPPS\Support\Filesystem;

class PageCache extends Integration {
	use HasHooks;

	/**
	 * The marker name used in the Htaccess file.
	 *
	 * This value should not be changed without accounting for previous value(s) in the injection
	 * and removal methods!
	 */
	const HTACCESS_MARKER = 'Cache Enabler';

	/**
	 * Retrieve all actions for the integration.
	 *
	 * @return array[]
	 */
	protected function getActions() {
		// phpcs:disable WordPress.Arrays
		return [
			[ 'activate_cache-enabler/cache-enabler.php',   [ $this, 'injectCacheEnablerRewriteRules' ] ],
			[ 'deactivate_cache-enabler/cache-enabler.php', [ $this, 'removeCacheEnablerRewriteRules' ] ],
			[ 'cache_enabler_complete_cache_cleared',       [ $this, 'setCacheDirectoryPermissions'   ] ],

			// Cron events.
			[ Maintenance::DAILY_MAINTENANCE_CRON_ACTION, [ $this, 'setCacheDirectoryPermissions' ] ],
			[ Maintenance::DAILY_MAINTENANCE_CRON_ACTION, [ $this, 'migrateCacheEnablerSettings'  ] ],
		];
		// phpcs:enable WordPress.Arrays
	}

	/**
	 * Retrieve all filters for the integration.
	 *
	 * @return array[]
	 */
	protected function getFilters() {
		return [
			[ 'pre_option_cache-enabler', [ $this, 'setDefaultPermalinkStructure' ] ],
		];
	}

	/**
	 * Apply default Cache Enabler settings.
	 *
	 * Version 1.15.3 mistakenly wrote default settings to the "cache-enabler" option instead of
	 * "cache_enabler", so this temporary task will clean it up if the settings haven't been
	 * changed from the defaults.
	 */
	public function migrateCacheEnablerSettings() {
		$settings = get_option( 'cache_enabler', [] );
		$expected = self::getCacheEnablerSettings();

		// Delete the legacy settings, which were previously being set by `wp nxmapps setup`.
		delete_option( 'cache-enabler' );

		// We're already running our defaults, so there's nothing to do.
		if ( $expected === $settings ) {
			return;
		}

		// We don't have any settings, so install the defaults.
		if ( empty( $settings ) ) {
			update_option( 'cache_enabler', $expected );
			return;
		}

		if ( ! method_exists( 'Cache_Enabler', 'get_default_settings' ) ) {
			return;
		}

		// Retrieve the defaults defined within the Cache_Enabler class.
		$method = new \ReflectionMethod( 'Cache_Enabler', 'get_default_settings' );
		$method->setAccessible( true );
		$defaults = $method->invoke( null );

		// They have been changed from the defaults, don't change customers' settings.
		if ( $settings !== $defaults ) {
			return;
		}

		// If they're running the defaults, apply our tweaked settings.
		update_option( 'cache_enabler', $expected );
	}

	/**
	 * Retrieve our default Cache Enabler settings.
	 *
	 * These settings correspond to the Cache_Enabler::settings_page() method.
	 *
	 * @link https://www.keycdn.com/support/wordpress-cache-enabler-plugin
	 *
	 * @return mixed[]
	 */
	public static function getCacheEnablerSettings() {
		/*
		 * A collection of paths that should be excluded.
		 *
		 * When adding a path here, it's essentially excluding the following pattern from the
		 * full-page cache:
		 *
		 *   /some-path*
		 */
		$excluded_paths = [
			'account',
			'addons',
			'administrator',
			'affiliate-area.php',
			'cart',
			'checkout',
			'events',
			'lock.php',
			'login',
			'mepr',
			'my-account',
			'page/ref',
			'ref',
			'register',
			'resetpass',
			'store',
			'thank-you',
			'wp-cron.php',
			'wp-includes',
			'wp-json',
			'xmlrpc.php',
		];

		/*
		 * A collection of cookie prefixes that, if present, should cause the cache to be bypassed.
		 */
		$excluded_cookie_prefixes = [
			'comment_author_',
			'wordpress_',
			'wp-postpass_',
			'wp-settings-',
			'wp-resetpass-',
			'wp_woocommerce_session',
			'woocommerce_',
			'mplk',
			'mp3pi141592pw',
		];

		// Escape anything that might break regular expressions.
		$excluded_cookie_prefixes = array_map( 'preg_quote', $excluded_cookie_prefixes );
		$excluded_paths           = array_map( function ( $path ) {
			return preg_quote( $path, '/' );
		}, $excluded_paths );

		// phpcs:disable WordPress.Arrays
		return [
			// Whether or not the cache should expire.
			'cache_expires' => 1,

			// Cached pages expire X hours after being created.
			'cache_expiry_time' => 2,

			// Clear the site cache if any post type has been published, updated, or trashed (instead of only the page and/or associated cache).
			'clear_site_cache_on_saved_post' => 0,

			// Clear the site cache if a comment has been saved (instead of only the page cache).
			'clear_site_cache_on_saved_comment' => 0,

			// Clear the site cache if any plugin has been activated, updated, or deactivated.
			'clear_site_cache_on_changed_plugin' => 0,

			// Create an additional cached version for WebP image support.
			'convert_image_urls_to_webp' => 0,

			// Pre-compress cached pages with Gzip.
			'compress_cache' => 0,

			// Minify HTML in cached pages.
			'minify_html' => 0,

			// Minify inline CSS + JavaScript.
			'minify_inline_css_js' => 0,

			// Post IDs that should bypass the cache.
			'excluded_post_ids' => '',

			// A regex matching page paths that should bypass the cache.
			'excluded_page_paths' => sprintf( '/^\/(%1$s)\/?/', implode( '|', $excluded_paths ) ),

			// A regex matching cookies that should bypass the cache.
			'excluded_cookies' => sprintf( '/^(?!wordpress_test_cookie)(%1$s).*/i', implode( '|', $excluded_cookie_prefixes ) ),
		];
		// phpcs:enable WordPress.Arrays
	}

	/**
	 * Ensure the wp-content/cache/ directory both exists and is writable.
	 *
	 * If the cache directory cannot be created/modified with the correct permissions, we'll run
	 * self::removeCacheEnablerRewriteRules() to ensure sites aren't trying to serve from a
	 * directory they can't read and/or write to.
	 *
	 * @return bool True if permissions were set/okay, false otherwise.
	 */
	public static function setCacheDirectoryPermissions() {
		$fs = Filesystem::init();

		try {
			$directories = [
				$fs->wp_content_dir() . '/cache',
				$fs->wp_content_dir() . '/cache/cache-enabler',
				$fs->wp_content_dir() . '/settings',
				$fs->wp_content_dir() . '/settings/cache-enabler',
			];

			foreach ( $directories as $dir ) {
				// Something exists here, but it's not a directory.
				if ( $fs->exists( $dir ) && ! $fs->is_dir( $dir ) ) {
					/*
					 * If the directory is a symlink, chances are someone has gone out of their way
					 * to set this up. Move along so we don't risk messing up whatever they might
					 * be doing.
					 */
					if ( is_link( $dir ) ) {
						continue;
					}

					throw new FilesystemException(
						sprintf( '%s exists, but is not a directory', $dir )
					);
				}

				// Create the directory if it doesn't exist.
				if ( ! $fs->exists( $dir ) && ! $fs->mkdir( $dir, 0755 ) ) {
					throw new FilesystemException(
						sprintf( 'Unable to create the directory %s', $dir )
					);
				}

				// Verify permissions.
				if ( '755' === $fs->getchmod( $dir ) ) {
					continue;
				}

				if ( ! $fs->chmod( $dir, 0755 ) ) {
					throw new FilesystemException(
						sprintf( 'Unable to set permissions for directory %s', $dir )
					);
				}
			}
		} catch ( FilesystemException $e ) {
			// Prevent sites from trying to serve caches that don't exist.
			self::removeCacheEnablerRewriteRules();
			return false;
		}

		return true;
	}

	/**
	 * Set a default permalink structure if one does not exist.
	 *
	 * Cache Enabler requires a non-default permalink structure in order to work. If no structure
	 * is set (e.g. "plain"), Cache Enabler will show a nag to users with the manage_options cap.
	 *
	 * To get around this, we can take advantage of the legacy option migration within the
	 * CacheEnabler::update_backend() method, where the plugin tries to move options from
	 * "cache-enabler" (dash) to "cache_enabler" (underscore). This is the only place within the
	 * plugin that it loads that option, so we won't be running this check every time it loads its
	 * configuration.
	 *
	 * When the legacy version is retrieved, also see if a permalink structure is defined. If not,
	 * set a default structure before Cache Enabler can bother users about it.
	 *
	 * @global $wp_rewrite
	 *
	 * @return false This will always return false to avoid short-circuiting the option loading.
	 */
	public static function setDefaultPermalinkStructure() {
		global $wp_rewrite;

		if ( empty( get_option( 'permalink_structure', '' ) ) ) {
			$wp_rewrite->set_permalink_structure( '/%postname%/' );
		}

		return false;
	}

	/**
	 * Inject the Cache Enabler rewrite rules into the site's Htaccess file.
	 *
	 * @link https://www.keycdn.com/support/wordpress-cache-enabler-plugin#apache
	 */
	public static function injectCacheEnablerRewriteRules() {
		$htaccess = ABSPATH . '.htaccess';

		// Before we do anything else, make sure the file permissions allow full-page caching.
		if ( ! self::setCacheDirectoryPermissions() ) {
			return;
		}

		// Don't add additional instructions.
		add_filter( 'insert_with_markers_inline_instructions', '__return_empty_array' );

		if ( file_exists( $htaccess ) && is_readable( $htaccess ) ) {
			// phpcs:ignore WordPress.WP.AlternativeFunctions.file_get_contents_file_get_contents
			$contents = (string) file_get_contents( $htaccess );
		} else {
			$contents = '';
		}

		// We haven't yet added Cache Enabler rules.
		if ( false === strpos( $contents, '# BEGIN ' . self::HTACCESS_MARKER ) ) {
			if ( false !== strpos( $contents, '# BEGIN WordPress' ) ) {
				$contents = str_replace(
					'# BEGIN WordPress',
					'# BEGIN ' . self::HTACCESS_MARKER . PHP_EOL . '# END ' . self::HTACCESS_MARKER . PHP_EOL . PHP_EOL . '# BEGIN WordPress',
					$contents
				);
			} else {
				$contents = '# BEGIN ' . self::HTACCESS_MARKER . PHP_EOL . '# END ' . self::HTACCESS_MARKER;
			}

			// phpcs:ignore WordPress.WP.AlternativeFunctions.file_system_read_file_put_contents
			file_put_contents( $htaccess, $contents );
		}

		// At this point, we should have an Htaccess file and the necessary markers.
		$rules = dirname( __DIR__ ) . '/snippets/cache-enabler-htaccess.conf';
		// phpcs:ignore WordPress.WP.AlternativeFunctions.file_get_contents_file_get_contents
		$lines = explode( PHP_EOL, (string) file_get_contents( $rules ) );

		return insert_with_markers( $htaccess, self::HTACCESS_MARKER, $lines );
	}

	/**
	 * Remove the Cache Enabler rewrite rules from the site's Htaccess file.
	 *
	 * @return bool True if the lines were found and removed, false otherwise.
	 */
	public static function removeCacheEnablerRewriteRules() {
		$htaccess = ABSPATH . '.htaccess';

		// Nothing to do or unable to act.
		if ( ! file_exists( $htaccess ) || ! is_readable( $htaccess ) ) {
			return false;
		}

		// phpcs:ignore WordPress.WP.AlternativeFunctions.file_get_contents_file_get_contents
		$original = (string) file_get_contents( $htaccess );

		/*
		 * Since the extract_from_markers() function doesn't grab the BEGIN/END tags, parse them
		 * using regular expressions.
		 *
		 * This pattern matches the "# BEGIN/END self::HTACCESS_MARKER" comments, along with
		 * anything in-between them (with a bit of whitespace normalization).
		 */
		$marker   = preg_quote( self::HTACCESS_MARKER, '/' );
		$regex    = "/\s*# BEGIN {$marker}\s+([\S\s]+)\s*# END {$marker}\s*/m";
		$contents = (string) preg_replace( $regex, PHP_EOL, $original );

		// If nothing's changed, return false.
		if ( trim( $contents ) === trim( $original ) ) {
			return false;
		}

		// phpcs:ignore WordPress.WP.AlternativeFunctions.file_system_read_file_put_contents
		return (bool) file_put_contents( $htaccess, $contents );
	}
}
