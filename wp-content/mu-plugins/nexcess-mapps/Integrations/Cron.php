<?php

/**
 * General cron configuration.
 */

namespace Nexcess\MAPPS\Integrations;

use Nexcess\MAPPS\Concerns\HasHooks;
use Nexcess\MAPPS\Concerns\HasWordPressDependencies;

class Cron extends Integration {
	use HasHooks;
	use HasWordPressDependencies;

	/**
	 * Determine whether or not this integration should be loaded.
	 *
	 * @return bool Whether or not this integration be loaded in this environment.
	 */
	public function shouldLoadIntegration() {
		// The "weekly" cron definition was added to WordPress core in version 5.4.
		return ! $this->siteIsAtLeastWordPressVersion( '5.4' );
	}

	/**
	 * Retrieve all filters for the integration.
	 *
	 * @return array[]
	 */
	protected function getFilters() {
		return [
			[ 'cron_schedules', [ $this, 'defineCronSchedules' ], -9999 ],
		];
	}

	/**
	 * Define any custom cron intervals.
	 *
	 * This callback should be run as early as possible to avoid potential conflicts with customer
	 * modifications (e.g. if they've defined what "weekly" should look like, use their definition
	 * instead of ours).
	 *
	 * @param array[] $schedules Registered cron schedules.
	 *
	 * @return array[] The filtered $schedules array.
	 */
	public function defineCronSchedules( $schedules ) {
		if ( ! isset( $schedules['weekly'] ) ) {
			$schedules['weekly'] = [
				'interval' => WEEK_IN_SECONDS,
				'display'  => _x( 'Once a Week', 'time interval', 'nexcess-mapps' ),
			];
		}

		return $schedules;
	}
}
