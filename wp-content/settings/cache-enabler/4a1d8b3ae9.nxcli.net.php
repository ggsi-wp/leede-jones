<?php
/**
 * Cache Enabler settings for https://4a1d8b3ae9.nxcli.net
 *
 * @since      1.5.0
 * @change     1.5.0
 *
 * @generated  03.02.2021 16:17:22
 */

return array (
  'version' => '1.6.2',
  'permalink_structure' => 'plain',
  'cache_expires' => 0,
  'cache_expiry_time' => 0,
  'clear_site_cache_on_saved_post' => 0,
  'clear_site_cache_on_saved_comment' => 0,
  'clear_site_cache_on_changed_plugin' => 0,
  'compress_cache' => 0,
  'convert_image_urls_to_webp' => 0,
  'excluded_post_ids' => '',
  'excluded_page_paths' => '',
  'excluded_query_strings' => '',
  'excluded_cookies' => '',
  'minify_html' => 0,
  'minify_inline_css_js' => 0,
);