jQuery(document).ready(function ($) {
    if (window.history.replaceState) {
        window.history.replaceState(null, null, window.location.href);
    }
    if($('#phone1').length > 0) {
        document.getElementById('phone1').onkeydown = function (e) {
            if (!e.key.match(/[0-9]/) && e.key !== 'Delete' && e.key !== 'Backspace' && e.key !== 'Tab') {
                e.preventDefault();
            }
        };
        document.getElementById('phone2').onkeydown = function (e) {
            if (!e.key.match(/[0-9]/) && e.key !== 'Delete' && e.key !== 'Backspace' && e.key !== 'Tab') {
                e.preventDefault();
            }
        };
        document.getElementById('phone3').onkeydown = function (e) {
            if (!e.key.match(/[0-9]/) && e.key !== 'Delete' && e.key !== 'Backspace' && e.key !== 'Tab') {
                e.preventDefault();
            }
        };
    }
    $(document).on('click', '.active-send-wrap', function (e) {
        e.preventDefault();
        $(this).parents('.box-phone').addClass('active');
    });
    $(document).on('click', '.cellphone-btn', function (e) {
        e.preventDefault();
        if ($('input[name="cellphone1"]').val() == '' && $('input[name="cellphone2"]').val() == '' && $('input[name="cellphone3"]').val() == '') {
            $('.error_phone_number').text('Please fill out this field');
            return false;
        }

        $('input[name="phone_number1"]').val($('input[name="cellphone1"]').val());
        $('input[name="phone_number2"]').val($('input[name="cellphone2"]').val());
        $('input[name="phone_number3"]').val($('input[name="cellphone3"]').val());

        $('input[name="hidden_first_name"]').val($('input[name="first_name"]').val());
        $('input[name="hidden_last_name"]').val($('input[name="last_name"]').val());
        $('input[name="hidden_address1"]').val($('input[name="address1"]').val());
        $('input[name="hidden_address2"]').val($('input[name="address2"]').val());
        $('input[name="hidden_country"]').val($('select[name="country"]').val());
        $('input[name="hidden_province"]').val($('select[name="province"]').val());
        $('input[name="hidden_city"]').val($('input[name="city"]').val());
        $('input[name="hidden_postal_code"]').val($('input[name="postal_code"]').val());
        $('input[name="hidden_advisor"]').val($('select[name="advisor"]').val());
        $('input[name="hidden_password"]').val($('input[name="password"]').val());
        $('input[name="hidden_confirm_password"]').val($('input[name="confirm_password"]').val());

        setTimeout(function () {
            $('form#cellphone-form').trigger('submit');
            $('form#hidden-form').trigger('submit');
        }, 100);
    });
    $(document).on('click', '.code-btn', function (e) {
        e.preventDefault();
        if ($('input[name="code_twilio1"]').val() == '') {
            $('.error_code_twilio').text('Please fill out this field');
            return false;
        }
        var code_twilio = $('input[name="code_twilio1"]').val().toString();
        $('input[name="code_twilio"]').val(code_twilio);

        $('input[name="hidden_first_name"]').val($('input[name="first_name"]').val());
        $('input[name="hidden_last_name"]').val($('input[name="last_name"]').val());
        $('input[name="hidden_address1"]').val($('input[name="address1"]').val());
        $('input[name="hidden_address2"]').val($('input[name="address2"]').val());
        $('input[name="hidden_country"]').val($('select[name="country"]').val());
        $('input[name="hidden_province"]').val($('select[name="province"]').val());
        $('input[name="hidden_city"]').val($('input[name="city"]').val());
        $('input[name="hidden_postal_code"]').val($('input[name="postal_code"]').val());
        $('input[name="hidden_advisor"]').val($('select[name="advisor"]').val());
        $('input[name="hidden_password"]').val($('input[name="password"]').val());
        $('input[name="hidden_confirm_password"]').val($('input[name="confirm_password"]').val());

        setTimeout(function () {
            $('form#code-form').trigger('submit');
        }, 100);
    });

    $('form.register_step1').on('submit', function () {
        var flag = true;
        var username = $('input[name="username"]');
        if (username.val() == '') {
            $('.error_username').text('Please fill out this field');
            flag = false;
        } else {
            $('.error_username').text('');
        }
        return flag;
    });

    $('form.register_step2').on('submit', function () {
        var flag = true;
        var position = undefined;
        //
        var password = $('input[name="password"]');
        var confirm_password = $('input[name="confirm_password"]');
        if (confirm_password.val() == '') {
            $('.error_confirm_password').text('Please fill out this field');
            flag = false;
        } else if (confirm_password.val() != password.val()) {
            $('.error_confirm_password').text("Passwords don't match");
            flag = false;
        } else {
            $('.error_confirm_password').text('');
        }
        var validated = true;
        var error_characters = '';
        var error_must_have = '';
        var error_number = '';
        var error_upper_case = '';
        var error_lower_case = '';
        if (!/[a-z]/.test(password.val())) {
            validated = false;
            error_lower_case += '1 lower case alpha';
        }
        if (!/[A-Z]/.test(password.val())) {
            validated = false;
            error_upper_case += '1 upper case';
            if (error_lower_case == '') {
                error_upper_case += ' alpha';
            } else {
                error_upper_case += ', ';
            }
        }
        if (!/\d/.test(password.val())) {
            validated = false;
            error_number += '1 number';
            if (error_upper_case != '' || error_lower_case != '') {
                error_characters += ', ';
            }
        }
        if (password.val().length < 8) {
            validated = false;
            error_characters = 'minimum 8 characters';
            if (error_number != '' || error_upper_case != '' || error_lower_case != '') {
                error_characters += ', ';
            }
        }
        if (error_number != '' || error_upper_case != '' || error_lower_case != '') {
            error_must_have += 'must have ';
        }
        var error_password = error_characters + error_must_have + error_number + error_upper_case + error_lower_case;
        if (password.val() == '') {
            $('.error_password').text('Please fill out this field');
            flag = false;
        } else if (validated === false) {
            $('.error_password').text(error_password);
            flag = false;
        } else {
            $('.error_password').text('');
        }
        //
        var advisor = $('select[name="advisor"]');
        if (advisor.val() == '') {
            $('.error_advisor').text('Please select your advisor');
            flag = false;
        } else {
            $('.error_advisor').text('');
        }
        //
        var postal_code = $('input[name="postal_code"]');
        if (postal_code.val() == '') {
            $('.error_postal_code').text('Please fill out this field');
            flag = false;
            position = postal_code.offset().top - 15 - $('.error_first_name').outerHeight(true) - $('.error_last_name').outerHeight(true) - $('.error_verify_phone_number').outerHeight(true) - $('.error_address1').outerHeight(true) - $('.error_city').outerHeight(true);
        } else {
            $('.error_postal_code').text('');
        }
        //
        var city = $('input[name="city"]');
        if (city.val() == '') {
            $('.error_city').text('Please fill out this field');
            flag = false;
            position = city.offset().top - 15 - $('.error_first_name').outerHeight(true) - $('.error_last_name').outerHeight(true) - $('.error_verify_phone_number').outerHeight(true) - $('.error_address1').outerHeight(true);
        } else {
            $('.error_city').text('');
        }
        //
        var address1 = $('input[name="address1"]');
        if (address1.val() == '') {
            $('.error_address1').text('Please fill out this field');
            flag = false;
            position = address1.offset().top - 15 - $('.error_first_name').outerHeight(true) - $('.error_last_name').outerHeight(true) - $('.error_verify_phone_number').outerHeight(true);
        } else {
            $('.error_address1').text('');
        }
        //
        var verify_phone_number = $('input[name="verify_phone_number"]');
        if (verify_phone_number.val() != 'sucessfully') {
            $('.error_verify_phone_number').text('Please verify your phone number');
            flag = false;
            position = $('#verify_phone_number').offset().top - 15 - $('.error_first_name').outerHeight(true) - $('.error_last_name').outerHeight(true);
        } else {
            $('.error_verify_phone_number').text('');
        }
        //
        var last_name = $('input[name="last_name"]');
        if (last_name.val() == '') {
            $('.error_last_name').text('Please fill out this field');
            flag = false;
            position = last_name.offset().top - 15 - $('.error_first_name').outerHeight(true);
        } else {
            $('.error_last_name').text('');
        }
        //
        var first_name = $('input[name="first_name"]');
        if (first_name.val() == '') {
            $('.error_first_name').text('Please fill out this field');
            flag = false;
            position = first_name.offset().top - 15;
        } else {
            $('.error_first_name').text('');
        }
        //
        if (position != undefined) {
            $("body, html").animate({scrollTop: position}, 'slow');
        }

        return flag;
    });
    if ($('#leede-login-form').length > 0) {
        $('#leede-login-form #username').attr('type', 'email');
        $('#leede-login-form .login-username').append('<span class="error error_username"></span>');
        $('#leede-login-form .login-password').append('<span class="error error_password"></span>');
    }
    $('form#leede-login-form').on('submit', function () {
        var flag = true;
        var username = $('#username');
        if (username.val() == '') {
            $('.error_username').text('Please fill out this field');
            flag = false;
        } else {
            $('.error_username').text('');
        }
        var password = $('#password');
        if (password.val() == '') {
            $('.error_password').text('Please fill out this field');
            flag = false;
        } else {
            $('.error_password').text('');
        }
        return flag;
    });
    $('form.change_password_form').on('submit', function () {
        var flag = true;
        //
        var current_password = $('input[name="current_password"]');
        var new_password = $('input[name="new_password"]');
        var confirm_new_password = $('input[name="confirm_new_password"]');
        if (confirm_new_password.val() == '') {
            $('.error_confirm_new_password').text('Please fill out this field');
            flag = false;
        } else if (confirm_password.val() !== new_password.val()) {
            $('.error_confirm_new_password').text("Passwords don't match");
            flag = false;
        } else {
            $('.error_confirm_new_password').text('');
        }
        var validated = true;
        var error_characters = '';
        var error_must_have = '';
        var error_number = '';
        var error_upper_case = '';
        var error_lower_case = '';
        if (!/[a-z]/.test(new_password.val())) {
            validated = false;
            error_lower_case += '1 lower case alpha';
        }
        if (!/[A-Z]/.test(new_password.val())) {
            validated = false;
            error_upper_case += '1 upper case';
            if (error_lower_case == '') {
                error_upper_case += ' alpha';
            } else {
                error_upper_case += ', ';
            }
        }
        if (!/\d/.test(new_password.val())) {
            validated = false;
            error_number += '1 number';
            if (error_upper_case != '' || error_lower_case != '') {
                error_characters += ', ';
            }
        }
        if (new_password.val().length < 8) {
            validated = false;
            error_characters = 'minimum 8 characters';
            if (error_number != '' || error_upper_case != '' || error_lower_case != '') {
                error_characters += ', ';
            }
        }
        if (error_number != '' || error_upper_case != '' || error_lower_case != '') {
            error_must_have += 'must have ';
        }
        var error_new_password = error_characters + error_must_have + error_number + error_upper_case + error_lower_case;
        if (new_password.val() == '') {
            $('.error_new_password').text('Please fill out this field');
            flag = false;
        } else if (validated == false) {
            $('.error_new_password').text(error_new_password);
            flag = false;
        } else {
            $('.error_new_password').text('');
        }
        if (current_password.val() == '') {
            $('.error_current_password').text('Please fill out this field');
            flag = false;
        } else {
            $('.error_current_password').text('');
        }
        return flag;
    });
    $(document).on('click', '.send-forgot-password', function (e) {
        e.preventDefault();
        $('form#send-forgot-password').trigger('submit');
    });
    $('form.forgot_password_step1').on('submit', function () {
        var flag = true;
        var username = $('input[name="username"]');
        if (username.val() == '') {
            $('.error_username').text('Please fill out this field');
            flag = false;
        } else {
            $('.error_username').text('');
        }
        return flag;
    });
    $('form.forgot_password_step3').on('submit', function () {
        var flag = true;
        //
        var new_password = $('input[name="new_password"]');
        var confirm_new_password = $('input[name="confirm_new_password"]');
        if (confirm_new_password.val() == '') {
            $('.error_confirm_new_password').text('Please fill out this field');
            flag = false;
        } else if (confirm_password.val() !== new_password.val()) {
            $('.error_confirm_new_password').text("Passwords don't match");
            flag = false;
        } else {
            $('.error_confirm_new_password').text('');
        }
        var validated = true;
        var error_characters = '';
        var error_must_have = '';
        var error_number = '';
        var error_upper_case = '';
        var error_lower_case = '';
        if (!/[a-z]/.test(new_password.val())) {
            validated = false;
            error_lower_case += '1 lower case alpha';
        }
        if (!/[A-Z]/.test(new_password.val())) {
            validated = false;
            error_upper_case += '1 upper case';
            if (error_lower_case == '') {
                error_upper_case += ' alpha';
            } else {
                error_upper_case += ', ';
            }
        }
        if (!/\d/.test(new_password.val())) {
            validated = false;
            error_number += '1 number';
            if (error_upper_case != '' || error_lower_case != '') {
                error_characters += ', ';
            }
        }
        if (new_password.val().length < 8) {
            validated = false;
            error_characters = 'minimum 8 characters';
            if (error_number != '' || error_upper_case != '' || error_lower_case != '') {
                error_characters += ', ';
            }
        }
        if (error_number != '' || error_upper_case != '' || error_lower_case != '') {
            error_must_have += 'must have ';
        }
        var error_new_password = error_characters + error_must_have + error_number + error_upper_case + error_lower_case;
        if (new_password.val() == '') {
            $('.error_new_password').text('Please fill out this field');
            flag = false;
        } else if (validated == false) {
            $('.error_new_password').text(error_new_password);
            flag = false;
        } else {
            $('.error_new_password').text('');
        }
        return flag;
    });
    //Nav active states
    var pathname = window.location.pathname;

    if (pathname == '/client-dashboard/') {
        $('.lj-client-navigation a[data-page="dash"]').addClass('nav-active');
    } else if (pathname == '/doxim-documents-page/') {
        var parts = window.location.search.substr(1).split("&");
        var $_GET = {};
        for (var i = 0; i < parts.length; i++) {
            var temp = parts[i].split("=");
            $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
        }
        if ($_GET['tab'] == 'account') {
            $('.lj-client-navigation a[data-page="monthly-statements"]').addClass('nav-active');
        } else if ($_GET['tab'] == 'trade') {
            $('.lj-client-navigation a[data-page="trade-confirmations"]').addClass('nav-active');
        } else if ($_GET['tab'] == 'tax') {
            $('.lj-client-navigation a[data-page="tax-documents"]').addClass('nav-active');
        } else if ($_GET['tab'] == 'reports') {
            $('.lj-client-navigation a[data-page="account-reports"]').addClass('nav-active');
        }
    } else if (pathname == '/sma-quarterlies/') {
        $('.lj-client-navigation a[data-page="sma"]').addClass('nav-active');
    } else if (pathname == '/account-forms/') {
        $('.lj-client-navigation a[data-page="account-forms"]').addClass('nav-active');
    } else if (pathname == '/welcome-package/') {
        $('.lj-client-navigation a[data-page="welcome-package"]').addClass('nav-active');
    } else if (pathname == '/manage-account/') {
        $('.lj-client-navigation a[data-page="change-password"]').addClass('nav-active');
    }

});