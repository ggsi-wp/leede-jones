<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( ! class_exists( 'Leede_Options' ) ) {
	class Leede_Options {

		public $args = array();
		public $sections = array();
		public $theme;
		public $ReduxFramework;

		public function __construct() {

			if ( ! class_exists( 'ReduxFramework' ) ) {
				return;
			}

			// This is needed. Bah WordPress bugs.  ;)
			if ( true == Redux_Helpers::isTheme( __FILE__ ) ) {
				$this->initSettings();
			} else {
				add_action( 'plugins_loaded', array( $this, 'initSettings' ), 10 );
			}

		}

		public function initSettings() {

			// Set the default arguments
			$this->setArguments();


			// Create the sections and fields
			$this->setSections();

			if ( ! isset( $this->args['opt_name'] ) ) { // No errors please
				return;
			}

			$this->ReduxFramework = new ReduxFramework( $this->sections, $this->args );
		}

		public function setSections() {
			//ACTUAL DECLARATION OF SECTIONS
			$this->sections[] = array(
				'title'  => __( 'General', 'leede' ),
				'icon'   => 'el el-cog',
				'fields' => array(
					array(
						'id'    => 'admin_email_address',
						'type'  => 'textarea',
						'title' => __( 'Administration Email Address', 'leede' ),
						'desc'  => __( '<em>Allow multiple email addresses separated by semi-colon.</em>', 'leede' ),
					),
					array(
						'id'          => 'passphrase',
						'type'        => 'password',
						'title'       => __( 'Passphrase', 'leede' ),
					),
				)
			);
			$this->sections[] = array(
				'title'  => __( 'MSSQL', 'leede' ),
				'icon'   => 'el el-network',
				'fields' => array(
					array(
						'id'          => 'mssql_servername',
						'type'        => 'text',
						'title'       => __( 'MSSQL Server Name', 'leede' ),
					),
					array(
						'id'          => 'mssql_database',
						'type'        => 'text',
						'title'       => __( 'MSSQL Database', 'leede' ),
					),
					array(
						'id'          => 'mssql_uid',
						'type'        => 'text',
						'title'       => __( 'MSSQL User ID', 'leede' ),
					),
					array(
						'id'          => 'mssql_pwd',
						'type'        => 'password',
						'title'       => __( 'MSSQL Password', 'leede' ),
					),
				)
			);
			$this->sections[] = array(
				'title'  => __( 'Twilio', 'leede' ),
				'icon'   => 'el el-phone-alt',
				'desc'   => __( '<em>Note: You can find here: <a href="https://www.twilio.com/console">https://www.twilio.com/console</a></em>', 'leede' ),
				'fields' => array(
					array(
						'id'    => 'twilio_phone_number',
						'type'  => 'text',
						'title' => __( 'Phone Number', 'leede' ),
					),
					array(
						'id'    => 'twilio_sid',
						'type'  => 'text',
						'title' => __( 'Account SID', 'leede' ),
					),
					array(
						'id'    => 'twilio_token',
						'type'  => 'text',
						'title' => __( 'Auth Token', 'leede' ),
					),
					array(
						'id'          => 'twilio_time',
						'type'        => 'text',
						'validate'    => 'numeric',
						'title'       => __( 'Code Expired', 'leede' ),
						'placeholder' => '300',
						'desc'        => __( '<em>Seconds</em>', 'leede' ),
					),
					array(
						'id'          => 'twilio_limit_sms',
						'type'        => 'text',
						'validate'    => 'numeric',
						'title'       => __( 'Limit Send SMS', 'leede' ),
						'placeholder' => '3'
					),
					array(
						'id'          => 'twilio_limit_code',
						'type'        => 'text',
						'validate'    => 'numeric',
						'title'       => __( 'Limit Send Code', 'leede' ),
						'placeholder' => '3'
					),
					array(
						'id'          => 'twilio_again',
						'type'        => 'text',
						'validate'    => 'numeric',
						'title'       => __( 'Try Again In', 'leede' ),
						'placeholder' => '3600',
						'desc'        => __( '<em>Seconds</em>', 'leede' ),
					),
				)
			);
			$this->sections[] = array(
				'title'  => __( 'Login / Logout', 'leede' ),
				'icon'   => 'el el-circle-arrow-right',
				'fields' => array(
					array(
						'id'    => 'login_content',
						'type'  => 'textarea',
						'title' => __( 'Login Content', 'leede' ),
						'desc'  => __( '<em>Note: Use code [leede_register_link text="Existing Client Registration Form" class="button"] to display register page link</em>', 'leede' ),
						'rows'  => 15
					),
					//
					array(
						'id'     => 'login_timeout_section',
						'type'   => 'section',
						'title'  => __( 'Login Timeout', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'login_timeout_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
					),
					array(
						'id'          => 'login_timeout_time',
						'type'        => 'text',
						'validate'    => 'numeric',
						'title'       => __( 'Timeout', 'leede' ),
						'placeholder' => '3600',
						'desc'        => __( '<em>Seconds</em>', 'leede' ),
					),
					//
					array(
						'id'     => 'login_failed_section',
						'type'   => 'section',
						'title'  => __( 'Login Failed', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'login_failed_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
					),
				)
			);
			$this->sections[] = array(
				'title'  => __( 'Register', 'leede' ),
				'icon'   => 'el el-edit',
				'fields' => array(
					array(
						'id'     => 'register_step1_verify_email_section',
						'type'   => 'section',
						'title'  => __( 'Registration - Step 1 - Verify Email', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'register_step1_verify_email_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'desc'  => __( '<em>Note: Use code [leede_register_link text="New Client Registration"] to display register page link</em>', 'leede' ),
					),
					//
					array(
						'id'     => 'register_step1_thanks_section',
						'type'   => 'section',
						'title'  => __( 'Thanks Page - Email Sent', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'register_step1_thanks_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'rows'  => 4
					),
					//
					array(
						'id'     => 'register_step1_error_section',
						'type'   => 'section',
						'title'  => __( 'Error - Email Already Used', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'register_step1_error_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'desc'  => __( '<em>Note: Use code [leede_template_link link="forgotpassword" text="reset your password."] to display forgot password page link</em>', 'leede' ),
						'rows'  => 4
					),
					//
					array(
						'id'     => 'register_step1_email_section',
						'type'   => 'section',
						'title'  => __( 'Email - To Client For Confirming Email Address', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'          => 'register_step1_email_subject',
						'type'        => 'text',
						'title'       => __( 'Subject', 'leede' ),
						'placeholder' => __( 'Confirming Email Address', 'leede' )
					),
					array(
						'id'    => 'register_step1_email_body',
						'type'  => 'textarea',
						'title' => __( 'Body', 'leede' ),
						'desc'  => __( '<em>Note: Use code [leede_logo] to display logo site and [leede_email_code_register] to display send security code.</em>', 'leede' ),
						'rows'  => 15
					),
					array(
						'id'    => 'register_step1_email_signature',
						'type'  => 'textarea',
						'title' => __( 'Signature', 'leede' ),
					),
					array(
						'id'          => 'register_step1_email_headers',
						'type'        => 'text',
						'title'       => __( 'Headers', 'leede' ),
						'placeholder' => __( 'From: Confirming Email Address <info@leedejones.ca>', 'leede' )
					),
					//
					array(
						'id'     => 'register_step1_expired_section',
						'type'   => 'section',
						'title'  => __( 'Failed - Link in email expired', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'register_step1_expired_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'desc'  => __( '<em>Note: Use code [leede_restart_form text="restart your registration."] to display reset register form.</em>', 'leede' ),
					),
					array(
						'id'          => 'register_step1_expired_time',
						'type'        => 'text',
						'validate'    => 'numeric',
						'title'       => __( 'Email Expired', 'leede' ),
						'placeholder' => '18000',
						'desc'        => __( '<em>Seconds</em>', 'leede' ),
					),
					//
					array(
						'id'     => 'register_step2_verify_account_section',
						'type'   => 'section',
						'title'  => __( 'Registration - Step 2 - Verify Account', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'register_step2_verify_account_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'rows'  => 3
					),
					array(
						'id'    => 'register_step2_address_content',
						'type'  => 'textarea',
						'title' => __( 'Address Confirmation', 'leede' ),
					),
					array(
						'id'    => 'register_step2_send_desc',
						'type'  => 'textarea',
						'title' => __( 'Send SMS Form Description', 'leede' ),
						'rows'  => 3
					),
					array(
						'id'    => 'register_step2_send_note',
						'type'  => 'textarea',
						'title' => __( 'Send SMS Form Note', 'leede' ),
						'rows'  => 4
					),
					array(
						'id'    => 'register_step2_verify_desc',
						'type'  => 'textarea',
						'title' => __( 'Verify Phone Number Form Description', 'leede' ),
						'rows'  => 5
					),
					array(
						'id'    => 'register_step2_success_desc',
						'type'  => 'textarea',
						'title' => __( 'Successful Verification Description', 'leede' ),
						'rows'  => 3
					),
					array(
						'id'    => 'register_step2_unsuccess_desc',
						'type'  => 'textarea',
						'title' => __( 'Unsuccessful Verification Description', 'leede' ),
						'rows'  => 5
					),
					//
					array(
						'id'     => 'register_step2_success_section',
						'type'   => 'section',
						'title'  => __( 'Registration Success', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'register_step2_success_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'desc'  => __( '<em>Note: Use code [leede_template_link link="login" text="Click here to login now."] to display login page link.</em>', 'leede' ),
					),
					//
					array(
						'id'     => 'register_step2_success_email_section',
						'type'   => 'section',
						'title'  => __( 'Success - Email To Advisor', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'          => 'register_step2_success_email_subject',
						'type'        => 'text',
						'title'       => __( 'Subject', 'leede' ),
						'placeholder' => __( 'Successful Registration Information', 'leede' )
					),
					array(
						'id'    => 'register_step2_success_email_body',
						'type'  => 'textarea',
						'title' => __( 'Body', 'leede' ),
						'desc'  => __( '<em>Note: Use code [leede_logo] to display logo site and [leede_username] to display username.</em>', 'leede' ),
						'rows'  => 10
					),
					array(
						'id'    => 'register_step2_success_email_signature',
						'type'  => 'textarea',
						'title' => __( 'Signature', 'leede' ),
					),
					array(
						'id'          => 'register_step2_success_email_headers',
						'type'        => 'text',
						'title'       => __( 'Headers', 'leede' ),
						'placeholder' => __( 'From: Successful Registration Information<info@leedejones.ca>', 'leede' )
					),
					//
					array(
						'id'     => 'register_step2_failed_section',
						'type'   => 'section',
						'title'  => __( 'Failed for Submitting a Registration', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'register_step2_failed_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
					),
					array(
						'id'          => 'register_step2_failed_count',
						'type'        => 'text',
						'validate'    => 'numeric',
						'title'       => __( 'Fail Count', 'leede' ),
						'placeholder' => '3',
					),
					array(
						'id'          => 'register_step2_failed_matches',
						'type'        => 'text',
						'validate'    => 'numeric',
						'title'       => __( 'Matches To Fail', 'leede' ),
						'placeholder' => '2',
					),
					//
					array(
						'id'     => 'register_step2_failed_email_section',
						'type'   => 'section',
						'title'  => __( 'Failed Email Notification - Incorrect Registration Information', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'          => 'register_step2_failed_email_subject',
						'type'        => 'text',
						'title'       => __( 'Subject', 'leede' ),
						'placeholder' => __( 'Incorrect Registration Information', 'leede' )
					),
					array(
						'id'    => 'register_step2_failed_email_body',
						'type'  => 'textarea',
						'title' => __( 'Body', 'leede' ),
						'desc'  => __( '<em>Note: Use code [leede_logo] to display logo site.</em>', 'leede' ),
						'rows'  => 10
					),
					array(
						'id'    => 'register_step2_failed_email_signature',
						'type'  => 'textarea',
						'title' => __( 'Signature', 'leede' ),
					),
					array(
						'id'          => 'register_step2_failed_email_headers',
						'type'        => 'text',
						'title'       => __( 'Headers', 'leede' ),
						'placeholder' => __( 'From: Incorrect Registration Information<info@leedejones.ca>', 'leede' )
					),
				)
			);
			$this->sections[] = array(
				'title'  => __( 'Forgot Password', 'leede' ),
				'icon'   => 'el el-key',
				'fields' => array(
					array(
						'id'    => 'forgot_password_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'rows'  => 3
					),
					array(
						'id'     => 'forgot_password_thanks_section',
						'type'   => 'section',
						'title'  => __( 'Thanks Page - Email Sent', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'forgot_password_thanks_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
					),
					//
					array(
						'id'     => 'forgot_password_email_section',
						'type'   => 'section',
						'title'  => __( 'Forgot Password Email Notification', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'          => 'forgot_password_email_subject',
						'type'        => 'text',
						'title'       => __( 'Subject', 'leede' ),
						'placeholder' => __( 'Forgot Password Email Notification', 'leede' )
					),
					array(
						'id'    => 'forgot_password_email_body',
						'type'  => 'textarea',
						'title' => __( 'Body', 'leede' ),
						'desc'  => __( '<em>Note: Use code [leede_logo] to display logo site, [leede_username] to display username and [leede_email_code_forgot_password] to display send security code.</em>', 'leede' ),
						'rows'  => 15
					),
					array(
						'id'    => 'forgot_password_email_signature',
						'type'  => 'textarea',
						'title' => __( 'Signature', 'leede' ),
					),
					array(
						'id'          => 'forgot_password_email_headers',
						'type'        => 'text',
						'title'       => __( 'Headers', 'leede' ),
						'placeholder' => __( 'From: Forgot Password Email Notification <info@leedejones.ca>', 'leede' )
					),
					//
					array(
						'id'     => 'forgot_password_expired_section',
						'type'   => 'section',
						'title'  => __( 'Failed - Link in email expired', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'forgot_password_expired_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'desc'  => __( '<em>Note: Use code [leede_restart_form text="password reset"] to display reset forgot password form.</em>', 'leede' ),
						'rows'  => 7
					),
					array(
						'id'          => 'forgot_password_expired_time',
						'type'        => 'text',
						'validate'    => 'numeric',
						'title'       => __( 'Email Expired', 'leede' ),
						'placeholder' => '18000',
						'desc'        => __( '<em>Seconds</em>', 'leede' ),
					),
					//
					array(
						'id'     => 'forgot_password_reset_section',
						'type'   => 'section',
						'title'  => __( 'Reset Password', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'forgot_password_reset_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
					),
					//
					array(
						'id'     => 'forgot_password_verify_section',
						'type'   => 'section',
						'title'  => __( 'Verify Security Code', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'forgot_password_verify_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'desc'  => __( '<em>Note: Use code [leede_send_sms_button class="inline-form" text="here"] to display resend link.</em>', 'leede' ),
						'rows'  => 8
					),
					//
					array(
						'id'     => 'forgot_password_success_section',
						'type'   => 'section',
						'title'  => __( 'Success Change Mobile SMS', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'forgot_password_success_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'rows'  => 3
					),
					//
					array(
						'id'     => 'forgot_password_thank_section',
						'type'   => 'section',
						'title'  => __( 'Thank you', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'forgot_password_thank_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'desc'  => __( '<em>Note: Use code [leede_template_link link="login" text="here"] to display login page link.</em>', 'leede' ),
						'rows'  => 3
					),
					//
					array(
						'id'     => 'forgot_password_failed_section',
						'type'   => 'section',
						'title'  => __( 'Failed - Verify Security Code', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'forgot_password_failed_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'desc'  => __( '<em>Note: Use code [leede_send_sms_button text="Send Code" ] to display go back button.</em>', 'leede' ),
					),
					//
					array(
						'id'     => 'forgot_password_many_failed_section',
						'type'   => 'section',
						'title'  => __( 'Failed - Too many failed attempts', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'forgot_password_many_failed_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'rows'  => 4
					),
				)
			);
			$this->sections[] = array(
				'title'  => __( 'Manage Profile', 'leede' ),
				'icon'   => 'el el-user',
				'fields' => array(
					array(
						'id'     => 'manage_profile_address_section',
						'type'   => 'section',
						'title'  => __( 'Change Address', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'manage_profile_address_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'rows'  => 10
					),
					array(
						'id'     => 'manage_profile_mobile_section',
						'type'   => 'section',
						'title'  => __( 'Change Mobile SMS', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'manage_profile_mobile_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'rows'  => 7
					),
					//
					array(
						'id'     => 'manage_profile_verify_section',
						'type'   => 'section',
						'title'  => __( 'Verify Security Code', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'manage_profile_verify_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'desc'  => __( '<em>Note: Use code [leede_back_form text="here"] to display resend link.</em>', 'leede' ),
						'rows'  => 8
					),
					//
					array(
						'id'     => 'manage_profile_success_section',
						'type'   => 'section',
						'title'  => __( 'Success Change Mobile SMS', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'manage_profile_success_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'rows'  => 4
					),
					//
					array(
						'id'     => 'manage_profile_failed_section',
						'type'   => 'section',
						'title'  => __( 'Failed - Verify Security Code', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'manage_profile_failed_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'desc'  => __( '<em>Note: Use code [leede_back_form class="button" text="Go Back" ] to display go back button.</em>', 'leede' ),
					),
					//
					array(
						'id'     => 'manage_profile_many_failed_section',
						'type'   => 'section',
						'title'  => __( 'Failed - Too many failed attempts', 'fftrust' ),
						'indent' => false
					),
					array(
						'id'    => 'manage_profile_many_failed_content',
						'type'  => 'textarea',
						'title' => __( 'Content', 'leede' ),
						'rows'  => 4
					),
				)
			);
		}

		/**
		 * All the possible arguments for Redux.
		 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
		 * */
		public function setArguments() {

			$theme = wp_get_theme(); // For use with some settings. Not necessary.

			$this->args = array(
				// TYPICAL -> Change these values as you need/desire
				'opt_name'        => 'leede_options',
				// This is where your data is stored in the database and also becomes your global variable name.
				'display_name'    => $theme->get( 'Name' ),
				// Name that appears at the top of your panel
				'display_version' => $theme->get( 'Version' ),
				// Version that appears at the top of your panel
				'menu_type'       => 'menu',
				//Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
				'allow_sub_menu'  => true,
				// Show the sections below the admin menu item or not
				'menu_title'      => __( 'Leede Options', 'leede' ),
				'page_title'      => __( 'Leede Options', 'leede' ),
				// You will need to generate a Google API key to use this feature.
				// Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
				'google_api_key'  => 'AIzaSyAs0iVWrG4E_1bG244-z4HRKJSkg7JVrVQ',
				// Must be defined to add google fonts to the typography module

				'async_typography'    => false,
				// Use a asynchronous font on the front end or font string
				'admin_bar'           => true,
				// Show the panel pages on the admin bar
				'global_variable'     => '',
				// Set a different name for your global variable other than the opt_name
				'dev_mode'            => false,
				// Show the time the page took to load, etc
				'customizer'          => true,
				// Enable basic customizer support

				// OPTIONAL -> Give you extra features
				'page_priority'       => null,
				// Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
				'page_parent'         => 'themes.php',
				// For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
				'page_permissions'    => 'manage_options',
				// Permissions needed to access the options panel.
				'menu_icon'           => '',
				// Specify a custom URL to an icon
				'last_tab'            => '',
				// Force your panel to always open to a specific tab (by id)
				'page_icon'           => 'icon-themes',
				// Icon displayed in the admin panel next to your menu_title
				'page_slug'           => 'leede_options',
				// Page slug used to denote the panel
				'save_defaults'       => true,
				// On load save the defaults to DB before user clicks save or not
				'default_show'        => false,
				// If true, shows the default value next to each field that is not the default value.
				'default_mark'        => '',
				// What to print by the field's title if the value shown is default. Suggested: *
				'show_import_export'  => true,
				// Shows the Import/Export panel when not used as a field.

				// CAREFUL -> These options are for advanced use only
				'transient_time'      => 60 * MINUTE_IN_SECONDS,
				'output'              => true,
				// Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
				'output_tag'          => true,
				// Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
				// 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

				// FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
				'database'            => '',
				// possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
				'system_info'         => false,
				// REMOVE
				'show_options_object' => false,
				// HINTS
				'hints'               => array(
					'icon'          => 'icon-question-sign',
					'icon_position' => 'right',
					'icon_color'    => 'lightgray',
					'icon_size'     => 'normal',
					'tip_style'     => array(
						'color'   => 'light',
						'shadow'  => true,
						'rounded' => false,
						'style'   => '',
					),
					'tip_position'  => array(
						'my' => 'top left',
						'at' => 'bottom right',
					),
					'tip_effect'    => array(
						'show' => array(
							'effect'   => 'slide',
							'duration' => '500',
							'event'    => 'mouseover',
						),
						'hide' => array(
							'effect'   => 'slide',
							'duration' => '500',
							'event'    => 'click mouseleave',
						),
					),
				)
			);
		}

	}

	global $reduxConfig;
	$reduxConfig = new Leede_Options();
}
add_action( 'redux/options/leede_options/saved', 'leede_create_table_login_report' );
function leede_create_table_login_report() {
	require_once ABSPATH . 'wp-admin/includes/upgrade.php';
	global $wpdb;
	$tablename       = $wpdb->prefix . 'login_report';
	$main_sql_create = "CREATE TABLE " . $tablename . " ( 
		login_report_id bigint(20) unsigned NOT NULL auto_increment, 
		ID bigint(20) unsigned NOT NULL default '0', 
		user_login varchar(255), 
		login_report_status varchar(11), 
		login_report_datetime  datetime, 
		PRIMARY KEY (login_report_id)) ";
	maybe_create_table( $tablename, $main_sql_create );
}