<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="wrap">
    <h1 class="wp-heading-inline"><?php echo esc_html__( 'Logs & Reporting', 'leede' ); ?></h1>
    <hr class="wp-header-end">
    <p><?php echo esc_html__( 'Use this section to export logs of system usage', 'leede' ); ?></p>
    <form method="post" action="">
        <h3><?php echo esc_html__( 'Login Report', 'leede' ); ?>
            <button class="download" name="export_login_report" type="submit">
                <?php echo esc_html__( '[download]', 'leede' ); ?>
            </button>
        </h3>
        <p><?php echo esc_html__( 'An excel/csv file containing a list of all login attempts to the system, success or failure, over the past 30 days.', 'leede' ); ?></p>
    </form>
    <br>
    <h3><?php echo esc_html__( 'Registration Report', 'leede' ); ?></h3>
    <p><?php echo esc_html__( "Not required, all registration form submissions, success or failure, are listed in the client area and have clients simply 'pending' or 'approved' for access.", 'leede' ); ?></p>
<!--    <form method="post" enctype="multipart/form-data">-->
<!--        <div class="file-upload">-->
<!--            <input type="file" name="chooseFile">-->
<!--            <input type="submit" name="upload">-->
<!--        </div>-->
<!--    </form>-->
<!--	--><?php
//	if ( isset( $_POST['upload'] ) ) {
//		if ( ( $handle = fopen( $_FILES['chooseFile']['tmp_name'], "r" ) ) !== false ) {
//			require_once ABSPATH . 'wp-admin/includes/upgrade.php';
//			global $wpdb;
//
//			$tablename       = $wpdb->prefix . 'clients';
//			$main_sql_create = "CREATE TABLE " . $tablename . " (
//			ID int NOT NULL AUTO_INCREMENT,
//            Client_Code int NOT NULL,
//            Client int NOT NULL,
//            Account_Number nvarchar(50) NOT NULL,
//            Document nvarchar(50) NOT NULL,
//            Comments_Docs int NOT NULL,
//            AdPL_Line_1 nvarchar(50) NOT NULL,
//            AdPL_Line_2 nvarchar(50) NOT NULL,
//            AdPL_Line_3 nvarchar(50) NOT NULL,
//            AdPL_Line_4 nvarchar(50) NULL,
//            AdPL_Line_5 nvarchar(11) NULL,
//            AdPL_Line_6 nvarchar(11) NULL,
//            AdPL_Line_7 nvarchar(11) NULL,
//            AdPL_Line_8 nvarchar(11) NULL,
//            Cell_Phone nvarchar(50) NULL,
//            City nvarchar(50) NOT NULL,
//            Civic_AdPLess nvarchar(50) NOT NULL,
//            Civic_Number nvarchar(50) NOT NULL,
//            Country nvarchar(50) NOT NULL,
//            E_Stmt nvarchar(50) NOT NULL,
//            Home_Phone nvarchar(50) NULL,
//            Other_Phone nvarchar(50) NULL,
//            Other_Phone2 nvarchar(11) NULL,
//            Other_Phone3 nvarchar(11) NULL,
//            Other_Phone4 nvarchar(11) NULL,
//            Postal_Code_ZIP nvarchar(50) NOT NULL,
//            Province_State nvarchar(50) NOT NULL,
//            Street nvarchar(50) NOT NULL,
//            Street_TyBC nvarchar(50) NULL,
//            Structured_AdPLess nvarchar(50) NOT NULL,
//            Unit_Number nvarchar(50) NULL,
//            Work_Phone nvarchar(50) NULL,
//            First_Name_Client nvarchar(50) NULL,
//            Last_Name_Client nvarchar(50) NOT NULL,
//            Email nvarchar(50) NOT NULL,
//            E_mail_2 nvarchar(50) NULL,
//            E_mail_3 nvarchar(50) NULL,
//            E_mail_4 nvarchar(50) NULL,
//            IA_Client nvarchar(50) NOT NULL,
//            PRIMARY KEY (ID))";
//			maybe_create_table( $tablename, $main_sql_create );
//
//			$wpdb->query( "TRUNCATE TABLE {$wpdb->prefix}clients" );
//
//			$flag = true;
//			while ( ( $filesop = fgetcsv( $handle, 10000, "," ) ) !== false ) {
//				if ( $flag ) {
//					$flag = false;
//					continue;
//				}
//				$data = array(
//					'Client_Code'        => $filesop[0] != 'NULL' ? $filesop[0] : null,
//					'Client'             => $filesop[1] != 'NULL' ? $filesop[1] : null,
//					'Account_Number'     => $filesop[2] != 'NULL' ? $filesop[2] : null,
//					'Document'           => $filesop[3] != 'NULL' ? $filesop[3] : null,
//					'Comments_Docs'      => $filesop[4] != 'NULL' ? $filesop[4] : null,
//					'AdPL_Line_1'        => $filesop[5] != 'NULL' ? $filesop[5] : null,
//					'AdPL_Line_2'        => $filesop[6] != 'NULL' ? $filesop[6] : null,
//					'AdPL_Line_3'        => $filesop[7] != 'NULL' ? $filesop[7] : null,
//					'AdPL_Line_4'        => $filesop[8] != 'NULL' ? $filesop[8] : null,
//					'AdPL_Line_5'        => $filesop[9] != 'NULL' ? $filesop[9] : null,
//					'AdPL_Line_6'        => $filesop[10] != 'NULL' ? $filesop[10] : null,
//					'AdPL_Line_7'        => $filesop[11] != 'NULL' ? $filesop[11] : null,
//					'AdPL_Line_8'        => $filesop[12] != 'NULL' ? $filesop[12] : null,
//					'Cell_Phone'         => $filesop[13] != 'NULL' ? $filesop[13] : null,
//					'City'               => $filesop[14] != 'NULL' ? $filesop[14] : null,
//					'Civic_AdPLess'      => $filesop[15] != 'NULL' ? $filesop[15] : null,
//					'Civic_Number'       => $filesop[16] != 'NULL' ? $filesop[16] : null,
//					'Country'            => $filesop[17] != 'NULL' ? $filesop[17] : null,
//					'E_Stmt'             => $filesop[18] != 'NULL' ? $filesop[18] : null,
//					'Home_Phone'         => $filesop[19] != 'NULL' ? $filesop[19] : null,
//					'Other_Phone'        => $filesop[20] != 'NULL' ? $filesop[20] : null,
//					'Other_Phone2'       => $filesop[21] != 'NULL' ? $filesop[21] : null,
//					'Other_Phone3'       => $filesop[22] != 'NULL' ? $filesop[22] : null,
//					'Other_Phone4'       => $filesop[23] != 'NULL' ? $filesop[23] : null,
//					'Postal_Code_ZIP'    => $filesop[24] != 'NULL' ? $filesop[24] : null,
//					'Province_State'     => $filesop[25] != 'NULL' ? $filesop[25] : null,
//					'Street'             => $filesop[26] != 'NULL' ? $filesop[26] : null,
//					'Street_TyBC'        => $filesop[27] != 'NULL' ? $filesop[27] : null,
//					'Structured_AdPLess' => $filesop[28] != 'NULL' ? $filesop[28] : null,
//					'Unit_Number'        => $filesop[29] != 'NULL' ? $filesop[29] : null,
//					'Work_Phone'         => $filesop[30] != 'NULL' ? $filesop[30] : null,
//					'First_Name_Client'  => $filesop[31] != 'NULL' ? $filesop[31] : null,
//					'Last_Name_Client'   => $filesop[32] != 'NULL' ? $filesop[32] : null,
//					'Email'              => $filesop[33] != 'NULL' ? $filesop[33] : null,
//					'E_mail_2'           => $filesop[34] != 'NULL' ? $filesop[34] : null,
//					'E_mail_3'           => $filesop[35] != 'NULL' ? $filesop[35] : null,
//					'E_mail_4'           => $filesop[36] != 'NULL' ? $filesop[36] : null,
//					'IA_Client'          => $filesop[37] != 'NULL' ? $filesop[37] : null,
//				);
//				echo "<pre>";
//				print_r($data);
//				echo "</pre>";
//				$wpdb->insert( $wpdb->prefix . 'clients', $data );
//			}
//			fclose( $handle );
//		}
//	}
//	?>
</div>