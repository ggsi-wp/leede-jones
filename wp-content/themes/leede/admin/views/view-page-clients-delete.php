<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( isset( $_POST['delete'] ) ) {
	$id = (int) $_POST['delete'];

	$post_id = '';
	$client_query = new WP_Query(
		array(
			'post_type'  => 'client',
			'meta_query' => array(
				array(
					'key'   => 'client_user_id',
					'value' => $id
				)
			)
		) );
	while ( $client_query->have_posts() ) :
		$client_query->the_post();
		$post_id = get_the_ID();
	endwhile;
	wp_reset_postdata();
	if ( ! empty( $post_id )) {
		wp_delete_post( $post_id, true );
	}
	wp_delete_user( $id );
	print('<script>window.location.href="admin.php?page=clients"</script>');
	exit;
}
$id = (int) $_GET['client_id'];
if ( !$id ) {
	echo "<h2>Invalid client ID !!!</h2>";
	exit;
}
$client = leede_get_client( $id );
if ( ! $client ) {
	echo "<h2>Invalid client ID !!!</h2>";
	exit;
}
$username    = $client->data->user_login;
?>
<div class="wrap">
    <h1 class="wp-heading-inline"><?php echo esc_html__( 'Delete Client', 'leede' ) ?></h1>
    <hr class="wp-header-end">
    <form id="clients-delete" method="post" action="">
        <p>
			<?php echo esc_html__( 'You have specified this user for deletion:', 'leede' ) ?>
        </p>
        <ul>
            <li>
                <?php echo $username;?>
                <input type="hidden" name="delete" value="<?php echo $client->ID ?>">
            </li>
        </ul>
        <p class="submit"><input type="submit" name="submit" class="button button-primary" value="Confirm Deletion"></p>
    </form>
</div>