<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

add_shortcode( 'leede_restart_form', 'leede_restart_form' );
function leede_restart_form( $atts ) {
	$atts = shortcode_atts( array(
		'text' => 'restart',
	), $atts, 'leede_restart_form' );

	return "<form method='post' class='inline-form' action=''><button type='submit' name='restart_form'>{$atts['text']}</button></form>";
}

add_shortcode( 'leede_send_sms_button', 'leede_send_sms_button' );
function leede_send_sms_button( $atts ) {
	$atts = shortcode_atts( array(
		'class' => '', //inline-form
		'text'  => 'Send Code',
	), $atts, 'leede_sms_form' );

	return "<form class='{$atts['class']}' method='post' action=''><button type='submit' name='submit_sms_twilio'>{$atts['text']}</button></form>";
}

add_shortcode( 'leede_verify_code_form', 'leede_verify_code_form' );
function leede_verify_code_form( $atts ) {
	$atts = shortcode_atts( array(
		'text' => 'Verify',
	), $atts, 'leede_verify_code_form' );

	return "<form method='post' action=''><div class='cellphone-code'><input title='code' type='text' name='code_twilio1' maxlength='7'></div><button type='submit' name='submit_code_twilio'>{$atts['text']}</button></form>";
}


add_shortcode( 'leede_back_form', 'leede_back_form' );
function leede_back_form( $atts ) {
	$atts = shortcode_atts( array(
		'class' => '',//button
		'text'  => 'Go Back',
	), $atts, 'leede_back_form' );

	return "<a href='#' class='active-send-wrap {$atts['class']}'>{$atts['text']}</a>";
}

add_shortcode( 'leede_email_code_register', 'leede_email_code_register' );
function leede_email_code_register() {
	if ( $_SESSION['register_step']['username'] && $_SESSION['register_step']['time'] ) {
		return leede_getTplPageURL( 'templates/page-register.php' ) . '?send_security_code=' . base64_encode( $_SESSION['register_step']['username'] . ' ' . $_SESSION['register_step']['time'] );
	} else {
		return '';
	}
}

add_shortcode( 'leede_logo', 'leede_logo' );
function leede_logo() {
	return '<img alt="img" src="' . get_stylesheet_directory_uri() . '/images/logo.jpg' . '"/>';
}

add_shortcode( 'leede_template_link', 'leede_template_link' );
function leede_template_link( $atts ) {
	$atts = shortcode_atts( array(
		'class' => '',
		'link'  => 'login',
		'text'  => 'here',
	), $atts, 'leede_template_link' );

	$template = "templates/page-" . $atts['link'] . ".php";
	$link     = leede_getTplPageURL( $template );

	return "<a class='{$atts['class']}' href='" . $link . "'>{$atts['text']}</a>";
}

add_shortcode( 'leede_register_link', 'leede_register_link' );
function leede_register_link( $atts ) {
	$atts = shortcode_atts( array(
		'class' => '',
		'text' => 'New Client Registration',
	), $atts, 'leede_register_link' );

	$template = "templates/page-register.php";
	$link     = leede_getTplPageURL( $template );

	return "<form method='post' action='" . $link . "'><button class='{$atts['class']}' type='submit' name='restart_form'>{$atts['text']}</button></form>";
}

add_shortcode( 'leede_username', 'leede_username' );
function leede_username() {
	if ( ! empty( $_SESSION['forgot_password_step']['username'] ) ) {
		$first_name = get_user_meta( $_SESSION['forgot_password_step']['username'], 'first_name', true );
		$last_name  = get_user_meta( $_SESSION['forgot_password_step']['username'], 'last_name', true );

		return $first_name . ' ' . $last_name;
	} else {
		return '';
	}
}

add_shortcode( 'leede_email_code_forgot_password', 'leede_email_code_forgot_password' );
function leede_email_code_forgot_password() {
	if ( $_SESSION['forgot_password_step']['username'] && $_SESSION['forgot_password_step']['time'] ) {
		return leede_getTplPageURL( 'templates/page-forgotpassword.php' ) . '?send_security_code=' . base64_encode( $_SESSION['forgot_password_step']['username'] . ' ' . $_SESSION['forgot_password_step']['time'] );
	} else {
		return '';
	}
}

//======
add_shortcode( 'leede_polylang_langswitcher', 'leede_polylang_langswitcher' );
function leede_polylang_langswitcher() {
	$output = '';
	if ( function_exists( 'pll_the_languages' ) ) {
		$args   = [
			'show_flags' => 0,
			'show_names' => 1,
			'echo'       => 0,
		];
		$output = '<ul class="polylang_langswitcher">' . pll_the_languages( $args ) . '</ul>';
	}

	return $output;
}

add_shortcode( 'leede_displayLogin', 'leede_displayLogin' );
function leede_displayLogin() { ?>
    <div class="header-widget widget_nav_menu header-right-widget">
        <div class="lj-login-wrapper">
            <div class="switch-lang"><a href="<?php echo site_url(); ?>/fr/">Francais</a></div>
            <div class="lj-login">
				<?php if ( is_user_logged_in() ) { ?>
                    <a href="<?php echo leede_getTplPageURL( 'templates/page-manage-profile.php' ); ?>" class="lj-btn">
						<?php echo esc_html__( 'Dashboard', 'leede' ); ?>
                    </a>
                    <a href="<?php echo wp_logout_url( leede_getTplPageURL( 'templates/page-login.php' ) ); ?>"
                       class="lj-btn">
						<?php echo esc_html__( 'Logout', 'leede' ); ?>
                    </a>
				<?php } else { ?>
                    <a href="<?php echo leede_getTplPageURL( 'templates/page-login.php' ); ?>" class="lj-btn">
						<?php echo esc_html__( 'Client Login', 'leede' ) ?>
                    </a>
				<?php } ?>
            </div>
        </div>
    </div>
<?php }

add_shortcode( 'leede_manage_profile_sidebar', 'leede_manage_profile_sidebar' );
function leede_manage_profile_sidebar() {
	if ( ! is_user_logged_in() || ( is_user_logged_in() && ! in_array( 'client', wp_get_current_user()->roles ) ) ) {
		wp_redirect( leede_getTplPageURL( 'templates/page-login.php' ) );
		exit;
	}
    ?>
    <article>
        <div class="welcome_roundel"><img alt="img" src="<?php echo get_stylesheet_directory_uri() . '/images/manager_profile.png'?>"/></div>
        <a href="<?php echo leede_getTplPageURL( 'templates/page-manage-profile.php' ); ?>">
            <h3><?php echo esc_html__( 'Manage Profile', 'leede' ); ?></h3>
        </a>
        <ul>
            <li><a href="<?php echo leede_getTplPageURL( 'templates/page-change-password.php' ); ?>"><?php echo esc_html__( 'Change Password', 'leede' ); ?></a></li>
            <li><a href="<?php echo leede_getTplPageURL( 'templates/page-change-address.php' ); ?>"><?php echo esc_html__( 'Change Address', 'leede' ); ?></a></li>
            <li><a href="<?php echo leede_getTplPageURL( 'templates/page-change-mobile-sms.php' ); ?>"><?php echo esc_html__( 'Change Cell Phone Number', 'leede' ); ?></a></li>
        </ul>
    </article>
<?php }


