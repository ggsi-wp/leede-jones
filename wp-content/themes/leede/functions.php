<?php

// enqueue the child theme stylesheet

Function wp_schools_enqueue_scripts() {
	wp_register_style( 'childstyle', get_stylesheet_directory_uri() . '/style.css' );
	wp_enqueue_style( 'childstyle' );
	wp_enqueue_script( 'childjs', get_stylesheet_directory_uri() . '/site-functions.js', array(), '', true );
	wp_register_style( 'postescanadastyle', 'http://ws1.postescanada-canadapost.ca/css/addresscomplete-2.30.min.css?key=nf32-ru33-uu86-af99' );
	wp_enqueue_style( 'postescanadastyle' );
	wp_enqueue_script( 'postescanadajs', 'http://ws1.postescanada-canadapost.ca/js/addresscomplete-2.30.min.js?key=nf32-ru33-uu86-af99', array(), '', true );
}

add_action( 'wp_enqueue_scripts', 'wp_schools_enqueue_scripts', 11 );

//remove unnecessary menus from lefthand navigation

function remove_menus() {
//	remove_menu_page( 'edit.php?post_type=client' );                   //client
	remove_menu_page( 'edit.php?post_type=slides' );                   //Qode Slider
	remove_menu_page( 'edit.php?post_type=portfolio_page' );         //Qode Portfolio
	remove_menu_page( 'edit.php?post_type=testimonials' );           //Qode Testimonials
	remove_menu_page( 'edit.php?post_type=carousels' );              //Qode Carousels
	remove_menu_page( 'edit.php?post_type=masonry_gallery' );        //Qode Masonry Gallery
	remove_menu_page( 'vc-general' );            //WPBakery
}

add_action( 'admin_menu', 'remove_menus' );

/* change font in editor */

add_action( 'admin_head-post.php', 'devpress_fix_html_editor_font' );
add_action( 'admin_head-post-new.php', 'devpress_fix_html_editor_font' );

function devpress_fix_html_editor_font() { ?>
    <style type="text/css">#wp-content-editor-container #content, #editorcontainer #content, #wp_mce_fullscreen {
            font-family: verdana;
            font-size: 13px;
        }</style>
<?php }

show_admin_bar( false );

/* shortcode for the current year */
function year_shortcode() {
	$year = date( 'Y' );

	return $year;
}

add_shortcode( 'year', 'year_shortcode' );

function _remove_script_version( $src ) {
	$parts = explode( '?ver', $src );

	return $parts[0];
}

add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );

add_filter( 'tablepress_edit_link_below_table', '__return_false' );

if ( ! function_exists( 'qode_remove_max_srcset' ) ) {
	function qode_remove_max_srcset() {
		return 1;
	}

	add_filter( 'max_srcset_image_width', 'qode_remove_max_srcset' );
}

function my_simple_crypt( $string, $action = 'e' ) {
	// you may change these values to your own
	$secret_key = 'LeedeJones';
	$secret_iv  = 'GSSI';

	$output         = false;
	$encrypt_method = "AES-256-CBC";
	$key            = hash( 'sha256', $secret_key );
	$iv             = substr( hash( 'sha256', $secret_iv ), 0, 16 );

	if ( $action == 'e' ) {
		$output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
	} else if ( $action == 'd' ) {
		$output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
	}

	return $output;
}

function widget_text_exec_php( $widget_text ) {
	if ( strpos( $widget_text, '<' . '?' ) !== false ) {
		ob_start();
		eval( '?>' . $widget_text );
		$widget_text = ob_get_contents();
		ob_end_clean();
	}

	return $widget_text;
}

add_filter( 'widget_text', 'widget_text_exec_php', 99 );

//
if ( ! defined( 'LEEDE_DIR' ) ) {
	define( 'LEEDE_DIR', get_template_directory() );
}
if ( ! defined( 'LEEDE_URI' ) ) {
	define( 'LEEDE_URI', trailingslashit( get_template_directory_uri() ) );
}

//
function leede_setup() {
	$path = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'leede', $path );
}

add_action( 'after_setup_theme', 'leede_setup' );

//admin
function leede_admin_enqueue_scripts() {
	wp_register_style( 'adminstyle', get_stylesheet_directory_uri() . '/admin/admin.css' );
	wp_enqueue_style( 'adminstyle' );
	wp_register_style( 'adminpostescanadastyle', 'http://ws1.postescanada-canadapost.ca/css/addresscomplete-2.30.min.css?key=nf32-ru33-uu86-af99' );
	wp_enqueue_style( 'adminpostescanadastyle' );
	wp_enqueue_script( 'adminpostescanadajs', 'http://ws1.postescanada-canadapost.ca/js/addresscomplete-2.30.min.js?key=nf32-ru33-uu86-af99', array(), '', true );
}

add_action( 'admin_enqueue_scripts', 'leede_admin_enqueue_scripts', 1 );
require_once( 'admin/shortcodes.php' );
require_once( 'admin/clients.php' );
require_once( 'ReduxCore/framework.php' );
require_once( 'admin/options.php' );
