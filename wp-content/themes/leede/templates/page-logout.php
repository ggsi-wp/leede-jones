<?php
/**
 * Template Name: Leede Logout
 *
 */
if ( is_user_logged_in() ) {
	wp_logout();
}

get_header();
?>
    <div class="container yyet leede-form">
        <div class="container_inner default_template_holder clearfix page_container_inner">
            <?php
            if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    <?php the_content();?>
                <?php endwhile;
                wp_reset_postdata(); ?>
            <?php endif;?>
        </div>
    </div>
<?php
get_footer();
