<?php
/**
 * Template Name: Leede Forgot Password
 *
 */
if ( is_user_logged_in() ) {
	wp_redirect( home_url() );
	exit;
}

global $leede_options;

if ( ! isset( $_SESSION ) ) {
	session_start();
}
if ( isset( $_POST['restart_form'] ) ) {
	session_unset();
}
if ( ! empty( $_GET['send_security_code'] ) ) {
	$_SESSION['forgot_password_step']['send_security_code'] = $_GET['send_security_code'];
	$send_security_code_array                               = explode( ' ', base64_decode( $_SESSION['forgot_password_step']['send_security_code'] ) );
	$_SESSION['forgot_password_step']['username']           = $send_security_code_array[0];
	$_SESSION['forgot_password_step']['time']               = isset( $send_security_code_array[1] ) ? $send_security_code_array[1] : 0;
	$forgot_password_expired_time                           = ! empty( $leede_options['forgot_password_expired_time'] ) ? $leede_options['forgot_password_expired_time'] : 18000;
	if ( leede_user_id_exists( $_SESSION['forgot_password_step']['username'] ) && time() - $_SESSION['forgot_password_step']['time'] <= $forgot_password_expired_time ) {
		$_SESSION['forgot_password_step']['send_security_code'] = 'valid';
	} else {
		$_SESSION['forgot_password_step']['send_security_code'] = 'expired';
	}
	wp_redirect( remove_query_arg( 'send_security_code' ) );
	exit;
}
require __DIR__ . '/../twilio-php-master/src/Twilio/autoload.php';
//Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

//API Credentials - Twilio
if ( $leede_options['twilio_phone_number'] && $leede_options['twilio_sid'] && $leede_options['twilio_token'] ) {
	$twilio_phone_number = $leede_options['twilio_phone_number'];
	$twilio_sid          = $leede_options['twilio_sid'];
	$twilio_token        = $leede_options['twilio_token'];
} else {
	$twilio_phone_number = '8167223210';//'+15005550006';
	$twilio_sid          = 'AC1b9ac518508cb3fbe05dc69412801763';//'ACb0a4bb3002502712ea208171727794cb';
	$twilio_token        = '1cfbbb3906c41e28a3bdef0412bbecf7';//'c69eb7a273c057f938ebb4ccf8071b43';
}

get_header();
?>
    <div class="container yyet leede-form">
        <div class="container_inner default_template_holder clearfix page_container_inner">
			<?php
			if ( isset( $_SESSION['forgot_password_step']['username'] ) && isset( $_SESSION['forgot_password_step']['send_security_code'] ) && $_SESSION['forgot_password_step']['send_security_code'] == 'valid' ) { ?>
                <div class="box-phone">
					<?php
					if ( isset( $_POST['submit_sms_twilio'] ) && ( ! isset( $_SESSION['forgot_password_step']['verify_phone_number'] ) || ( isset( $_SESSION['forgot_password_step']['verify_phone_number'] ) && $_SESSION['forgot_password_step']['verify_phone_number'] != 'unsucessfully' ) ) ) {
						if ( isset( $_SESSION['forgot_password_step']['count_sms'] ) && $_SESSION['forgot_password_step']['count_sms'] >= 3 ) {
							$_SESSION['forgot_password_step']['count_sms'] = $_SESSION['forgot_password_step']['count_sms'] + 1;
						} else {
							$_SESSION['forgot_password_step']['count_code'] = isset( $_SESSION['forgot_password_step']['count_code'] ) ? $_SESSION['forgot_password_step']['count_code'] : 0;
							try {
								$client                                                 = new Client( $twilio_sid, $twilio_token );
								$code_twilio                                            = rand( 1000000, 9999999 );
								$_SESSION['forgot_password_step']['code_twilio']        = md5( $code_twilio );
								$_SESSION['forgot_password_step']['code_twilio_expiry'] = time();
								$phone_number1                                          = get_user_meta( $_SESSION['forgot_password_step']['username'], 'phone_number1', true );
								$phone_number2                                          = get_user_meta( $_SESSION['forgot_password_step']['username'], 'phone_number2', true );
								$phone_number3                                          = get_user_meta( $_SESSION['forgot_password_step']['username'], 'phone_number3', true );
								$phone_number                                           = $phone_number1 . $phone_number2 . $phone_number3;
								// Use the client to do fun stuff like send text messages!
								$client->messages->create(
								// the number you'd like to send the message to
									$phone_number,
									[
										// A Twilio phone number you purchased at twilio.com/console
										'from' => $twilio_phone_number,
										// the body of the text message you'd like to send
										'body' => "" . $code_twilio . " is your password recovery code from Leede Jones Gable Inc."
									]
								);
								$getLastResponse                                         = $client->httpClient->lastResponse;
								$getContent                                              = $getLastResponse->getContent();
								$getBody                                                 = $getContent['body'];
								$_SESSION['forgot_password_step']['verify_phone_number'] = 'waiting';
								$_SESSION['forgot_password_step']['count_sms']           = isset( $_SESSION['forgot_password_step']['count_sms'] ) ? ( $_SESSION['forgot_password_step']['count_sms'] + 1 ) : 1;
							} catch ( Exception $e ) {
								if ( isset( $_SESSION['change_mobile_sms']['verify_phone_number'] ) ) {
									unset( $_SESSION['change_mobile_sms']['verify_phone_number'] );
								}
							}
						}

					}
					$twilio_time       = ! empty( $leede_options['twilio_time'] ) ? (int) $leede_options['twilio_time'] : 300;
					$twilio_limit_sms  = ! empty( $leede_options['twilio_limit_sms'] ) ? (int) $leede_options['twilio_limit_sms'] : 3;
					$twilio_limit_code = ! empty( $leede_options['twilio_limit_code'] ) ? (int) $leede_options['twilio_limit_code'] : 3;
					$twilio_again      = ! empty( $leede_options['twilio_again'] ) ? (int) $leede_options['twilio_again'] : 3600;
					if ( isset( $_SESSION['forgot_password_step']['code_twilio_expiry'] ) && time() - $_SESSION['forgot_password_step']['code_twilio_expiry'] > $twilio_time ) {
						unset( $_SESSION['forgot_password_step']['code_twilio'] );
						unset( $_SESSION['forgot_password_step']['code_twilio_expiry'] );
					}
					if ( isset( $_POST['submit_code_twilio'] ) && ( ( isset( $_SESSION['forgot_password_step']['code_twilio'] ) && md5( $_POST['code_twilio1'] ) != $_SESSION['forgot_password_step']['code_twilio'] ) || ! isset( $_SESSION['forgot_password_step']['code_twilio'] ) ) ) {
						$_SESSION['forgot_password_step']['count_code'] = isset( $_SESSION['forgot_password_step']['count_code'] ) ? $_SESSION['forgot_password_step']['count_code'] + 1 : 1;
					}
					if ( isset( $_POST['submit_code_twilio'] ) && isset( $_SESSION['forgot_password_step']['code_twilio'] ) && md5( $_POST['code_twilio1'] ) == $_SESSION['forgot_password_step']['code_twilio'] && isset( $_SESSION['forgot_password_step']['count_sms'] ) && $_SESSION['forgot_password_step']['count_sms'] <= $twilio_limit_sms && isset( $_SESSION['forgot_password_step']['count_code'] ) && $_SESSION['forgot_password_step']['count_code'] <= $twilio_limit_code ) {
						$_SESSION['forgot_password_step']['verify_phone_number'] = 'sucessfully';
					}
					if ( ( isset( $_SESSION['forgot_password_step']['count_sms'] ) && $_SESSION['forgot_password_step']['count_sms'] > $twilio_limit_sms ) || ( isset( $_SESSION['forgot_password_step']['count_code'] ) && $_SESSION['forgot_password_step']['count_code'] > $twilio_limit_code ) ) {
						$_SESSION['forgot_password_step']['verify_phone_number'] = 'unsucessfully';
					}
					if ( ( isset( $_POST['submit_sms_twilio'] ) || isset( $_POST['submit_code_twilio'] ) ) && isset( $_SESSION['forgot_password_step']['verify_phone_number'] ) && $_SESSION['forgot_password_step']['verify_phone_number'] == 'unsucessfully' ) {
						$_SESSION['forgot_password_step']['verify_phone_number_expiry'] = time();
					}
					if ( isset( $_SESSION['forgot_password_step']['verify_phone_number_expiry'] ) && time() - $_SESSION['forgot_password_step']['verify_phone_number_expiry'] > $twilio_again ) {
						$_SESSION['forgot_password_step']['count_sms']  = 0;
						$_SESSION['forgot_password_step']['count_code'] = 0;
						unset( $_SESSION['forgot_password_step']['verify_phone_number'] );
						unset( $_SESSION['forgot_password_step']['verify_phone_number_expiry'] );
					}
					?>
					<?php if ( isset( $_SESSION['forgot_password_step']['verify_phone_number'] ) ) { ?>
						<?php if ( $_SESSION['forgot_password_step']['verify_phone_number'] == 'sucessfully' ) { ?>
                            <div class="code-wrap">
								<?php if ( isset( $_POST['new_password'] ) && $_POST['new_password'] == $_POST['confirm_new_password'] ) {
									wp_set_password( $_POST['new_password'], $_SESSION['forgot_password_step']['username'] ); ?>
                                    <h1><?php echo esc_html__( 'Thank you', 'leede' ); ?></h1>
									<?php
									//Thank you
									if ( $leede_options['forgot_password_thank_content'] ) {
										echo do_shortcode( $leede_options['forgot_password_thank_content'] );
									}
									?>
								<?php } else { ?>
                                    <form method="post" class="forgot_password_form forgot_password_step3" action="">
                                        <h1><?php echo esc_html__( 'Reset Your Password', 'leede' ); ?></h1>
										<?php
										//Success Change Mobile SMS
										if ( $leede_options['forgot_password_success_content'] ) {
											echo do_shortcode( $leede_options['forgot_password_success_content'] );
										}
										?>
                                        <fieldset>
                                            <label for="new_password">
                                                <span class="required">* </span><?php echo esc_html__( 'Password', 'leede' ); ?>
                                            </label>
                                            <div class="field">
                                                <input id="new_password" type="password" name="new_password">
                                                <span class="error error_new_password"></span>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <label for="confirm_new_password">
                                                <span class="required">* </span><?php echo esc_html__( 'Confirm Password', 'leede' ); ?>
                                            </label>
                                            <div class="field">
                                                <input id="confirm_new_password" type="password"
                                                       name="confirm_new_password">
                                                <span class="error error_confirm_new_password"></span>
                                                <div class="clearfix"></div>
                                                <button type="submit" name="submit_forgot_password_step3">
													<?php echo esc_html__( 'Submit', 'leede' ); ?>
                                                </button>
                                            </div>
                                        </fieldset>
                                    </form>
								<?php } ?>
                            </div>
						<?php } ?>
						<?php if ( $_SESSION['forgot_password_step']['verify_phone_number'] == 'unsucessfully' ) { ?>
                            <div class="code-wrap">
                                <h1><?php echo esc_html__( 'Reset Your Password', 'leede' ); ?></h1>
								<?php
								//Failed - Too many failed attempts
								if ( $leede_options['forgot_password_many_failed_content'] ) {
									echo do_shortcode( $leede_options['forgot_password_many_failed_content'] );
								}
								?>
                            </div>
						<?php } ?>
						<?php if ( $_SESSION['forgot_password_step']['verify_phone_number'] == 'waiting' ) { ?>
                            <div class="code-wrap">
								<?php if ( isset( $_POST['submit_code_twilio'] ) ) { ?>
                                    <h1><?php echo esc_html__( 'Reset Your Password', 'leede' ); ?></h1>
									<?php
									//Failed - Verify Security Code
									if ( $leede_options['forgot_password_failed_content'] ) {
										echo do_shortcode( $leede_options['forgot_password_failed_content'] );
									}
									?>
								<?php } else { ?>
                                    <h1><?php echo esc_html__( 'Reset Your Password', 'leede' ); ?></h1>
									<?php
									//Verify Security Code
									if ( $leede_options['forgot_password_verify_content'] ) {
										echo do_shortcode( $leede_options['forgot_password_verify_content'] );
									}
									?>
                                    <br>
                                    <form method="post" action="">
                                        <div class="cellphone-code">
                                            <input title="code" type="text" name="code_twilio1" maxlength="7">
                                        </div>
                                        <button type="submit" name="submit_code_twilio">
											<?php echo esc_html__( 'Verify', 'leede' ) ?>
                                        </button>
                                    </form>
								<?php } ?>
                            </div>
						<?php } ?>
					<?php } ?>
					<?php if ( ! isset( $_SESSION['forgot_password_step']['verify_phone_number'] ) || ( isset( $_SESSION['forgot_password_step']['verify_phone_number'] ) && $_SESSION['forgot_password_step']['verify_phone_number'] != 'sucessfully' ) ) { ?>
                        <div class="send-wrap">
                            <h1><?php echo esc_html__( 'Reset Your Password', 'leede' ); ?></h1>
							<?php
							//Reset Password
							if ( $leede_options['forgot_password_reset_content'] ) {
								echo $leede_options['forgot_password_reset_content'];
							}
							?>
                            <form method="post" action="">
                                <button type="submit" name="submit_sms_twilio">
									<?php echo esc_html__( 'Send Code', 'leede' ); ?>
                                </button>
                            </form>
                        </div>
					<?php } ?>
                </div>
			<?php }
			if ( isset( $_SESSION['forgot_password_step']['username'] ) && isset( $_SESSION['forgot_password_step']['send_security_code'] ) && $_SESSION['forgot_password_step']['send_security_code'] == 'expired' ) { ?>
                <h1><?php echo esc_html__( 'Forgot Password', 'leede' ); ?></h1>
				<?php
				//Failed - Link in email expired
				if ( $leede_options['forgot_password_expired_content'] ) {
					echo do_shortcode( $leede_options['forgot_password_expired_content'] );
				}
				?>
			<?php }
			$email = isset( $_POST['username'] ) ? $_POST['username'] : '';
			$user  = '';
			if ( username_exists( $email ) ) {
				$user = get_user_by( 'login', $email );
			} elseif ( email_exists( $email ) ) {
				$user = get_user_by( 'email', $email );
			}
			if ( $email != '' && $user != '' ) {
				//Forgot Password Email Notification
				$array                            = array(
					'username' => $user->ID,
					'time'     => time(),
				);
				$_SESSION['forgot_password_step'] = $array;
				//Email - To Client For Confirming Email Address
				$first_name         = get_user_meta( $user->ID, 'first_name', true );
				$last_name          = get_user_meta( $user->ID, 'last_name', true );
				$send_security_code = leede_getTplPageURL( 'templates/page-forgotpassword.php' ) . '?send_security_code=' . base64_encode( $_SESSION['forgot_password_step']['username'] . ' ' . $_SESSION['forgot_password_step']['time'] );
				$logo_img           = get_stylesheet_directory_uri() . '/images/logo.jpg';
				$to                 = $email;
				if ( $leede_options['forgot_password_email_subject'] ) {
					$subject = $leede_options['forgot_password_email_subject'];
				} else {
					$subject = 'Forgot Password Email Notification';
				}
				if ( $leede_options['forgot_password_email_body'] ) {
					$body = do_shortcode( $leede_options['forgot_password_email_body'] );
				} else {
					$body = '
                    <p><img alt="img" src="' . $logo_img . '"/></p>
                    <p>Dear ' . $first_name . ' ' . $last_name . ':</p>
                    <h3>Please confirm your password reset</h3>
                    <p>
                    Thank you for starting the password reset process. In order to complete your password reset please click the confirmation link below. After clicking this link you will be taken to a page where you can enter a password and verify your cell phone number as an additional security check.
                    </p>
                    <p><a href="' . $send_security_code . '">' . $send_security_code . '</a><p>
                    <p>If you have problems to open this link, please copy and paste the above URL into your web browser.</p>
                    <p>If you received this email by mistakes, simply delete it.</p>
                    <p>If you have any questions please contact your investment advisor.</p>
                    ';
				}
				if ( $leede_options['forgot_password_email_signature'] ) {
					$body .= '<br>' . do_shortcode( $leede_options['forgot_password_email_signature'] );
				} else {
					$body .= '
                    <br>
                    <p><small>This is an automatically delivered message and replies to this address cannot be answered.</small></p> 
                    <h3 style="font-weight: normal; padding-top: 15px; border-top: 1px solid #eee">© Leede Jones Gable Inc. 2020</h3>
					';
				}
				$headers = array( 'Content-Type: text/html; charset=UTF-8' );
				if ( $leede_options['forgot_password_email_headers'] ) {
					$headers[] = $leede_options['forgot_password_email_headers'];
				}
				wp_mail( $to, $subject, $body, $headers );
			}
			if ( isset( $_SESSION['forgot_password_step']['username'] ) && ! isset( $_SESSION['forgot_password_step']['send_security_code'] ) ) {
				?>
                <h1><?php echo esc_html__( 'Reset Your Password', 'leede' ); ?></h1>
				<?php
				//Thanks Page - Email Sent
				if ( $leede_options['forgot_password_thanks_content'] ) {
					echo do_shortcode( $leede_options['forgot_password_thanks_content'] );
				}
				?>
			<?php }
			if ( ! isset( $_SESSION['forgot_password_step']['username'] ) && ! isset( $_SESSION['forgot_password_step']['send_security_code'] ) ) { ?>
                <h1><?php echo esc_html__( 'Reset Your Password', 'leede' ); ?></h1>
				<?php
				//Forgot Password
				if ( $leede_options['forgot_password_content'] ) {
					echo do_shortcode( $leede_options['forgot_password_content'] );
				}
				?>
                <form method="post" class="forgot_password_form forgot_password_step1" action="">
                    <fieldset>
                        <label for="username">
                            <span class="required">* </span><?php echo esc_html__( 'Email:', 'leede' ); ?></label>
                        <div class="field">
                            <input id="username" type="email" name="username" value="<?php echo $email; ?>">
                            <span class="error error_username"></span>
                            <button type="submit"
                                    name="submit_forgot_password_step1"><?php echo esc_html__( 'Submit', 'leede' ); ?></button>
							<?php if ( $email != '' && $user == '' ) { ?>
                                <div class="box">
									<?php echo esc_html__( 'Not currently registered for', 'leede' ); ?>
									<?php echo $email . '?'; ?>
                                    <a href="<?php echo leede_getTplPageURL( 'templates/page-register.php' ); ?>">
										<?php echo esc_html__( 'Register now.', 'leede' ); ?>
                                    </a>
                                </div>
							<?php } ?>
                        </div>
                    </fieldset>
                    <p><?php echo esc_html__( 'Already have an account?', 'leede' ) ?>
                        <a href="<?php echo leede_getTplPageURL( 'templates/page-login.php' ) ?>"><?php echo esc_html__( 'Sign In »', 'leede' ) ?></a>
                    </p>
                </form>
			<?php }
			?>
        </div>
    </div>
<?php
if ( isset( $_POST['new_password'] ) && $_POST['new_password'] == $_POST['confirm_new_password'] ) {
	session_unset();
}
get_footer();