<?php
/**
 * Template Name: Leede Change Password
 *
 */
if ( ! is_user_logged_in() || ( is_user_logged_in() && ! in_array( 'client', wp_get_current_user()->roles ) ) ) {
	wp_redirect( home_url() );
	exit;
}
get_header();
?>
    <div class="container yyet leede-form leede-manage-profile">
        <div class="container_inner default_template_holder clearfix page_container_inner">
            <div class="leede-container">
                <div class="leede-col-3">
	                <?php echo do_shortcode('[lj-client-dashboard]');?>
                </div>
                <div class="leede-col-9">
                    <h2><?php echo esc_html__( 'Change Password', 'leede' ); ?></h2>
                    <br>
	                <?php
	                $current_password = isset( $_POST['current_password'] ) ? $_POST['current_password'] : '';
	                if ( $current_password != '' ) {
		                $check_current_password = wp_check_password( $_POST['current_password'], wp_get_current_user()->data->user_pass, wp_get_current_user()->ID );
		                if ( $_POST['new_password'] == $_POST['confirm_new_password'] &&  isset( $check_current_password ) && $check_current_password == 1 ) {
			                wp_set_password( $_POST['new_password'], wp_get_current_user()->ID );
			                ?>
                            <div class="notice"><?php echo esc_html__( 'Your password has been changed.', 'leede' ); ?></div>
		                <?php } else { ?>
                            <div class="notice error"><?php echo esc_html__( 'The password you entered is incorrect.', 'leede' ); ?></div>
		                <?php }
	                } ?>
                    <form method="post" class="change_password_form" action="">
                        <fieldset>
                            <label for="current_password">
                                <span class="required">* </span><?php echo esc_html__( 'Current Password', 'leede' ); ?></label>
                            <div class="field">
                                <input id="current_password" type="password" name="current_password">
                                <span class="error error_current_password"></span>
                            </div>
                        </fieldset>
                        <fieldset>
                            <label for="new_password">
                                <span class="required">* </span><?php echo esc_html__( 'New Password', 'leede' ); ?></label>
                            <div class="field">
                                <input id="new_password" type="password" name="new_password">
                                <span class="error error_new_password"></span>
                            </div>
                        </fieldset>
                        <fieldset>
                            <label for="confirm_new_password">
                                <span class="required">* </span><?php echo esc_html__( 'Re-enter New Password', 'leede' ); ?>
                            </label>
                            <div class="field">
                                <input id="confirm_new_password" type="password" name="confirm_new_password">
                                <span class="error error_confirm_new_password"></span>
                                <div class="clearfix"></div>
                                <button type="submit"
                                        name="submit_change_password"><?php echo esc_html__( 'Submit', 'leede' ); ?></button>
                            </div>
                        </fieldset>
                        <p>
			                <?php echo esc_html__( 'Password must be a minimum of eight characters and include at least one number, one uppercase letter and one lowercase letter.', 'leede' ); ?>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();