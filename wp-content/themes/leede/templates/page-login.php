<?php
/**
 * Template Name: Leede Login
 *
 */
if ( is_user_logged_in() ) {
	wp_redirect( home_url() );
	exit;
}
global $leede_options;

get_header();
?>

    <div class="container yyet leede-form">
        <div class="container_inner default_template_holder clearfix page_container_inner">
            <div class="table-form clearfix">
                <div class="cell-form">
                    <h1><?php echo esc_html__( 'Client Dashboard Login', 'leede' ); ?></h1>
					<?php
					$username_failed = ( isset( $_COOKIE['username_failed'] ) ) ? $_COOKIE['username_failed'] : '';
					if ( $username_failed !== "" ) { ?>
                        <p>
							<?php echo esc_html__( 'Not currently registered for', 'leede' ); ?>
							<?php echo $username_failed . '?'; ?>
                            <a href="<?php echo leede_getTplPageURL( 'templates/page-register.php' ); ?>">
								<?php echo esc_html__( 'Register now.', 'leede' ); ?>
                            </a>
                        </p>
					<?php }
					$logout_message = ( isset( $_COOKIE['logout_message'] ) ) ? $_COOKIE['logout_message'] : '';
					if ( $logout_message !== "" ) { ?>
						<?php
						//Login Timeout
						if ( $leede_options['login_timeout_content'] ) {
							echo do_shortcode( $leede_options['login_timeout_content'] );
						}
						?>
					<?php }
					//					$args = array(
					//						'redirect'       => ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
					//						'form_id'        => 'leede-login-form',
					//						'label_username' => esc_html__( 'Email:', 'leede' ),
					//						'label_password' => esc_html__( 'Password:', 'leede' ),
					//						'label_log_in'   => esc_html__( 'Login Now', 'leede' ),
					//						'id_username'    => 'username',
					//						'id_password'    => 'password',
					//						'id_submit'      => 'wp-submit',
					//						'remember'       => false
					//					);
					//					wp_login_form( $args );
					echo do_shortcode( '[wpum_login_form psw_link="yes" register_link="no"]' );
					?>
					<?php
					$login_failed = ( isset( $_COOKIE['login_failed'] ) ) ? $_COOKIE['login_failed'] : 0;
					if ( $login_failed === "failed" ) { ?>
						<?php
						//Login Failed
						if ( $leede_options['login_failed_content'] ) {
							echo do_shortcode( $leede_options['login_failed_content'] );
						}
						?>
					<?php } ?>
                    <p class="forgot-password">
						<?php echo esc_html__( 'Forgot your password?', 'leede' ); ?>
                        <a href="<?php echo leede_getTplPageURL( 'templates/page-forgotpassword.php' ); ?>">
							<?php echo esc_html__( 'Reset it here', 'leede' ); ?>
                        </a>
                    </p>
                </div>
                <div class="cell-form">
                    <h1><?php echo esc_html__( 'Register for the Client Dashboard', 'leede' ); ?></h1>
					<?php
					if ( $leede_options['login_content'] ) {
						echo do_shortcode( $leede_options['login_content'] );
					}
					?>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
