<?php
/**
 * Template Name: Leede Manage Profile
 *
 */
if ( ! is_user_logged_in() || ( is_user_logged_in() && ! in_array( 'client', wp_get_current_user()->roles ) ) ) {
	wp_redirect( home_url() );
	exit;
}
get_header();
?>
    <div class="container yyet leede-form leede-manage-profile">
        <div class="container_inner default_template_holder clearfix page_container_inner">
            <?php echo do_shortcode('[lj-client-dashboard]');?>
        </div>
    </div>
<?php
get_footer();
