<?php
/**
 * Template Name: Leede Change Mobile SMS
 *
 */

if ( ! is_user_logged_in() || ( is_user_logged_in() && ! in_array( 'client', wp_get_current_user()->roles ) ) ) {
	wp_redirect( home_url() );
	exit;
}
if ( ! isset( $_SESSION ) ) {
	session_start();
}

global $leede_options;

require __DIR__ . '/../twilio-php-master/src/Twilio/autoload.php';
//Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

//API Credentials - Twilio
if ( $leede_options['twilio_phone_number'] && $leede_options['twilio_sid'] && $leede_options['twilio_token'] ) {
	$twilio_phone_number = $leede_options['twilio_phone_number'];
	$twilio_sid          = $leede_options['twilio_sid'];
	$twilio_token        = $leede_options['twilio_token'];
} else {
	$twilio_phone_number = '8167223210';//'+15005550006';
	$twilio_sid          = 'AC1b9ac518508cb3fbe05dc69412801763';//'ACb0a4bb3002502712ea208171727794cb';
	$twilio_token        = '1cfbbb3906c41e28a3bdef0412bbecf7';//'c69eb7a273c057f938ebb4ccf8071b43';
}

get_header();
?>
    <div class="container yyet leede-form leede-manage-profile">
        <div class="container_inner default_template_holder clearfix page_container_inner">
            <div class="leede-container">
                <div class="leede-col-3">
	                <?php echo do_shortcode('[lj-client-dashboard]');?>
                </div>
                <div class="leede-col-9">
                    <h2><?php echo esc_html__( 'Change Cell Phone Number', 'leede' ); ?></h2>
                    <div class="box-phone">
						<?php
						if ( isset( $_POST['submit_sms_twilio'] ) && ( ! isset( $_SESSION['change_mobile_sms']['verification_cellphone'] ) || ( isset( $_SESSION['change_mobile_sms']['verification_cellphone'] ) && $_SESSION['change_mobile_sms']['verification_cellphone'] != 'unsucessfully' ) ) ) {
							if ( isset( $_SESSION['change_mobile_sms']['count_sms'] ) && $_SESSION['change_mobile_sms']['count_sms'] >= 3 ) {
								$_SESSION['change_mobile_sms']['count_sms'] = $_SESSION['change_mobile_sms']['count_sms'] + 1;
							} else {
								$_SESSION['change_mobile_sms']['count_code'] = isset( $_SESSION['change_mobile_sms']['count_code'] ) ? $_SESSION['change_mobile_sms']['count_code'] : 0;
								try {
									$client                                              = new Client( $twilio_sid, $twilio_token );
									$code_twilio                                         = rand( 1000000, 9999999 );
									$_SESSION['change_mobile_sms']['code_twilio']        = md5( $code_twilio );
									$_SESSION['change_mobile_sms']['code_twilio_expiry'] = time();
									// Use the client to do fun stuff like send text messages!
									$client->messages->create(
									// the number you'd like to send the message to
										$_POST['phone_number1'] . $_POST['phone_number2'] . $_POST['phone_number3'],
										[
											// A Twilio phone number you purchased at twilio.com/console
											'from' => $twilio_phone_number,
											// the body of the text message you'd like to send
											'body' => "" . $code_twilio . " is your verification code from Leede Jones Gable Inc."
										]
									);
									$getLastResponse                                      = $client->httpClient->lastResponse;
									$getContent                                           = $getLastResponse->getContent();
									$getBody                                              = $getContent['body'];
									$_SESSION['change_mobile_sms']['submit_sms_twilio']   = 'success';
									$_SESSION['change_mobile_sms']['phone_number1']       = trim( $_POST['phone_number1'] );
									$_SESSION['change_mobile_sms']['phone_number2']       = trim( $_POST['phone_number2'] );
									$_SESSION['change_mobile_sms']['phone_number3']       = trim( $_POST['phone_number3'] );
									$_SESSION['change_mobile_sms']['verify_phone_number'] = 'waiting';
									$_SESSION['change_mobile_sms']['count_sms']           = isset( $_SESSION['change_mobile_sms']['count_sms'] ) ? ( $_SESSION['change_mobile_sms']['count_sms'] + 1 ) : 1;
								} catch ( Exception $e ) {
									$_SESSION['change_mobile_sms']['submit_sms_twilio'] = 'error';
									if ( isset( $_SESSION['change_mobile_sms']['verify_phone_number'] ) ) {
										unset( $_SESSION['change_mobile_sms']['verify_phone_number'] );
									}
								}
							}
						}
						$twilio_time       = ! empty( $leede_options['twilio_time'] ) ? (int) $leede_options['twilio_time'] : 300;
						$twilio_limit_sms  = ! empty( $leede_options['twilio_limit_sms'] ) ? (int) $leede_options['twilio_limit_sms'] : 3;
						$twilio_limit_code = ! empty( $leede_options['twilio_limit_code'] ) ? (int) $leede_options['twilio_limit_code'] : 3;
						$twilio_again      = ! empty( $leede_options['twilio_again'] ) ? (int) $leede_options['twilio_again'] : 3600;
						if ( isset( $_SESSION['change_mobile_sms']['code_twilio_expiry'] ) && time() - $_SESSION['change_mobile_sms']['code_twilio_expiry'] > $twilio_time ) {
							unset( $_SESSION['change_mobile_sms']['code_twilio'] );
							unset( $_SESSION['change_mobile_sms']['code_twilio_expiry'] );
						}
						if ( isset( $_POST['submit_code_twilio'] ) && ((isset( $_SESSION['change_mobile_sms']['code_twilio'] ) && md5( $_POST['code_twilio1'] ) != $_SESSION['change_mobile_sms']['code_twilio'] ) || !isset( $_SESSION['change_mobile_sms']['code_twilio'] ))) {
							$_SESSION['change_mobile_sms']['count_code'] = isset( $_SESSION['change_mobile_sms']['count_code'] ) ? $_SESSION['change_mobile_sms']['count_code'] + 1 : 1;
						}
						if ( ( isset( $_SESSION['change_mobile_sms']['count_sms'] ) && $_SESSION['change_mobile_sms']['count_sms'] > $twilio_limit_sms ) || ( isset( $_SESSION['change_mobile_sms']['count_code'] ) && $_SESSION['change_mobile_sms']['count_code'] > $twilio_limit_code ) ) {
							$_SESSION['change_mobile_sms']['verify_phone_number'] = 'unsucessfully';
						}
						if ( ( isset( $_POST['submit_sms_twilio'] ) || isset( $_POST['submit_code_twilio'] ) ) && isset( $_SESSION['change_mobile_sms']['verify_phone_number'] ) && $_SESSION['change_mobile_sms']['verify_phone_number'] == 'unsucessfully' ) {
							$_SESSION['change_mobile_sms']['verify_phone_number_expiry'] = time();
						}
						if ( isset( $_SESSION['change_mobile_sms']['verify_phone_number_expiry'] ) && time() - $_SESSION['change_mobile_sms']['verify_phone_number_expiry'] > $twilio_again ) {
							unset( $_SESSION['change_mobile_sms']['submit_sms_twilio'] );
							$_SESSION['change_mobile_sms']['count_sms']  = 0;
							$_SESSION['change_mobile_sms']['count_code'] = 0;
							unset( $_SESSION['change_mobile_sms']['verify_phone_number'] );
							unset( $_SESSION['change_mobile_sms']['verify_phone_number_expiry'] );
						}
						?>
						<?php if ( isset( $_POST['submit_code_twilio'] ) && isset( $_SESSION['change_mobile_sms']['code_twilio'] ) && md5( $_POST['code_twilio1'] ) == $_SESSION['change_mobile_sms']['code_twilio'] && isset( $_SESSION['change_mobile_sms']['count_sms'] ) && $_SESSION['change_mobile_sms']['count_sms'] <= $twilio_limit_sms && isset( $_SESSION['change_mobile_sms']['count_code'] ) && $_SESSION['change_mobile_sms']['count_code'] <= $twilio_limit_code ) {
							unset( $_SESSION['change_mobile_sms']['verify_phone_number'] );
							$phone_number1     = $_SESSION['change_mobile_sms']['phone_number1'];
							$old_phone_number1 = get_user_meta( wp_get_current_user()->ID, 'phone_number1', true );
							if ( $old_phone_number1 != $phone_number1 ) {
								update_user_meta( wp_get_current_user()->ID, 'phone_number1', $phone_number1 );
							}
							$phone_number2     = $_SESSION['change_mobile_sms']['phone_number2'];
							$old_phone_number2 = get_user_meta( wp_get_current_user()->ID, 'phone_number2', true );
							if ( $old_phone_number2 != $phone_number2 ) {
								update_user_meta( wp_get_current_user()->ID, 'phone_number2', $phone_number2 );
							}
							$phone_number3     = $_SESSION['change_mobile_sms']['phone_number3'];
							$old_phone_number3 = get_user_meta( wp_get_current_user()->ID, 'phone_number3', true );
							if ( $old_phone_number3 != $phone_number3 ) {
								update_user_meta( wp_get_current_user()->ID, 'phone_number3', $phone_number3 );
							}
							?>
                            <div class="code-wrap">
								<?php
								//Success Change Mobile SMS
								if ( $leede_options['manage_profile_success_content'] ) {
									echo do_shortcode( $leede_options['manage_profile_success_content'] );
								}
								$mobile_sms = get_user_meta( wp_get_current_user()->ID, 'phone_number1', true ) . ' ' . get_user_meta( wp_get_current_user()->ID, 'phone_number2', true ) . ' ' . get_user_meta( wp_get_current_user()->ID, 'phone_number3', true );;
								echo leede_getCellphone( $mobile_sms );
								?>
                            </div>
						<?php } ?>
						<?php if ( isset( $_SESSION['change_mobile_sms']['verify_phone_number'] ) && $_SESSION['change_mobile_sms']['verify_phone_number'] == 'unsucessfully' ) { ?>
                            <div class="code-wrap">
								<?php
								//Failed - Too many failed attempts
								if ( $leede_options['manage_profile_many_failed_content'] ) {
									echo do_shortcode( $leede_options['manage_profile_many_failed_content'] );
								}
								?>
                            </div>
						<?php } ?>
						<?php if ( isset( $_SESSION['change_mobile_sms']['verify_phone_number'] ) && ( $_SESSION['change_mobile_sms']['verify_phone_number'] ) == 'waiting' ) { ?>
                            <div class="code-wrap">
								<?php if ( isset( $_POST['submit_code_twilio'] ) ) { ?>
									<?php
									//Failed - Verify Security Code
									if ( $leede_options['manage_profile_failed_content'] ) {
										echo do_shortcode( $leede_options['manage_profile_failed_content'] );
									}
									?>
								<?php } else { ?>
                                    <!--Verify Security Code-->
									<?php
									if ( $leede_options['manage_profile_verify_content'] ) {
										echo do_shortcode( $leede_options['manage_profile_verify_content'] );
									}
									?>
                                    <form method="post" action="">
                                        <div class="cellphone-code">
                                            <input title="code" type="text" name="code_twilio1" maxlength="7">
                                        </div>
                                        <button type="submit" name="submit_code_twilio">
											<?php echo esc_html__( 'Verify', 'leede' ) ?>
                                        </button>
                                    </form>
								<?php } ?>
                            </div>
						<?php } ?>
                        <div class="send-wrap">
							<?php
							//Change Mobile SMS
							if ( $leede_options['manage_profile_mobile_content'] ) {
								echo do_shortcode( $leede_options['manage_profile_mobile_content'] );
							}
							?>
                            <br>
                            <form method="post" action="">
                                <label>
                                    <span class="required">* </span><?php echo esc_html__( 'New Cell Phone Number', 'leede' ); ?>
                                </label>
                                <br>
                                <div class="cellphone-number">
                                    (<input id="phone1" maxlength="3" class="phone-number" title="Cell phone" type="text"
                                            name="phone_number1"
                                            value="">)
                                    <span></span>
                                    <input id="phone2" maxlength="3" class="phone-number" title="Cell phone" type="text"
                                           name="phone_number2"
                                           value="">
                                    <span></span>
                                    <input id="phone3" maxlength="4" class="phone-number" title="Cell phone" type="text"
                                           name="phone_number3"
                                           value="">
                                </div>
								<?php if ( isset( $_SESSION['change_mobile_sms']['submit_sms_twilio'] ) && $_SESSION['change_mobile_sms']['submit_sms_twilio'] == 'error' ) { ?>
                                    <p style="color: red">
                                        <em>
                                            <small><?php echo esc_html__( "Sorry: Something's wrong with your phone number.", 'leede' ) ?></small>
                                        </em>
                                    </p>
								<?php } ?>
                                <button type="submit" name="submit_sms_twilio">
									<?php echo esc_html__( 'Send Code', 'leede' ); ?>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();